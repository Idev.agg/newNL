//
//  SourceModels.swift
//  newNL
//
//  Created by aggrroo on 3/3/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

enum DataType {
    
    case string, double, int, device, auto, timer, scenario, choosable, temperatureParameter, humidityParameter, complexCalendar, unknown
    
    static func dataTypeForParameterOf(eventer: NLRXDevice, type: AutoType) -> DataType {
        var dataType: DataType = .choosable
        switch eventer.subType {
        case .temperatureAndHumiditySensor:
            switch type {
            case .cooling, .heating:
                return .temperatureParameter
            case .draining, .humidification:
                dataType = .humidityParameter
            default:
                return .choosable
            }
        case .temperatureSensor:
            dataType = .temperatureParameter
        default:
            break
        }
        return dataType
    }
}

class DataError: CommonError {
    
}

protocol SourceModelProtocol {
    
    var value: Any? {get set}
    var dataType: DataType {get set}
    var error: DataError? {get set}
    
}

class SourceModel: SourceModelProtocol {
    
    var title: String?
    var value: Any?
    var dataType: DataType
    var error: DataError?
    var selectingList: [Displayable]?
    var minLength: Int?
    var maxLength: Int?
    
    init(title: String?, value: Any?, dataType: DataType, error: DataError? = nil) {
        self.title = title
        self.value = value
        self.dataType = dataType
        self.error = error
    }
    
}

class nameAndIconModel: SourceModel {
    
    var icon: UIImage?
    
    init(title: String?, value: Any?, icon: UIImage?, dataType: DataType, error: DataError?) {
        super.init(title: title, value: value, dataType: dataType)
    }
    
}

class Validator {
    
    static func validateArray(_ fields: [SourceModel]?) -> Bool {
        var valid = true
        guard let fields = fields else { return valid }
        for field in fields {
            guard validate(field)  else {
                valid = false
                continue
            }
        }
        return valid
    }
    
    static func validate(_ field: SourceModel) -> Bool {
        var valid = true
        if field.value == nil && field.dataType != .string {
            field.error = DataError.init(error: .emptyField)
            valid = false
        }
        if field.maxLength != nil && field.value is Displayable {
            let text = (field.value as! Displayable).displayName
            if text.count > field.maxLength! {
                valid = false
                field.error = DataError.init(code: CommonError.defaultCode, discription: "Превышена максимальная длинна - \(field.maxLength!) символов")
            }
        }
        return valid
    }
    
}

//
//  AttachModels.swift
//  newNL
//
//  Created by aggrroo on 5/25/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class BindModel: NLBaseObject {
    
    private var rxChannel: Int?
    var ftxId: String?
    var error: AttachmentError?
    
    init(data: Data) {
        super.init(name: nil, type: .attachObj)
        let rxChannelData = data[2..<4]
        let rxStateData = data[0..<2]
        let rxHexChannel = String(data: rxChannelData, encoding: .utf8)
        let rxHexState = String(data: rxStateData, encoding: .utf8)
        if let wrappedHexState = rxHexState, wrappedHexState == "02" {
            if let wrappedHexChannel = rxHexChannel {
                rxChannel = Int(wrappedHexChannel, radix: 16)
            }
        }
        
        let ftxIdData = data[21..<29]
        let ftxStateData = data[15..<17]
        let ftxId = String(data: ftxIdData, encoding: .utf8)?.uppercased()
        let ftxHexState = String(data: ftxStateData, encoding: .utf8)
        let successStates = ["02", "03"]
        let alreadyAttachedStates = ["04"]
        let fullFtxStates = ["00", "01"]
        if let wrappedFtxHexState = ftxHexState {
            if successStates.contains(wrappedFtxHexState) {
                self.ftxId = ftxId
            } else if alreadyAttachedStates.contains(wrappedFtxHexState) {
                self.error = AttachmentError(error: .ftxAlreadyAttached)
            } else if fullFtxStates.contains(wrappedFtxHexState) {
                self.error = AttachmentError(error: .fullFtxAttached)
            } else {
                self.error = AttachmentError(error: .attachmentError)
            }
        }
    }
    
    func isRxAttachedToChannel(chan: Int) -> Bool {
        return chan == rxChannel
    }
    
    
}

class RxSetModel: NLBaseObject {
    
}

//
//  File.swift
//  newNL
//
//  Created by aggrroo on 25.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

class NLLogItem: NSObject, DataConvertibleItemProtocol {
    
    override init() {
        super.init()
    }
    
}

class NLLogList: NLBaseObject {
    
    var logList: [NLLogItem]?
    
    init(logList: [NLLogItem]) {
        super.init(name: nil, type: .list)
        self.logList = logList
    }
    
    func getLogsFor(device: NLRXDevice) -> [NLLogItem] {
        return [NLLogItem()]
    }
    
}

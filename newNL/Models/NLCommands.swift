//
//  Commands.swift
//  newNL
//
//  Created by aggrroo on 23.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

class NLCommonCommand: NSObject, DataConvertibleItemProtocol {
    
    static let MODE = 0
    static let CTR = 1
    static let RES = 2
    static let CHANEL = 3
    static let COMMAND = 4
    static let FORMAT = 5
    static let DATA0 = 6
    static let DATA1 = 7
    static let DATA2 = 8
    static let DATA3 = 9
    static let ID_INTERVAL = Range(10...13)
    
    static let lENGHT = 14
    
    var mode: Int
    var ctr: Int
    var res: Int
    var chanel: Int
    var command: PowerBlockCommand
    var format: Int
    var data0: Int
    var data1: Int
    var data2: Int
    var data3: Int
    var id: String = "00000000"
    
    init(mode: Int, ctr: Int, res: Int, chanel: Int, command: PowerBlockCommand, format: Int, data0: Int, data1: Int, data2: Int, data3: Int, id: String?) {
        self.mode = mode
        self.ctr = ctr
        self.res = res
        self.chanel = chanel
        self.command = command
        self.format = format
        self.data0 = data0
        self.data1 = data1
        self.data2 = data2
        self.data3 = data3
        guard let id = id else { return }
        self.id = id
    }
    
    convenience init(device: NLBaseDevice?, scenario: NLScenario?, auto: NLAuto?, timerType: TimerType) {
        let mode: Int = {
            guard let device = device else {
                if scenario != nil {
                    return 254
                } else if auto != nil {
                    switch timerType {
                    case .onAuto, .onAndOffAuto:
                        return 253
                    case .offAuto:
                        return 252
                    default:
                        return 255
                    }
                } else {
                    return 255
                }
            }
            switch device {
            case is NLTXDevice:
                return 0
            case is NLFTXDevice:
                return 2
            default:
                return 255
            }
        }()
        let ctr: Int = {
            guard let device = device else {
                if let scenario = scenario  {
                    return scenario.number
                } else if let auto = auto {
                    return auto.number
                } else {
                    return 255
                }
            }
            switch device {
            case is NLTXDevice:
                return 0
            case is NLFTXDevice:
                return 9
            default:
                return 255
            }
        }()
        let chanel: Int = {
            guard let device = device else {
                if scenario != nil || auto != nil {
                    return 255
                }
                return 0
            }
            switch device {
            case is NLTXDevice:
                return device.chanel
            case is NLFTXDevice:
                return 0
            default:
                return 255
            }
        }()
        var id: String? {
            guard let device = device, device is NLFTXDevice else {
                return nil
            }
            return (device as! NLFTXDevice).id
        }
        var cmd: PowerBlockCommand = {
            switch timerType {
            case .on, .onAndOff, .sunrise:
                return .on
            default:
                return .off
            }
            
        }()
        self.init(mode: mode, ctr: ctr, res: 0, chanel: chanel, command: cmd, format: 0, data0: 0, data1: 0, data2: 0, data3: 0, id: id)
    }
    
    convenience init(device: NLBaseDevice?, scenario: NLScenario?, command: AllowedAutoCommands?) {
        let mode: Int = {
            guard let device = device else {
                guard let scenario = scenario else {
                    return 255
                }
                return 254
            }
            switch device {
            case is NLTXDevice:
                return 0
            case is NLFTXDevice:
                return 2
            default:
                return 255
            }
        }()
        let ctr: Int = {
            guard let device = device else {
                guard let scenario = scenario else {
                    return 255
                }
                return scenario.number
            }
            switch device {
            case is NLTXDevice:
                return 0
            case is NLFTXDevice:
                return 9
            default:
                return 255
            }
        }()
        let chanel: Int = {
            guard let device = device else {
                guard let scenario = scenario else {
                    return 255
                }
                return 0
            }
            switch device {
            case is NLTXDevice:
                return device.chanel
            case is NLFTXDevice:
                return 0
            default:
                return 255
            }
        }()
        var id: String? {
            guard let device = device, device is NLFTXDevice else {
                return nil
            }
            return (device as! NLFTXDevice).id
        }
        self.init(mode: mode, ctr: ctr, res: 0, chanel: chanel, command: command?.toBlockCommand() ?? PowerBlockCommand.unknown, format: 0, data0: 0, data1: 0, data2: 0, data3: 0, id: id)
    }
    
    var realValue: Data {
        var data = Data.emptyDataWithCount(count: 14)
        data[NLCommonCommand.MODE] = UInt8(mode)
        data[NLCommonCommand.CTR] = UInt8(ctr)
        data[NLCommonCommand.RES] = UInt8(res)
        data[NLCommonCommand.CHANEL] = UInt8(chanel)
        data[NLCommonCommand.COMMAND] = command.realValue
        data[NLCommonCommand.FORMAT] = UInt8(format)
        data[NLCommonCommand.DATA0] = UInt8(data0)
        data[NLCommonCommand.DATA1] = UInt8(data1)
        data[NLCommonCommand.DATA2] = UInt8(data2)
        data[NLCommonCommand.DATA3] = UInt8(data3)
        let idData = id.toDataWithCount(count: 4)
        for i in 0..<idData.count {
            data[NLCommonCommand.ID_INTERVAL.startIndex + i] = idData[i]
        }
        return data
    }
    
    func getCommandString() -> String { return String.empty }
    
}

extension NLCommonCommand {
    
    var deviceType: DeviceType {
        return UInt8(self.mode).deviceType
    }
    
    func getShortName() -> String {
        switch command {
        case .runScenario:
            return getScenarioFromCommand()?.displayName ?? "Сценарий не найден"
        default:
            return getDeviceFromCommand()?.getShortName() ?? "Блок не найден"
        }
        
    }
    
    func getDeviceFromCommand() -> NLBaseDevice? {
        guard let devices = ModelsManager.shared.getDevices() else { return nil }
        return devices.first(where: {$0.chanel == self.chanel && $0.deviceType == deviceType})
    }
    
    var toString: String {
        switch self.command {
        case .on, .off, .switching, .runScenario:
            return ""
        case .temporaryOn:
            return "Временно включить"
        case .setBrightness:
            return "Установить яркость"
        case .unknown:
            return "Выберите"
        }
    }
    
    func getScenarioFromCommand() -> NLScenario? {
        guard mode == 254, let scenarious = ModelsManager.shared.getScenarious() else { return nil }
        let scenario = scenarious.first { (scenario) -> Bool in
            return scenario.number == ctr
        }
        return scenario
    }
    
    func getAutoFromCommand() -> NLAuto? {
        guard mode == 252 || mode == 253 else { return nil }
        guard let autos = ModelsManager.shared.getAutos() else { return nil }
        let auto = autos.first { (auto) -> Bool in
            return auto.number == ctr
        }
        return auto
    }
    
}

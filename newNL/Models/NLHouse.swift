//
//  NLHouse.swift
//  newNL
//
//  Created by Глеб Сергей on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

class NLHouse: NLBaseObject {
    
    static let FIRST_BYTE = 8710
    static let NAME_PARSING_INTERVAL = Range(0...62)
    static let FINISHED_BYTE = 8773
    
    var currentRoomIndex = 0
    
    init(name: String) { super.init(name: name, type: .nlHouse) }
    
    func getDeviceCount() -> Int { return ModelsManager.shared.getDevices()?.count ?? 0 }
    
    func getRoomsCount() -> Int { return ModelsManager.shared.getRooms()?.count ?? 0 }
    
    func getVisibleRoomsCount() -> Int {
        guard let rooms = ModelsManager.shared.getRooms() else { return 0 }
        let count: Int? = {
            var count = 0
            for room in rooms {
                guard room.number != NLRoom.UNDISTRIBUTED_DEVICES_ROOM_NUMBER else { continue }
                count += 1
            }
            return count
        }()
        return count ?? 0
    }
    
    func getVisibleRooms() -> [NLRoom]? {
        guard var visible = getRoomsWithoutUndistributed() else { return nil }
        if let undistributted = getRoomWith(number: NLRoom.UNDISTRIBUTED_DEVICES_ROOM_NUMBER) {
            if undistributted.getDeviceCount() > 0 {
                visible.append(undistributted)
            }
        }
        return visible
    }
    
    func getRoomsWithoutUndistributed() -> [NLRoom]? {
        guard let rooms = ModelsManager.shared.getRooms() else { return nil }
        let visible: [NLRoom] = {
            var visible = [NLRoom]()
            for room in rooms {
                guard room.number != NLRoom.UNDISTRIBUTED_DEVICES_ROOM_NUMBER else { continue }
                visible.append(room)
            }
            return visible
        }()
        return visible
    }
    
    func getRoomWith(number: Int) -> NLRoom? {
        guard let rooms = ModelsManager.shared.getRooms() else { return nil }
        for room in rooms {
            guard room.number == number else { continue }
            return room
        }
        return nil
    }
    
    func getDeviceList() -> [NLBaseDevice]? {
        guard let devices = ModelsManager.shared.getDevices() else { return nil }
        return devices
    }

}

//
//  NLTXDevice.swift
//  newNL
//
//  Created by aggrroo on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

class NLTXDevice: NLBaseDevice {
        
    static let SETTINGS_PARSING_INTERVAL = Range(0...3)
    
    var settings: Data?
    
    init(settings: Data, chanel: Int, deviceType: DeviceType, subType: DeviceSubType) {
        super.init(chanel: chanel, deviceType: deviceType, subType: subType)
        self.settings = settings
    }
    
    
}

//
//  NLAuto.swift
//  newNL
//
//  Created by aggrroo on 22.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

class NLAuto: NLBaseObject {
    
    static let MAX_AUTOS_COUNT = 30
    static let COMMANDS_COUNT = 1
    
    static let FIRST_SETTINGS_BYTE = 4102
    static let FIRST_NAME_BYTE = 6
    static let NAME_RANGE = Range(0...31)
    static let TYPE = 0
    static let STATE = 1
    static let EVENTER_TYPE = 2
    static let EVENTER_CHANEL = 3
    static let PARAMETER = 4
    static let FIRST_COMMAND_BYTE = 5
    static let AUTO_LENGHT = 273
    static let NAME_LENGHT = 32
    static let DERIVED_BYTE = 8197
    
    var autoType: AutoType
    var isActive: Bool
    var eventer: NLRXDevice
    var parameterOrIncomeCommand: Int
    var command: NLCommonCommand
    var number: Int
    
    init(name: String, autoType: AutoType, isActive: Bool, eventer: NLRXDevice, command: NLCommonCommand, parameter: Int, number: Int) {
        self.autoType = autoType
        self.isActive = isActive
        self.eventer = eventer
        self.command = command
        self.parameterOrIncomeCommand = parameter
        self.number = number
        super.init(name: name, type: .nlAuto)
    }
    
    convenience init(model: DetailedAutoViewModel) {
        var parameter = model.parameterOrIncomeCommand
        if parameter == nil {
            if model.eventer?.subType != .control {
                parameter = 21
            } else {
                parameter = 255
            }
        }
        self.init(name: model.name!, autoType: model.autoType!, isActive: model.isActive, eventer: model.eventer!, command: model.command!, parameter: parameter!, number: model.number!)
    }
    
}

class NLAutoList: NLBaseObject {
    
    var autos: [NLAuto]?
    
    init(autos: [NLAuto]) {
        super.init(name: nil, type: .list)
        self.autos = autos
    }
    
    override func toData() -> (Data, Data?) {
        let data = Data.emptyDataWithCount(count: 12288)
        return (data, nil)
    }
    
}

enum AllowedAutoCommands: CaseIterable, Displayable {
    
    var displayName: String {
        switch self {
        case .on:
            return "Включить"
        case .off:
            return "Выключить"
        case .switch:
            return "Переключить"
        case .runScenario:
            return "Вызвать сценарий"
        @unknown default:
            return String.empty
        }
    }
    
    case on, off, `switch`, runScenario, unknown
    
    static var selectable: [AllowedAutoCommands] {
        return [.on, off, .switch, runScenario]
    }
    
    func toBlockCommand() -> PowerBlockCommand {
        switch self {
        case .on:
            return .on
        case .off:
            return .off
        case .switch:
            return .switching
        case .runScenario:
            return .runScenario
        case .unknown:
            return .unknown
        }
    }
    
}

enum AutoType: CaseIterable, Displayable {
    
    case heating
    case cooling
    case draining
    case humidification
    case event
    case unknown
    
    static var choosable: [AutoType] {
        var chosableCases = AutoType.allCases
        chosableCases.removeLast(1)
        return chosableCases
    }
    
    var stringValue: String {
        switch self {
        case .heating:
            return "Термостат нагрев"
        case .cooling:
            return "Термостат охлаждение"
        case .draining:
            return "Гигростат осушение"
        case .humidification:
            return "Гигростат увлажнение"
        case .event:
            return "Событие"
        case .unknown:
            return "Выберите"
        }
    }
    
    var displayName: String {
        return stringValue
    }
    
    var realValue: UInt8 {
        switch self {
        case .heating:
            return 0
        case .cooling:
            return 1
        case .draining:
            return 2
        case .humidification:
            return 3
        case .event:
            return 4
        case .unknown:
            return 255
        }
    }
}

extension UInt8 {
    
    var autoType: AutoType {
        
        switch self {
        case 0: return .heating
        case 1: return .cooling
        case 2: return .draining
        case 3: return .humidification
        case 4: return .event
        default: return .unknown
        }
    }
    
}

extension NLAuto: Displayable {
    
    var displayName: String {
        return name ?? String.empty
    }
    
}

//
//  NLFTXDevice.swift
//  newNL
//
//  Created by aggrroo on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

class NLFTXDevice: NLBaseDevice {
    
    static let ID_PARSING_INTERVAL = Range(0...3)
    
    var id: String?
    var isDimmable: Bool = false
    var currentState: DeviceState?
    var setts: String?
    var stateSettings: Int?
    
    init(id: String?, chanel: Int, deviceType: DeviceType, subType: DeviceSubType) {
        super.init(chanel: chanel, deviceType: deviceType, subType: subType)
        self.id = id
    }
    
}

class Thermostat: NLFTXDevice {
    
    var currentTemperature: Double?
    var heatingTemperature: Double?
    
    override init(id: String?, chanel: Int, deviceType: DeviceType, subType: DeviceSubType) {
        super.init(id: id, chanel: chanel, deviceType: deviceType, subType: subType)
    }
    
}

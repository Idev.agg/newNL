//
//  NLRoom.swift
//  newNL
//
//  Created by Глеб Сергей on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

class NLRoom: NLBaseObject, Displayable {
    
    static let MAX_ROOM_COUNT = 32
    
    static let NAME_PARSING_INTERVAL = Range(0...30)
    static let FINISHED_NAME_BYTE = 31
    static let ICON_BYTE = 32
    static let RESERVED_INTERVAL = Range(33...52)
    static let ROOM_DESCRIBE_BYTES_COUNT = 53
    
    static let FIRST_BYTE = 8774
    static let FINISHED_BYTE = 10469
    
    static let UNDISTRIBUTED_DEVICES_ROOM_NUMBER = 255
    static let UNDISTRIBUTED_DEVICES_ROOM_NAME = "Нераспределенные устройства"

    var number: Int
    var iconIndex: Int?
    
    init(name: String, number: Int, iconIndex: Int? = 255) {
        self.number = number
        self.iconIndex = iconIndex
        super.init(name: name, type: .nlRoom)
    }
    
    var displayName: String {
        return self.name ?? String.empty
    }
    
    func getDeviceCount() -> Int {
        guard let devices = ModelsManager.shared.getDevices() else { return 0 }
        let count: Int? = {
            var count = 0
            for device in devices {
                guard device.attachedRoomNumber == number else { continue }
                count += 1
            }
            return count
        }()
        return count ?? 0
    }
    
    func getDevicesList() -> [NLBaseDevice]? {
        guard let devices = ModelsManager.shared.getDevices() else { return nil }
        let devicesInCurrentRoom: [NLBaseDevice] = {
            var devicesInCurrentRoom = [NLBaseDevice]()
            for device in devices {
                guard device.attachedRoomNumber == number else { continue }
                devicesInCurrentRoom.append(device)
            }
            return devicesInCurrentRoom
        }()
        guard devicesInCurrentRoom.count > 0 else { return nil }
        return devicesInCurrentRoom
    }
    
}

//
//  NLStateObject.swift
//  newNL
//
//  Created by aggrroo on 23.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

class NLStateObject: NLBaseObject {
    
    var userSettingsEditingTime: String?
    
    var devicesInfoList: [Int: DeviceInfo]

    init(list: [Int: DeviceInfo]) {
        devicesInfoList = list
        super.init(name: nil, type: .list)
    }
    
}

class DeviceInfo: NSObject, DataConvertibleItemProtocol, BitConvertibleProtocol {
    
    var state: [Bit]
    var sets: String
    
    init(state: [Bit], sets: String) {
        self.state = state
        self.sets = sets
        super.init()
    }
    
    func toBits(byte: UInt8) -> [Bit] { return [Bit.one] }
    
    func toByte(bits: [Bit]) -> UInt8 { return UInt8() }
   
}

protocol BitConvertibleProtocol: class {
    
    func toBits(byte: UInt8) -> [Bit]
    
    func toByte(bits: [Bit]) -> UInt8
    
}

extension Bit {}



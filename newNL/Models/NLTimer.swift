//
//  NLTimer.swift
//  newNL
//
//  Created by aggrroo on 22.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

class NLTimer: NLBaseObject {
    
    static let MAX_TIMERS_COUNT = 32
    static let COMMANDS_COUNT = 8
    
    static let NAME_DESCRIBE_RANGE = Range(0...30)
    static let NAME_STOP_BYTE = 31
    static let SETTINGS_LENGHT = 119
    static let FIRST_SETTINGS_BYTE = 4102
    static let FIRST_COMMAND_SETTINGS_BYTE = 7
    static let FIRST_NAME_BYTE = 6
    static let NAME_DESCRIBE_LENGHT = 32
    
    static let TYPE = 0
    static let STATE = 1
    static let ON_TIME_H = 2
    static let ON_TIME_M = 3
    static let OFF_TIME_H = 4
    static let OFF_TIME_M = 5
    static let DAYS = 6
    
    var number: Int?
    var timerType: TimerType?
    var commands: [NLCommonCommand]?
    var isActive: Bool?
    var onTime: NLTimeObject?
    var offTime: NLTimeObject?
    var calendar: NLCalendar?
    
    init(name: String, timerType: TimerType, commands: [NLCommonCommand], isActive: Bool, onTime: NLTimeObject, offTime: NLTimeObject, calendar: NLCalendar, number: Int) {
        super.init(name: name, type: .nlTimer)
        self.commands = commands
        self.isActive = isActive
        self.onTime = onTime
        self.offTime = offTime
        self.calendar = calendar
        self.number = number
        self.timerType = timerType
    }
    
    init(model: NLTimerModel) {
        super.init(name: model.name, type: .nlTimer)
        name = model.name
        number = model.number
        timerType = model.timerType
        commands = model.commands
        isActive = model.isActive
        onTime = model.calendarObj?.onTime
        offTime = model.calendarObj?.offTime
        calendar = model.calendarObj?.calendar
    }
    
    func timesCount() -> Int {
        guard let timerType = timerType else { return 0 }
        switch timerType {
        case .onAndOff, .onAndOffAuto: return 2
        default: return 1
        }
    }
    
}

class NLTimerModel: NSObject {
    
    var name: String?
    var number: Int?
    var timerType: TimerType?
    var commands: [NLCommonCommand]?
    var isActive: Bool?
    var calendarObj: ComplexCalendarObject?
    
    init(timer: NLTimer?) {
        super.init()
        guard let timer = timer else {
            var completedNumbers = [Int]()
            if let timers = ModelsManager.shared.getTimers(), timers.count > 0 {
                for timer in timers {
                    completedNumbers.append(timer.number ?? 0)
                }
            }
            var timerNumber = 0
            if completedNumbers.count > 0 {
                timerNumber = {
                    for i in 0..<NLTimer.MAX_TIMERS_COUNT {
                        if !completedNumbers.contains(i) {
                            return i
                        }
                    }
                    return 0
                }()
            }
            self.number = timerNumber
            return
        }
        name = timer.name
        number = timer.number
        timerType = timer.timerType
        commands = timer.commands
        isActive = timer.isActive
        calendarObj = ComplexCalendarObject(onTime: timer.onTime, offTime: timer.offTime, calendar: timer.calendar, type: timer.timerType ?? .unknown)
    }
}

class NLTimerList: NLBaseObject {
    
    var timers: [NLTimer]?
    
    init(timers: [NLTimer]) {
        super.init(name: nil, type: .list)
        self.timers = timers
    }
}

struct NLTimeObject {
    
    var hours: Int?
    var minutes: Int?
    
    static var `default` = NLTimeObject(hours: 0, minutes: 0)
    
    init(hours: Int, minutes: Int) {
        self.hours = hours
        self.minutes = minutes
    }
    
    func getStringTime() -> String {
        guard let hours = hours, let minutes = minutes else { return "-:-" }
        return String(format: "%02d:%02d", arguments: [hours, minutes])
    }
}

struct Day {
    
    static let DAYS_COUNT = 7
    static let BITS_COUNT = 8
    
    var value: Bit?
    
    init(bit: Bit) {
        value = bit
    }
    
}

struct NLCalendar {
    
    static let DAYS_SHORT_NAMES = ["Пн", "Вт", "Ср", "Чт", "Пт", "Сб", "Вс"]
    
    static var `defaultValue` = NLCalendar(bits: [Bit.zero, Bit.zero, Bit.zero, Bit.zero, Bit.zero, Bit.zero, Bit.zero, Bit.zero])
    
    var days: [Day]?
    
    init(bits: [Bit]) {
        var bits = bits
        bits.removeFirst()
        days = [Day]()
        for bit in bits {
            days?.append(Day(bit: bit))
        }
    }
    
    var realValue: UInt8 {
        var bits = UInt8(0).toBits(count: 8)
        if let days = days {
            for i in 0..<7 {
                bits[i + 1] = days[i].value ?? Bit.zero
            }
        }
        return Array.bitsToBytes(bits: bits.reversed()).first ?? Data.nullByte
    }
    
}

class TimerTypeConverter {
    
    init() {}
    
    func toType(mode: UInt8, type: UInt8, command: PowerBlockCommand) -> TimerType {
        switch type {
        case 0:
            switch mode {
            case 0, 2:
                switch command {
                case .on:  return .on
                case .off: return .off
                default:  return .unknown
                }
            case 252: return .offAuto
            case 253: return .onAuto
            case 254: return .runScenario
            default: return .unknown
            }
        case 1:
            switch mode {
            case 0, 2: return .onAndOff
            case 253: return .onAndOffAuto
            default: return .unknown
            }
        default: return .unknown
        }
    }
    
    func fromType(type: TimerType) -> (mode: UInt8, type: UInt8, command: PowerBlockCommand) {
        return (UInt8(), UInt8(), PowerBlockCommand.unknown)
    }
    
}

enum TimerType: CaseIterable {
    case on
    case off
    case onAndOff
    case onAuto
    case offAuto
    case onAndOffAuto
    case runScenario
    case sunrise
    case dusk
    case unknown
}

extension TimerType: Displayable {
    
    static var choosable: [TimerType] {
        var allCases = TimerType.allCases
        allCases.removeLast()
        return allCases
    }
    
    var displayName: String {
        switch self {
        case .on:
            return "Включить"
        case .off:
            return "Выключить"
        case .onAndOff:
            return "Включить и выключить"
        case .onAuto:
            return "Включить автоматизацию"
        case .offAuto:
            return "Выключить автоматизацию"
        case .onAndOffAuto:
            return "Включить и выключить автоматизацию"
        case .runScenario:
            return "Вызвать сценарий"
        case .sunrise:
            return "Рассвет"
        case .dusk:
            return "Закат"
        case .unknown:
            return String.empty
        }
    }
 
}

//
//  NLRXDevice.swift
//  newNL
//
//  Created by aggrroo on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

class NLRXDevice: NLBaseDevice {
    
    private static let EMPTY_DEVICE_CHANEL = 65
    
    override init(chanel: Int, deviceType: DeviceType, subType: DeviceSubType) {
        super.init(chanel: chanel, deviceType: deviceType, subType: subType)
    }
    
    static func empty() -> NLRXDevice {
        return NLRXDevice(chanel: NLRXDevice.EMPTY_DEVICE_CHANEL, deviceType: .rx, subType: .unknown)
    }
    
}

class TemperatureSensor: NLRXDevice {
    
    var temperature: Double?
    
    override init(chanel: Int, deviceType: DeviceType, subType: DeviceSubType) {
        super.init(chanel: chanel, deviceType: deviceType, subType: subType)
    }
    
}

class TemperatureAndHumiditySensor: TemperatureSensor {
    
    var humidity: Double?
    
    override init(chanel: Int, deviceType: DeviceType, subType: DeviceSubType) {
        super.init(chanel: chanel, deviceType: deviceType, subType: subType)
    }
    
}

class Sensor: NLRXDevice {
    override init(chanel: Int, deviceType: DeviceType, subType: DeviceSubType) {
        super.init(chanel: chanel, deviceType: deviceType, subType: subType)
    }
}


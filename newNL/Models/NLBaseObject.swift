//
//  BaseObject.swift
//  newNL
//
//  Created by aggrroo on 22.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

class NLBaseObject: NSObject, DataConvertibleItemProtocol {
    
    var name: String?
    var type: BaseObjectType
    
    init(name: String?, type: BaseObjectType) {
        self.name = name
        self.type = type
    }
    
    func toData() -> (Data, Data?) {
        return (Data(), nil)
    }
    
}

enum BaseObjectType {
    case nlHouse
    case nlRoom
    case nlDevice
    case nlTimer
    case nlScenario
    case nlAuto
    case list
    case commandResponse
    case attachObj
    case rxSetObj
}

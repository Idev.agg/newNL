//
//  NLScenario.swift
//  newNL
//
//  Created by aggrroo on 22.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

class NLScenario: NLBaseObject, Displayable {
    
    static let MAX_SCENARIOUS_COUNT = 32
    static let SCENARIO_LENGHT = 1024
    static let MAX_COMMANDS_COUNT = 73
    static let FIRST_BYTE = 6
    static let FIRST_USER_PROPERTIES_BYTE = 10470
    static let NAME_STOP_BYTE = 31
    static let NAME_RANGE = 0...30
    static let ICON = 32
    static let USER_PROPERTIES_LENGHT = 33
    
    var iconIndex: Int?
    var number: Int
    var commands: [NLCommonCommand]
    
    init(number: Int, commands: [NLCommonCommand]) {
        self.number = number
        self.commands = commands
        super.init(name: nil, type: .nlScenario)
    }
    
    var displayName: String {
        return name ?? String.empty
    }
    
    func runCommand() -> String {
        let cmd = String(format: "%02X", 10)
        let hexNum = String(format: "%02X", number)
        return String(format: "01%@%@0000000000000000000000", cmd, hexNum)
    }
    
}

class NLScenarioList: NLBaseObject {
    
    var scenarious: [NLScenario]?

    init(scenarious: [NLScenario]) {
        super.init(name: nil, type: .list)
        self.scenarious = scenarious
    }
    
}

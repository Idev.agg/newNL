//
//  NLBaseDevice.swift
//  newNL
//
//  Created by Глеб Сергей on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

class NLBaseDevice: NLBaseObject {
    
    static let MAX_DEVICES_COUNT = 64
    
    static let TX_START_BYTE = 6
    static let RX_START_BYTE = 1030
    static let FTX_START_BYTE = 2054

    // user properties
    
    static let NAME_PARSING_INTERVAL = Range(0...30)
    static let FINISHED_NAME_BYTE = 31
    static let ICON_INDEX_BYTE = 32
    static let ATTACHED_ROOM_NUMBER_BYTE = 33
    static let DEVICE_DESCRIBE_LENGHT_USER_PROPERTIES = 34
    
    static let TX_START_USER_PROPERTIES = 6
    static let RX_START_USER_PROPERTIES = 2182
    static let FTX_START_USER_PROPERTIES = 4358
    
    // technic properties
    
    static let DEVICE_DESCRIBE_LENGHT = 8
    static let SUBTYPE_BYTE = 4
    static let SOFT_VERSION_BYTE = 5
    static let CURRENT_SETTINGS_BYTE = 6
    static let RESERVED_BYTE = 7
    
    private static let SHORT_NAME_SEPARATOR = " - "
    
    var chanel: Int
    var attachedRoomNumber: Int?
    var iconIndex: Int?
    var deviceType: DeviceType
    var subType: DeviceSubType
    
    init(chanel: Int, deviceType: DeviceType, subType: DeviceSubType) {
        self.chanel = chanel
        self.subType = subType
        self.deviceType = deviceType
        super.init(name: nil, type: .nlDevice)
    }
    
    func addProperties(name: String?, iconIndex: Int?, attachedRoomNumber: Int?) {
        self.name = name
        self.iconIndex = iconIndex
        self.attachedRoomNumber = attachedRoomNumber
    }
    
    func getShortName() -> String {
        return getRoomName() + NLBaseDevice.SHORT_NAME_SEPARATOR + (name ?? String.empty)
    }
    
    func getHexChanel() -> String { return String(chanel, radix: 16) }
    
    func getRoomName() -> String {
        guard attachedRoomNumber != NLRoom.UNDISTRIBUTED_DEVICES_ROOM_NUMBER else {
            return NLRoom.UNDISTRIBUTED_DEVICES_ROOM_NAME
        }
        guard let roomNumber = attachedRoomNumber, let house = ModelsManager.shared.getHouse(), let room = house.getRoomWith(number: roomNumber), let roomName = room.name else { return String.empty }
        return roomName
    }
    
    func prepareCommandStr(command: PowerBlockCommand) -> String {
        var hexChannel = String(format:"%02X", chanel)
        var id = "00000000"
        var deviceIdentifier = "0000"
        if self is NLFTXDevice {
            id = (self as! NLFTXDevice).id ?? id
            deviceIdentifier = "0208"
            hexChannel = "00"
        }
        return String(format: "00%@00%@%@0000000000%@", deviceIdentifier, hexChannel, command.hexCmd, id)
    }

}

extension NLBaseDevice: Displayable {
    
    var displayName: String {
        return getShortName()
    }
    
}

class NLBaseDevicesList: NLBaseObject {
    
    var devices: [NLBaseDevice]?
    
    init(devices: [NLBaseDevice]) {
        super.init(name: nil, type: .list)
        self.devices = devices
    }
    
    func getDevice(type: DeviceType, chanel: Int?, id: String?) -> NLBaseDevice? {
        guard let devices = devices else { return nil }
        switch type {
        case .tx:
            for device in devices {
                guard device.deviceType == .tx, device.chanel == chanel else { continue }
                return device
            }
            return nil
        case .rx:
            for device in devices {
                guard device.deviceType == .rx, device.chanel == chanel else { continue }
                return device
            }
            return nil
        case .ftx:
            for device in devices {
                guard let device =  device as? NLFTXDevice, device.id == id else { continue }
                return device
            }
            return nil
        default:
            return nil
        }
    }
    
}

extension Array where Element: NLBaseDevice {
    
    var list: NLBaseDevicesList {
        return NLBaseDevicesList(devices: self)
    }
    
}

enum PowerBlockCommand {
    case on
    case off
    case switching
    case temporaryOn
    case setBrightness
    case runScenario
    case unknown
}

extension PowerBlockCommand {
    
    var hexCmd: String {
        switch self {
        case .on:
            return String(format: "%02X", 2)
        case .off:
            return String(format: "%02X", 0)
        case .switching:
            return String(format: "%02X", 4)
        default:
            return String.empty
        }
    }
    
    func toAutoCommand() -> AllowedAutoCommands {
        switch self {
        case .on:
            return .on
        case .off:
            return .off
        case .switching:
            return .switch
        case .runScenario:
            return .runScenario
        case .temporaryOn, .setBrightness, .unknown:
            return .off
        }
    }
    
    var realValue: UInt8 {
        switch self {
        case .on:
            return 2
        case .off:
            return 0
        case .switching:
            return 4
        case .temporaryOn:
            return 255
            #warning("!!!")
        case .setBrightness:
            return 255
        case .runScenario:
            return 254
        case .unknown:
            return 255
        }
    }
    
}

extension UInt8 {
    
    var command: PowerBlockCommand {
        switch self {
        case 0: return  .off
        case 2: return  .on
        case 4: return  .switching
        case 254: return .runScenario
        default: return .unknown
        }
    }
    
}

enum ControllCommands: Displayable {
    
    case on, off, `switch`, brightDown, brightUp, brightBack, brightSet, runScenario, setScenario, stopSetting, anyCommand
    
    static var selectable: [ControllCommands] {
        return [ControllCommands]()
    }
    
    var displayName: String {
        switch self {
        case .on:
            return "Включить"
        case .off:
            return "Выключить"
        case .switch:
            return "Переключить"
        case .brightDown:
            return "Яркость вниз"
        case .brightUp:
            return "Яркость вверх"
        case .brightBack:
            return "Яркость назад"
        case .brightSet:
            return "Яркость задать"
        case .runScenario:
            return "Вызвать сценарий"
        case .setScenario:
            return "Записать сценарий"
        case .stopSetting:
            return "Остановить регулировку"
        case .anyCommand:
            return "Любая команда"
        }
    }
    
    var realValue: UInt8 {
        switch self {
        case .on:
            return 2
        case .off:
            return 0
        case .switch:
            return 4
        case .brightDown:
            return 1
        case .brightUp:
            return 3
        case .brightBack:
            return 5
        case .brightSet:
            return 6
        case .runScenario:
            return 7
        case .setScenario:
            return 8
        case .stopSetting:
            return 10
        case .anyCommand:
            return 255
        }
    }
    
}



enum sensorsCommands {
    case on
    case off
    case lowBattery
    case sensorData
}

//
//  Error.swift
//  newNL
//
//  Created by aggrroo on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

protocol NLErrorProtocol: class {
    
    var code: Int { get set }
    var discription: String { get set }
    
}

protocol DataConvertibleItemProtocol: class {}

class Result_<T: NLBaseObject, error: CommonError> {
    
    var value: NLBaseObject?
    var error: CommonError?
    
    init(value: NLBaseObject?, error: CommonError?) {
        self.value = value
        self.error = error
    }
}

class CommandResponse : NLBaseObject {
    
    var success: Bool
    
    private static var successData: Data {
        var data = Data()
        data.append(contentsOf: [UInt8(79), UInt8(75)])
        return data
    }
    
    init(data: Data) {
        success = String(data: data, encoding: .utf8) == "OK"
        super.init(name: nil, type: .commandResponse)
    }
    
    static func success() -> CommandResponse {
        return CommandResponse(data: successData)
    }
}

class CommonError: NLErrorProtocol, Equatable {
    
    static let defaultCode = 5000
    
    static let baseError = CommonError(error: .occuredAnErrorTryAgain)
    static let success = CommonError(error: .withoutError)
    
    var code: Int
    var discription: String
    
    init(code: Int, discription: String) {
        self.code = code
        self.discription = discription
        debugErrorMessage(error: self)
    }
    
    convenience init(error: ErrorDiscriptionsWithCodes) {
        self.init(code: error.code, discription: error.discription)
    }
    
    func isSuccessful() -> Bool {
        return self == CommonError.success
    }
    
    static func == (lhs: CommonError, rhs: CommonError) -> Bool {
        return lhs.code == rhs.code || lhs.discription == rhs.discription
    }
    
}

class ApiError: CommonError {
    
    var handler: (() -> Void)?
    
    override init(code: Int, discription: String) {
        super.init(code: code, discription: discription)
    }
    
    convenience init(code: Int) {
        self.init(code: code, discription: code.error.discription)
    }
    
}

class ParsingError: CommonError {}

class ConvertingBackError: CommonError {}

class StorageError: CommonError {}

class RequestError: CommonError {}

class AttachmentError: CommonError {}

enum ErrorDiscriptionsWithCodes {
    // WITHOUT ERROR
    case withoutError // 0
    //COMMON
    case occuredAnErrorTryAgain // 7001
    // API DOWNLOAD/UPLOAD
    case unknownApiError // 7301
    case notCompletlieDataApiError // 7302
    case noConnectionApiError // 7303
    case notFoundControllerApiError // 7304
    case incorrectLoginApiError // 7305
    case canNotDownloadData // 7306
    case canNotUploadData // 7307
    case incorrectUrl // 7308
    case requiredLogin // 7309
    // PARSING
    case unknownData // 7101
    // CONVERT BACK
    case canNotConvertObjectToData // 7201
    // BASE
    // DATA STORAGE
    case unknownDataStorageError // 7401
    case noSuchFile // 7402
    case noData // 7403
    // REQUEST
    case alreadyInTurn // 7501
    // FIELDS
    case emptyField // 7601
    case alsoUsedName //7602
    // ATTACHMENT
    case ftxAlreadyAttached // 7701
    case attachmentError // 7702
    case fullFtxAttached // 7703
    
    case emptyBlock // 7801
    case emptyScenario // 7802
    case emptyEventer // 7803
    case emptyAuto // 7804
}

extension ErrorDiscriptionsWithCodes {
    
    var discription: String {
        switch self {
            // COMMON ERROR
        case .occuredAnErrorTryAgain: return "COMMON - Произошла ошибка. Повторите еще раз."
            // WITHOUT ERROR
        case .withoutError: return "COMMON - Ошибок нет."
            // API DOWNLOAD/UPLOAD
        case .unknownApiError: return "API - Неизвестная ошибка."
        case .notCompletlieDataApiError: return "API - Неполные данные."
        case .noConnectionApiError: return "API - Отсутствует соединение."
        case .notFoundControllerApiError: return "API - Не удалось обнаружить контроллер."
        case .incorrectLoginApiError: return "API - Ошибка авторизации."
        case .requiredLogin: return "API - Необходимо авторизоваться."
        case .canNotDownloadData: return "API - Не удалось получить данные."
        case .canNotUploadData: return "API - Не удалось отправить данные."
        case .incorrectUrl: return "API - Неверный URL."
            // PARSING
        case .unknownData: return "DATA - Неизвестные данные."
            // CONVERT BACK
        case .canNotConvertObjectToData: return "DATA - Ошибка конвертации обьектов в бинарный файл."
            // DATA STORAGE
        case .unknownDataStorageError: return "DATA - Неизвестная ошибка хранения данных."
        case .noSuchFile: return "DATA - Файл не найден."
        case .noData: return "DATA - Нет данных."
            // REQUEST
        case .alreadyInTurn: return "REQUEST - Уже в очереди."
            //FIEDLS
        case .emptyField: return "Заполните поле"
        case .ftxAlreadyAttached: return "Устройство было привязано ранее"
        case .attachmentError: return "Ошибка при привязке"
        case .fullFtxAttached: return "Вы уже привязали максимальное колличество устройств noolite-F(64 устройства)"
        case .alsoUsedName : return "Имя уже используется"
        case .emptyBlock: return "Выберите блок"
        case .emptyScenario: return "Выберите сценарий"
        case .emptyEventer: return "Выберите датчик/пульт"
        case .emptyAuto: return "Выберите автоматизацию"
        }
    }
    
    var code: Int {
        switch self {
            // WITHOUT ERROR
        case .withoutError: return 0
            // COMMON
        case .occuredAnErrorTryAgain: return 7001
            // PARSING
        case .unknownData: return 7101
            // CONVERT BACK
        case .canNotConvertObjectToData: return 7201
            // API DOWNLOAD/UPLOAD
        case .unknownApiError: return 7301
        case .notCompletlieDataApiError: return 7302
        case .noConnectionApiError: return 7303
        case .notFoundControllerApiError: return 7304
        case .incorrectLoginApiError: return 7305
        case .canNotDownloadData: return 7306
        case .canNotUploadData: return 7307
        case .incorrectUrl: return 7308
        case .requiredLogin: return 7309
            // DATA STORAGE
        case .unknownDataStorageError: return 7401
        case .noSuchFile: return 7402
        case .noData: return 7403
            // REQUEST
        case .alreadyInTurn: return 7501
            // FIELDS
        case .emptyField: return 7601
            // ATTACHMENT
        case .ftxAlreadyAttached: return 7701
        case .attachmentError: return 7702
        case .fullFtxAttached: return 7703
        case .alsoUsedName: return 7602
        case .emptyBlock: return 7801
        case .emptyScenario: return 7802
        case .emptyEventer: return 7803
        case .emptyAuto: return 7804
        }
    }
    
}

extension Int {
    
    var error: ErrorDiscriptionsWithCodes {
        switch self {
            // WITHOUT ERROR
        case 0: return .withoutError
            // COMMON
        case 7001: return .occuredAnErrorTryAgain
            // PARSING
        case 7101: return .unknownData
            // CONVERT BACK
        case 7201: return .canNotConvertObjectToData
            // API DOWNLOAD/UPLOAD
        case 7301: return .unknownApiError
        case 7302: return .notCompletlieDataApiError
        case 7303: return .noConnectionApiError
        case 7304: return .notFoundControllerApiError
        case 7305: return .incorrectLoginApiError
        case 7306: return .canNotDownloadData
        case 7307: return .canNotUploadData
        case 7308: return .incorrectUrl
        case 7309: return .requiredLogin
            // DATA STORAGE
        case 7401: return .unknownDataStorageError
        case 7402: return .noSuchFile
        case 7403: return .noData
            // REQUEST
        case 7501: return .alreadyInTurn
            //FIELDS
        case 7601: return .emptyField
        case 7602: return .alsoUsedName
            //ATTACHMENT
        case 7701: return .ftxAlreadyAttached
        case 7702: return .attachmentError
        case 7703: return .fullFtxAttached
            // DEFAULT
        case 7801: return .emptyBlock
        case 7802: return .emptyScenario
        case 7803: return .emptyEventer
        default: return .occuredAnErrorTryAgain
        }
    }
}


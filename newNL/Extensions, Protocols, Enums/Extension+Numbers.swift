//
//  Extension+Numbers.swift
//  newNL
//
//  Created by aggrroo on 4/26/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

extension Int: Displayable {
    
    static func array(first: Int, count: Int) -> [Int] {
        var arr = [Int]()
        for i in first..<first + count {
            arr.append(i)
        }
        return arr
    }
    
    var displayName: String {
        return "\(self)"
    }
    
}

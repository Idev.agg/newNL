//
//  Extension AutoLayout.swift
//  newNL
//
//  Created by aggrroo on 2/7/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

extension UIView {
    
    func addSubviewWithZeroConstraintsToSuperview(subview: UIView) {
        addSubviewWithConstraintsToSuperview(subview: subview, top: 0, left: 0, right: 0, bottom: 0)
    }
    
    func addSubviewWithConstraintsToSuperview(subview: UIView, top: CGFloat, left: CGFloat, right: CGFloat, bottom: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(subview: subview)
        let constants = [top, left, -right, -bottom]
        let attributes: [NSLayoutConstraint.Attribute] = [.top, .left, .right, .bottom]
        for i in 0..<attributes.count {
            addConstraint(NSLayoutConstraint(item: subview, attribute: attributes[i], relatedBy: .equal, toItem: self, attribute: attributes[i], multiplier: 1, constant: constants[i]))
        }
    }
    
    func setHeight(_ height: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: self, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height))
    }
    
    func setWidth(_ width: CGFloat) {
        translatesAutoresizingMaskIntoConstraints = false
        addConstraint(NSLayoutConstraint(item: self, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: width))
    }
    
    private func addSubview(subview: UIView) {
        translatesAutoresizingMaskIntoConstraints = false
        subview.translatesAutoresizingMaskIntoConstraints = false
        addSubview(subview)
    }
    
    func addSubviewWith(subview: UIView, attributes: [NSLayoutConstraint.Attribute], values: [CGFloat]) {
        translatesAutoresizingMaskIntoConstraints = false
        guard attributes.count == values.count else { return }
        addSubview(subview)
        for i in 0..<attributes.count {
            addConstraint(NSLayoutConstraint(item: subview, attribute: attributes[i], relatedBy: .equal, toItem: self, attribute: attributes[i], multiplier: 1, constant: values[i]))
        }
    }
    
//    func setEqualCenterY(subview: UIView, multiplier: CGFloat = 1) {
//        translatesAutoresizingMaskIntoConstraints = false
//        guard subviews.contains(subview) else { return }
//        subview.centerYAnchor.constraint(equalToSystemSpacingBelow: self.centerYAnchor, multiplier: multiplier)
//    }
    
    func addSubviewWithZero(subview:UIView, attributes: [NSLayoutConstraint.Attribute], notStrongAttributes: [NSLayoutConstraint.Attribute]? = nil) {
        translatesAutoresizingMaskIntoConstraints = false
        addSubview(subview: subview)
        for attribute in attributes {
            addConstraint(NSLayoutConstraint(item: subview, attribute: attribute, relatedBy: .equal, toItem: self, attribute: attribute, multiplier: 1, constant: 0))
        }
        guard let notStrongAttributes = notStrongAttributes else { return }
        for notStrongAttibute in notStrongAttributes {
            addConstraint(NSLayoutConstraint(item: subview, attribute: notStrongAttibute, relatedBy: .greaterThanOrEqual, toItem: self, attribute: notStrongAttibute, multiplier: 1, constant: 0))
        }
    }
    
}

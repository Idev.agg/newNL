//
//  Extension+UITextField.swift
//  newNL
//
//  Created by aggrroo on 4/26/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

protocol Indexing {
    var indexPath: IndexPath? { get set }
}

extension UITextField: Indexing {
    
    struct StoredIndexPath {
        static var indexPath: IndexPath?
    }
    
    var indexPath: IndexPath? {
        get {
            return StoredIndexPath.indexPath
        }
        set {
            StoredIndexPath.indexPath = newValue
        }
    }
    
}

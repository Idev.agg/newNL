//
//  Extension UIColor + CGColor.swift
//  JUNO
//
//  Created by Sergey on 12/12/18.
//  Copyright © 2018 elatesoftware. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        assert(red >= 0 && red <= 255, "Invalid red component")
        assert(green >= 0 && green <= 255, "Invalid green component")
        assert(blue >= 0 && blue <= 255, "Invalid blue component")
        self.init(red: CGFloat(red) / 255.0, green: CGFloat(green) / 255.0, blue: CGFloat(blue) / 255.0, alpha: 1.0)
    }
    
    convenience init(rgb: Int) {
        self.init(
            red: (rgb >> 16) & 0xFF,
            green: (rgb >> 8) & 0xFF,
            blue: rgb & 0xFF
        )
    }
    
    static let alpha87: CGFloat = 0.87
    static let alpha60: CGFloat = 0.6
    static let alpha33: CGFloat = 0.33
}

extension CGColor {
    
    func withAlpha(_ a: CGFloat) -> CGColor {
        return UIColor(cgColor: self).withAlphaComponent(a).cgColor
    }
    
}

enum Colors {
    
    case white, black, none, greenLight, greenDark, red, gray, grayAA
    
    var uiValue: UIColor {
        switch self {
        case .white: return UIColor(rgb: 0xFFFFFF)
        case .black: return UIColor(rgb: 0x000000)
        case .none: return UIColor.clear
        case .greenLight: return UIColor(rgb: 0x98CB78)
        case .greenDark: return UIColor(rgb: 0x79B054)
        case .red: return UIColor(rgb: 0xD16967)
        case .gray: return UIColor(rgb: 0xC8C8C8)
        case .grayAA: return UIColor(rgb: 0xAAAAAA)
        }
    }
}



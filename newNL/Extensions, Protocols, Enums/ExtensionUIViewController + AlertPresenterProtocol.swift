//
//  ExtensionUIViewController + AlertPresenterProtocol.swift
//  newNL
//
//  Created by Глеб Сергей on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController: AlertPresenterProtocol {
    
    func presentSimpleAlert(with title: String?, text: String?, acceptTitle: String?, acceptHandler: (() -> ())?) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: acceptTitle, style: .default, handler: { (action) in
            acceptHandler?()
        }))
        alert.view.tintColor = .green
//        if let visualEffectView = alert.view.searchVisualEffectView() {
//
//            visualEffectView.effect = UIBlurEffect(style: .dark)
//        }
        present(alert, animated: true, completion: nil)
    }
    
    func presentSimpleAlert(with title: String?, text: String?, acceptTitle: String?, cancelTitle: String?, acceptHandler: (() -> ())?, cancelHandler: (() -> ())?) {
        let alert = UIAlertController(title: title, message: text, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: acceptTitle, style: .default, handler: { (action) in
            acceptHandler?()
        }))
        alert.addAction(UIAlertAction(title: cancelTitle, style: .default, handler: { (action) in
            cancelHandler?()
        }))
        alert.view.tintColor = .green
//        if let visualEffectView = alert.view.searchVisualEffectView() {
//            visualEffectView.effect = UIBlurEffect(style: .dark)
//        }
        present(alert, animated: true, completion: nil)
    }
    
    func presentAlertWithError(error: CommonError, handler: (() -> ())?) {
        let alert = UIAlertController(title: "Ошибка", message: error.discription + " - \(error.code)", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ок", style: .default, handler: { (action) in
            handler?()
        }))
        alert.view.tintColor = .green
//        if let visualEffectView = alert.view.searchVisualEffectView() {
//            visualEffectView.effect = UIBlurEffect(style: .dark)
//        }
        present(alert, animated: true, completion: nil)
    }
    
    static func presentErrorAlertOnCurrentScreen(error: CommonError, handler: (() -> ())?) {
        guard let controller = AppDelegate.appDelegate().currentController else {
            handler?()
            return
        }
        controller.presentAlertWithError(error: error, handler: handler)
    }
    
}

extension UIView {
    
    func searchVisualEffectView() -> UIVisualEffectView? {
        if let visualEffectView = self as? UIVisualEffectView {
            return visualEffectView
        } else {
            for subview in subviews {
                if let founded = subview.searchVisualEffectView() { return founded }
            }
        }
        return nil
    }
    
}

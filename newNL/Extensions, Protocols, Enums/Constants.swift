//
//  Constants.swift
//  newNL
//
//  Created by aggrroo on 19.10.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

let BORDER_WIDTH: CGFloat = 1
let CORNER_RADIUS: CGFloat = 6

//SIZES

let size0: CGFloat = 0
let zeroConstant: CGFloat = 0
let size5: CGFloat = 5
let size10: CGFloat = 10
let size13: CGFloat = 13
let size15: CGFloat = 15
let size16: CGFloat = 16
let size20: CGFloat = 20
let size30: CGFloat = 30
let size40: CGFloat = 40
let size50: CGFloat = 50
let size120: CGFloat = 120


//COLORS

let red: UIColor = Colors.red.uiValue
let lightGreen: UIColor = Colors.greenLight.uiValue
let darkGreen: UIColor = Colors.greenDark.uiValue
let black: UIColor = Colors.black.uiValue
let white: UIColor = Colors.white.uiValue
let gray: UIColor = Colors.gray.uiValue
let grayAA: UIColor = Colors.grayAA.uiValue
let clear: UIColor = Colors.none.uiValue

let HIGHLITED_ALPHA: CGFloat = 0.5
let SIMPLE_APLHA: CGFloat = 1
let ALPHA_60: CGFloat = 0.6
let ALPHA_40: CGFloat = 0.4
let ALPHA_20: CGFloat = 0.2

let BASE_ANIMATION_DURATION = 0.3

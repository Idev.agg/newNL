//
//  Extension Layers.swift
//  JUNO
//
//  Created by Sergey on 12/12/18.
//  Copyright © 2018 elatesoftware. All rights reserved.
//

import UIKit

extension CAGradientLayer {
    
    static let gradientStart = CGPoint(x: 0, y: 1)
    static let gradientFinish = CGPoint(x: 1, y: 0)
    
    func becomeDiagonaled() {
        startPoint = CAGradientLayer.gradientStart
        endPoint = CAGradientLayer.gradientFinish
    }
    
}

extension CAGradientLayer {
    
    func colorOfPoint(point:CGPoint) -> CGColor {
        var pixel: [CUnsignedChar] = [0, 0, 0, 0]
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let context = CGContext(data: &pixel, width: 1, height: 1, bitsPerComponent: 8, bytesPerRow: 4, space: colorSpace, bitmapInfo: bitmapInfo.rawValue)
        context!.translateBy(x: -point.x, y: -point.y)
        render(in: context!)
        let red: CGFloat   = CGFloat(pixel[0]) / 255.0
        let green: CGFloat = CGFloat(pixel[1]) / 255.0
        let blue: CGFloat  = CGFloat(pixel[2]) / 255.0
        let alpha: CGFloat = CGFloat(pixel[3]) / 255.0
        let color = UIColor(red:red, green: green, blue:blue, alpha:alpha)
        return color.cgColor
    }
}

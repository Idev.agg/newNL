//
//  StringExtension.swift
//  newNL
//
//  Created by aggrroo on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

extension String: Displayable {
    
    var displayName: String {
        return self
    }
    
}

extension String {
    
    static var space: String { return " " }
    static var empty: String { return "" }
    
    var attributed: NSMutableAttributedString { return NSMutableAttributedString(string: self) }
    
    var attributedThin24: NSMutableAttributedString {
        return NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 24, weight: .thin)])
    }
    
    var attributedLight16: NSMutableAttributedString {
        return NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16, weight: .light)])
    }
    
    func attributedString(font: UIFont, color: UIColor) -> NSMutableAttributedString {
        return NSMutableAttributedString(string: self, attributes: [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: color])
    }
    
}

extension NSMutableAttributedString {
        
    static func getImageSting(image: UIImage) -> NSMutableAttributedString {
        let imageAttachment = NSTextAttachment()
        let font = UIFont.systemFont(ofSize: size16, weight: .regular)
        let ratio = image.size.width / image.size.height
        imageAttachment.image = image
        imageAttachment.bounds = CGRect(x: size0, y: (font.capHeight - size20).rounded() / 2, width: size20 * ratio, height: size20)
        return NSMutableAttributedString(attachment: imageAttachment)
    }

    static func getSingleImageString(image: UIImage) -> NSMutableAttributedString {
        let imageAttachment = NSTextAttachment()
        let ratio = image.size.width / image.size.height
        imageAttachment.image = image
        imageAttachment.bounds = CGRect(x: size0, y: size0, width: size20 * ratio, height: size20)
        return NSMutableAttributedString(attachment: imageAttachment)
    }
    
}

extension NSAttributedString {
    
    func getWidth(withConstrainedHeight height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return ceil(boundingBox.width)
    }
    
}

extension String {
    
    func getPrefix() -> Prefixes {
        switch self {
        case "PRF64U": return .userBin
        case "PRF64D": return .deviceBin
        case "PRF64T": return .timerBin
        case "PRF64P": return .presetBin
        case "PRF64A": return .autoBin
        case "PRF64S": return .settingsBin
        default: return .unknown
        }
    }
    
    func toDataWithCount(count: Int) -> Data {
        guard let stringData = self.data(using: .windowsCP1251) else {
            return Data.emptyDataWithCount(count: count)
        }
        if stringData.count > count {
            return stringData[0..<count]
        } else {
            var data = Data()
            data.append(stringData)
            data.append(Data.emptyDataWithCount(count: count - stringData.count))
            return data
        }
    }
    
}

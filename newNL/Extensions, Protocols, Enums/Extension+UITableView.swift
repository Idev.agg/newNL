//
//  Extension+UITableView.swift
//  newNL
//
//  Created by aggrroo on 4/23/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

extension UITableView {
    
    static func tableViewWith(delegate: Any, backgroundColor: UIColor? = white) -> UITableView {
        let tableView = UITableView()
        tableView.delegate = delegate as? UITableViewDelegate
        tableView.dataSource = delegate as? UITableViewDataSource
        tableView.backgroundColor = backgroundColor
        tableView.separatorStyle = .none
        return tableView
    }
    
    func setAndLayoutTableHeaderView(header: UIView) {
        self.tableHeaderView = header
        header.setNeedsLayout()
        header.frame.size = header.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
    }
    
}

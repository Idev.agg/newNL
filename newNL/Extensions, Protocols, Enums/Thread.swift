//
//  Thread.swift
//  newNL
//
//  Created by aggrroo on 11.10.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

public typealias Closure = () -> ()

extension Thread {
    
    public class func performOnMain(_ closure: @escaping Closure) {
        guard Thread.isMainThread else {
            DispatchQueue.main.async {
                closure()
            }
            return
        }
        closure()
    }
    
    public class func performAfter(seconds: Double,_ closure: @escaping Closure) {
        DispatchQueue.global().asyncAfter(deadline: .now() + seconds) {
            closure()
        }
    }
    
    public class func performOnMainAfter(seconds: Double,_ closure: @escaping Closure) {
        performAfter(seconds: seconds) {
            performOnMain {
                closure()
            }
        }
    }
    
}

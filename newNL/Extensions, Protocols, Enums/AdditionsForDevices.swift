//
//  AdditionsForDevices.swift
//  newNL
//
//  Created by aggrroo on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

enum DeviceState {    
    case on
    case off
    case unknown
    
    static func getFromBits(bits: [Bit]) -> DeviceState {
        var state = DeviceState.unknown
        if bits[0] == .one {
            state = bits[3] == .one ? .on : .off
        }
        return state
    }
}

enum DeviceType {
    case tx
    case rx
    case ftx
    case frx
    case unknown
}

enum DeviceSubType {
    case unknown
    // TX
    case dimmer
    case relay
    case rgbController
    case impulseRelay
    // RX
    case temperatureSensor
    case temperatureAndHumiditySensor
    case motionSensor
    case leakSensor
    case openingSensor
    case lightSensor
    case control
    case rollerBlindsController
    // FTX
    case thermostat
    case dimmerF
    case relayF
    case socket
    case rollerBlinds
}

extension UInt8 {
    
    var deviceType: DeviceType {
        switch self {
        case 0: return .tx
        case 1: return .rx
        case 2: return .ftx
        case 3: return .frx
        default: return .unknown
        }
    }
    
}

extension DeviceSubType: Displayable {
    
    var displayName: String {
        guard DeviceSubType.arrayTxTypes.contains(self) else { return String.empty }
        switch self {
        case .relay:
            return "Реле"
        case .dimmer:
            return "Диммер"
        case .rgbController:
            return "Светодиодный контроллер"
        case .impulseRelay:
            return "Импульсное реле"
        default:
            return String.empty
        }
    }
    
    static var arrayTxTypes: [DeviceSubType] {
        return [.relay, .dimmer, .rgbController, .impulseRelay]
    }
    
}

extension UInt8 {
    
    func getSubTypeFor(type: DeviceType) -> DeviceSubType {
        switch type {
        case .tx:
            switch self {
            case 0: return .relay
            case 1: return .dimmer
            case 2: return .rgbController
            case 10: return .impulseRelay
            default: return .unknown
            }
        case .rx:
            switch self {
            case 0: return .control
            case 1: return .temperatureSensor
            case 2: return .temperatureAndHumiditySensor
            case 5: return .motionSensor
            case 7: return .rollerBlindsController
            case 8: return .openingSensor
            case 9: return .leakSensor
            default: return .unknown
            }
        case .ftx:
            switch self {
            case 1, 2: return .relayF
            case 3,4: return .socket
            case 5: return .dimmerF
            case 6: return .thermostat
            case 7: return .rollerBlinds
            default: return . unknown
            }
            
        case .frx: return .unknown
        case .unknown: return .unknown
        }

    }
}

extension DeviceSubType {
    
    var integerType: Int? {
        switch self {
        case .relay:
            return 0
        case .dimmer:
            return 1
        case .rgbController:
            return 2
        case .impulseRelay:
            return 10
        default:
            return nil
        }
    }
    
}

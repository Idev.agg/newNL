//
//  UIViewController + MenuPresenting.swift
//  JUNO
//
//  Created by Sergey on 12/17/18.
//  Copyright © 2018 elatesoftware. All rights reserved.
//

import UIKit

protocol MenuPresentableProtocol {
    
    func setupMenu()
    
}

extension BaseTabBarController: MenuPresentableProtocol {
    
    func setupMenu() {
        guard let menu = AppDelegate.appDelegate().menu, let menuView = menuView else { return }
        menu.delegate = self
        menuView.addSubview(menu.view)
        menu.view.setWidth(view.bounds.width)
        menu.view.setHeight(view.bounds.height)
        menu.view.frame.origin.x = -UIScreen.main.bounds.width
    }
    
}

protocol MenuPresenterProtocol {

    func revealMenu()
    
}

extension BaseTabBarController: SideMenuDelegate {
    
    func didDisplay() {
        // TODO: ??
    }
    
    func didHide(withSelection: Bool = false) {
        guard let menuView = menuView else { return }
        menuView.isHidden = true
        if selectedViewController is BaseNavigationController {
            if let current = (selectedViewController as! BaseNavigationController).currentController {
                if withSelection, selectedIndex != 1, viewControllers?.count == 4 {
                    selectedViewController = viewControllers![1]
                }
                if current is RoomViewController {
                    current.viewDidAppear(true)
                }
            }
        }
    }
    
    func tappedToHide(withSelection: Bool) {
        hideMenu(withSelection: withSelection)
    }

}

extension BaseTabBarController: MenuPresenterProtocol {
    
    var panGesture: UIPanGestureRecognizer {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(hideMenuWithSwipe))
        return pan
    }
    
    var screenEdgeGesture: UIScreenEdgePanGestureRecognizer {
        let screenEdgeGesture = UIScreenEdgePanGestureRecognizer(target: self, action: #selector(dragMenuFromScreenEdge))
        screenEdgeGesture.edges = [.left]
        return screenEdgeGesture
    }
    
    @objc func revealMenu() {
        guard let menu = AppDelegate.appDelegate().menu else { return }
        menu.reload()
        guard menu.status == .hidden else {
            hideMenu(withSelection: false)
            return
        }
        showMenu()
    }
    
    @objc private func showMenu() {
        guard let menu = AppDelegate.appDelegate().menu, let menuView = menuView, let shadowView = shadowView else { return }
        menuView.isHidden = false
        menuView.layoutSubviews()
        view.bringSubviewToFront(menuView)
        UIView.animate(withDuration: BASE_ANIMATION_DURATION, animations: { [weak self] in
            menu.view.frame.origin.x = self?.view.bounds.origin.x ?? zeroConstant
            shadowView.alpha = ALPHA_60
        }) { (complete) in
            menu.becomeVisible()
            
        }
    }
    
    @objc func hideMenu(withSelection: Bool, handler: (() -> ())? = nil) {
        guard let menu = AppDelegate.appDelegate().menu, let menuView = menuView, let shadowView = shadowView else { return }
        UIView.animate(withDuration: BASE_ANIMATION_DURATION, animations: {
            menu.view.frame.origin.x = -UIScreen.main.bounds.width
            shadowView.alpha = zeroConstant
        }) { (complete) in
            menuView.isHidden = true
            menu.reload()
            menu.didHide(withSelection: withSelection)
            handler?()
        }
    }
    
    @objc private func dragMenuFromScreenEdge(sender: UIScreenEdgePanGestureRecognizer) {
        guard (self.selectedViewController as? UINavigationController)?.viewControllers.count == 1 else { return }
        guard let menu = AppDelegate.appDelegate().menu, menu.status == .hidden, let menuView = menuView, let shadowView = shadowView else { return }
        if sender.state == .began {
            menuView.isHidden = false
        }
        if sender.state == .began || sender.state == .changed {
            let translation = sender.translation(in: view).x
            guard menu.view.frame.origin.x <= zeroConstant, translation <= menu.activeWidth else {
                menu.view.frame.origin.x = zeroConstant
                return
            }
            menu.view.frame.origin.x = -menu.activeWidth + translation
            shadowView.alpha = ALPHA_60 * translation / menu.activeWidth
        } else if sender.state == .ended {
            menu.view.frame.origin.x >= -menu.activeWidth / 2 ? showMenu() : hideMenu(withSelection: false)
        }
    }
    
    @objc private func hideMenuWithSwipe(sender: UIPanGestureRecognizer) {
        guard let menu = AppDelegate.appDelegate().menu, menu.status == .showen, menuView != nil, let shadowView = shadowView else { return }
        if sender.state == .began || sender.state == .changed {
            let translation = sender.translation(in: view).x
            shadowView.alpha = ALPHA_60 * (1 - abs(translation) / menu.activeWidth)
            guard menu.view.frame.origin.x <= zeroConstant, translation < zeroConstant else {
                menu.view.frame.origin.x = zeroConstant
                return
            }
            menu.view.frame.origin.x = translation
        } else if sender.state == .ended {
            menu.view.frame.origin.x >= -menu.activeWidth / 2 ? showMenu() : hideMenu(withSelection: false)
        }
    }
    
}

//
//  Extension Cell.swift
//  newNL
//
//  Created by aggrroo on 2/24/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

extension UICollectionViewCell {
    
    private func cast(device: NLBaseDevice, collection: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        var cell = self
        switch device.deviceType {
        case .tx:
            cell = collection.dequeueReusableCell(withReuseIdentifier: DeviceCollectionViewCell().reuseIdentifier!, for: indexPath)
            (cell as! DeviceCollectionViewCell).configureWith(device: device as! NLTXDevice)
        case .rx:
            switch (device as! NLRXDevice).subType {
            case .temperatureSensor, .temperatureAndHumiditySensor:
                cell = collection.dequeueReusableCell(withReuseIdentifier: TemperatureSensorCollectionViewCell().reuseIdentifier!, for: indexPath)
                (cell as! TemperatureSensorCollectionViewCell).configureWith(sensor: device as! NLRXDevice)
            case .motionSensor, .leakSensor, .lightSensor, .control, .openingSensor:
                cell = collection.dequeueReusableCell(withReuseIdentifier: DeviceCollectionViewCell().reuseIdentifier!, for: indexPath)
                (cell as! DeviceCollectionViewCell).configureWith(sensor: device as! NLRXDevice)
            default:
                cell = collection.dequeueReusableCell(withReuseIdentifier: DeviceCollectionViewCell().reuseIdentifier!, for: indexPath)
                (cell as! DeviceCollectionViewCell).configureWith(sensor: device as! NLRXDevice)
                //TEMP
//                return cell
            }
        case .ftx:
            switch (device as! NLFTXDevice).subType {
            case .thermostat:
                cell = collection.dequeueReusableCell(withReuseIdentifier: ThermostatCollectionViewCell().reuseIdentifier!, for: indexPath)
                (cell as! ThermostatCollectionViewCell).configureWith(thermostat: device as! Thermostat)
            case .dimmerF, .relayF, .socket, .rollerBlinds, .unknown:
                cell = collection.dequeueReusableCell(withReuseIdentifier: FDeviceCollectionViewCell().reuseIdentifier!, for: indexPath)
                (cell as! FDeviceCollectionViewCell).configureWith(fDevice: device as! NLFTXDevice)
            default:
                return cell
            }
        default:
            return self
        }
        return cell
    }
    
    func cast(object: NLBaseObject, collection: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        guard object.type != .nlDevice else {
            return self.cast(device: object as! NLBaseDevice, collection: collection, indexPath: indexPath)
        }
        var cell = self
        switch object.type {
        case .nlScenario:
            cell = collection.dequeueReusableCell(withReuseIdentifier: ScenarioCollectionViewCell().reuseIdentifier!, for: indexPath)
            (cell as! ScenarioCollectionViewCell).configureWith(scenario: object as! NLScenario)
        default:
            debugPrintText(text: "unknown cell")
        }
        return cell
    }
    
}

extension UITableViewCell {
    
    func cast(object: Any, tableView: UITableView, indexPath: IndexPath, selected: Bool = false, vc: UIViewController? = nil) -> UITableViewCell {
        var cell = BaseTableViewCell()
        if object is SourceModel {
            switch (object as! SourceModel).dataType {
            case .string:
                let reuseId = ChoosableParametersTableViewCell.REUSE_ID
                cell = (tableView.dequeueReusableCell(withIdentifier: reuseId) as? CellWithTextField) ?? CellWithTextField.init(style: .default, reuseIdentifier: reuseId)
                cell.configure(entity: object)
                (cell as! CellWithTextField).parameterNameTextField.indexPath = indexPath
                (cell as! CellWithTextField).parameterNameTextField.delegate = vc as? UITextFieldDelegate
            case .double:
                return cell
            case .int:
                return cell
            case .device:
                let reuseId = ChooseDevicesTableViewCell.REUSE_ID
                cell = (tableView.dequeueReusableCell(withIdentifier: reuseId) as? ChooseDevicesTableViewCell) ?? ChooseDevicesTableViewCell.init(style: .default, reuseIdentifier: reuseId)
                (cell as? ChooseDevicesTableViewCell)?.configure(entity: object)
            case .auto:
                return cell
            case .timer:
                return cell
            case .scenario:
                let reuseId = ChooseScenarioTableViewCell.REUSE_ID
                cell = (tableView.dequeueReusableCell(withIdentifier: reuseId) as? ChooseScenarioTableViewCell) ?? ChooseScenarioTableViewCell.init(style: .default, reuseIdentifier: reuseId)
                (cell as? ChooseScenarioTableViewCell)?.configure(entity: object)
            case .choosable:
                let reuseId = ChoosableParametersTableViewCell.REUSE_ID
                cell = (tableView.dequeueReusableCell(withIdentifier: reuseId) as? ChoosableParametersTableViewCell) ?? ChoosableParametersTableViewCell.init(style: .default, reuseIdentifier: reuseId)
                cell.configure(entity: object)
            case .temperatureParameter:
                let reuseId = SetParametersTableViewCell.REUSE_ID
                cell = (tableView.dequeueReusableCell(withIdentifier: reuseId) as? SetParametersTableViewCell) ?? SetParametersTableViewCell.init(style: .default, reuseIdentifier: reuseId)
                (cell as! SetParametersTableViewCell).configure(entity: object as? SourceModel, type: .temperatureParameter)
            case .humidityParameter:
                let reuseId = SetParametersTableViewCell.REUSE_ID
                cell = (tableView.dequeueReusableCell(withIdentifier: reuseId) as? SetParametersTableViewCell) ?? SetParametersTableViewCell.init(style: .default, reuseIdentifier: reuseId)
                (cell as! SetParametersTableViewCell).configure(entity: object as? SourceModel, type: .humidityParameter)
            case .complexCalendar:
                let reuseId = ComplexCalendarTableViewCell.REUSE_ID
                cell = (tableView.dequeueReusableCell(withIdentifier: reuseId) as? ComplexCalendarTableViewCell) ?? ComplexCalendarTableViewCell.init(style: .default, reuseIdentifier: reuseId)
                cell.configure(entity: object)
            case .unknown:
                return cell
            }
        } else if object is MenuCellModel {
            cell = tableView.dequeueReusableCell(withIdentifier: MenuTableViewCell.REUSE_ID, for: indexPath) as! MenuTableViewCell
            (cell as! MenuTableViewCell).configureWith(model: object as! MenuCellModel)
        } else if object is String {
            let reuseId = SelectableCellWithLabel.REUSE_ID
            cell = (tableView.dequeueReusableCell(withIdentifier: reuseId) as? SelectableCellWithLabel) ?? SelectableCellWithLabel.init(style: .default, reuseIdentifier: reuseId)
            (cell as! SelectableCellWithLabel).configure(entity: object, selected: selected)
            
        }
        return cell
    }
}


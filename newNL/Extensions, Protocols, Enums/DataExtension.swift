//
//  DataExtension.swift
//  newNL
//
//  Created by aggrroo on 21.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

extension Data {
    
    func getString(start: Int, lastReadable: Int) -> String? {
        var notEmptyBytes = Data()
        for byte in self {
            guard byte != Data.nullByte else {
                return String.init(data: notEmptyBytes, encoding: .windowsCP1251)
            }
            notEmptyBytes.append(byte)
        }
        return String.init(data: notEmptyBytes[start..<lastReadable], encoding: .windowsCP1251)
    }
    
    static func emptyDataWithCount(count: Int) -> Data {
        var data = Data()
        for _ in 0..<count {
            data.append(nullByte)
        }
        return data
    }
    
}

extension Data {
    
    static var emptyByte: UInt8 { return UInt8(255) }
    
    static var nullByte: UInt8 { return UInt8(0) }
    
    func getPrefix() -> Prefixes {
        guard let prefix = String.init(data: self[Parser.PREFIX_PARSING_RANGE], encoding: .windowsCP1251) else {
            return String.empty.getPrefix()
        }
        return prefix.getPrefix()
    }
    
}

extension Data {
    
    struct HexEncodingOptions: OptionSet {
        let rawValue: Int
        static let upperCased = HexEncodingOptions(rawValue: 1 << 0)
    }
    
    func hexEncodedString(options: HexEncodingOptions = []) -> String {
        let format = options.contains(.upperCased) ? "%02hhX" : "%02hhx"
        return map { String(format: format, $0) }.joined()
    }
}

extension Data {
    
    func byAddingUserProperties(type: AttachmentDeviceType, channel: Int, sourceArray: [SourceModel]) -> Data {
        guard self.getPrefix() == .userBin else { return self }
        var data = self
        var startByte = 6
        if type == .att_rx {
            startByte = 2182
        } else if type == .att_ftx {
            startByte = 4358
        }
        startByte += channel * 34
        
        var roomNumber = 255
        if let room = sourceArray.first(where: { $0.title == "Расположение" })?.value as? NLRoom {
            roomNumber = room.number
        }
        let deviceName = (sourceArray.first(where: { $0.title == "Название" })?.value as? String) ?? String.empty
        data.replaceSubrange(startByte..<startByte + 31, with: deviceName.toDataWithCount(count: 31))
        data[startByte + 32] = UInt8(0)
        data[startByte + 33] = UInt8(roomNumber)
        return data
    }

}

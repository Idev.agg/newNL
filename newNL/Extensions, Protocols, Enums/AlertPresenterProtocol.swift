//
//  File.swift
//  newNL
//
//  Created by Глеб Сергей on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation
import UIKit

protocol AlertPresenterProtocol: class {
    
    func presentSimpleAlert(with title: String?, text: String?, acceptTitle: String?, acceptHandler: (() -> ())?)
    
    func presentSimpleAlert(with title: String?, text: String?, acceptTitle: String?, cancelTitle: String?, acceptHandler: (() -> ())?, cancelHandler: (() -> ())?)
    
    func presentAlertWithError(error: CommonError, handler: (() -> ())?)
    
}



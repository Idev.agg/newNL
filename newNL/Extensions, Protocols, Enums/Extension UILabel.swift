//
//  Extension UILabel.swift
//  newNL
//
//  Created by aggrroo on 3/9/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

extension UILabel {
    
    static func initWith<T: UILabel>(text: String?, font: UIFont, textColor: UIColor, numberOfLines: Int? = 0, align: NSTextAlignment = .left) -> T {
        let label = T()
        label.textColor = textColor
        label.font = font
        label.text = text
        label.numberOfLines = numberOfLines ?? 0
        label.textAlignment = align
        return label
    }
    
}

//
//  ExtensionViewController + LoadingViewPresenterProtocol.swift
//  newNL
//
//  Created by Глеб Сергей on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

extension BaseViewController: LoadingViewPresenterProtocol {
    
    func showLoadingView(show: Bool, description: String? = nil) {
        Thread.performOnMain {
            for subview in self.view.subviews {
                guard subview is ProgressView else { continue }
                subview.removeFromSuperview()
            }
            if show {
                let progressView = ProgressView(frame: .zero)
                self.view.addSubview(progressView)
                progressView.snp.makeConstraints({ (maker) in
                    maker.edges.equalToSuperview()
                })
                progressView.setup()
                return
            }
        }
    }
    
    static func showLoadingViewOnCurrentScreen(show: Bool, description: String? = nil) {
        guard let controller = AppDelegate.appDelegate().currentController as? BaseViewController else {
            return
        }
        controller.showLoadingView(show: show, description: description)
    }

}

extension UIView: LoadingViewPresenterProtocol {
    
    func showLoadingView(show: Bool, description: String? = nil) {
        Thread.performOnMain {
            for subview in self.subviews {
                guard subview is ProgressView else { continue }
                subview.removeFromSuperview()
            }
            if show {
                let progressView = ProgressView(frame: .zero)
                self.addSubview(progressView)
                progressView.snp.makeConstraints({ (maker) in
                    maker.top.equalToSuperview()
                    maker.bottom.equalToSuperview()
                    maker.left.equalToSuperview()
                    maker.right.equalToSuperview()
                })
                return
            }
            
        }
    }
    
}

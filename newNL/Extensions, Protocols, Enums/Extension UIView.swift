//
//  Extension UIView.swift
//  newNL
//
//  Created by aggrroo on 3/9/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit
import PureLayout

extension UIView {
    
    func becomeStrocked(with color: UIColor) {
        layer.cornerRadius = CORNER_RADIUS
        layer.borderWidth = BORDER_WIDTH
        layer.borderColor = color.cgColor
    }
    
    func becomeRounded(radius: CGFloat) {
        layer.cornerRadius = radius
    }
    
    static func initWith(color: UIColor) -> UIView {
        let view = UIView()
        view.backgroundColor = color
        return view
    }
    
    func addBotLineWithColor(_ color: UIColor) {
        let botLine = UIView()
        addSubview(botLine)
        botLine.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge:.top)
        botLine.autoSetDimension(.height, toSize: 1)
        botLine.backgroundColor = color
    }
    
}

extension UIImageView {
    
    static func initWith(image: UIImage?) -> UIImageView {
        let imageView = UIImageView()
        imageView.image = image
        return imageView
    }
    
}

extension UIButton {
    
    static func buttonWith(title: String?, titleColor: UIColor = black, image: UIImage? = nil, strocked: Bool = true, target: Any, selector: Selector) -> UIButton {
        let button = UIButton()
        button.setTitleColor(titleColor, for: .normal)
        button.setTitle(title, for: .normal)
        if strocked {
            button.becomeStrocked(with: titleColor)
        }
        button.setImage(image, for: .normal)
        button.addTarget(target, action: selector, for: .touchUpInside)
        return button
    }
    
//    open override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.alpha = HIGHLITED_ALPHA
//    }
//    
//    open override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.alpha = SIMPLE_APLHA
//    }
//    
//    open override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
//        self.alpha = SIMPLE_APLHA
//    }
    
}

extension UIStackView {
    
    func addSpaceBelow() {
        self.addArrangedSubview(UIView())
    }
    
}

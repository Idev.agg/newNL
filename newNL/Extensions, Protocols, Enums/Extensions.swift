//
//  Extensions.swift
//  newNL
//
//  Created by aggrroo on 10.10.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

extension Bool {
    
    func toByte() -> UInt8 { return self ? 0 : 255 }
    
}

extension Double {
    
    var text: String { return "\(self)" }
    
}

extension Int {
    
    var text: String { return "\(self)" }
    
}

extension UInt8 {
    
    func toBits(count: Int) -> [Bit] {
        var byte = self
        var bits = [Bit](repeating: .zero, count: count)
        for i in 0..<count {
            let currentBit = byte & 0x01
            if currentBit != 0 {
                bits[i] = .one
            }
            byte >>= 1
        }
        return bits
    }
    
    func toBool() -> Bool {
        guard self == Data.emptyByte else { return true }
        return false
    }
    
}

extension Array where Element == Bit {
    
    static func bitsToBytes(bits: [Bit]) -> [UInt8] {
        assert(bits.count % 8 == 0, "Bit array size must be multiple of 8")
        let numBytes = 1 + (bits.count - 1) / 8
        var bytes = [UInt8](repeating: 0, count: numBytes)
        for (index, bit) in bits.enumerated() {
            if bit == .one {
                bytes[index / 8] += UInt8(1 << (7 - index % 8))
            }
        }
        return bytes
    }
    
}

enum Bit: UInt8, CustomStringConvertible {
    case zero, one
    var description: String {
        switch self {
        case .one: return "1"
        case .zero: return "0"
        }
    }
    
    func toggle() -> Bit {
        switch self {
        case .zero:
            return Bit.one
        case .one:
            return Bit.zero
        }
    }
}

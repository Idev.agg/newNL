//
//  BaseViewModel.swift
//  newNL
//
//  Created by aggrroo on 14.11.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

protocol BaseViewModelProtocol: class {}

class BaseViewModel: NSObject, BaseViewModelProtocol {
    
    override init() {
        super.init()
        debugInizializationMessage(someClass: self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
}

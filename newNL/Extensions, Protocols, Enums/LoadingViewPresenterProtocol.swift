//
//  LoadingViewPresenterProtocol.swift
//  newNL
//
//  Created by Глеб Сергей on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation
import UIKit

protocol LoadingViewPresenterProtocol: class {
    
    func showLoadingView(show: Bool, description: String?)
    
}

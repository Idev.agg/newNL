//
//  AppDelegate.swift
//  newNL
//
//  Created by Глеб Сергей on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var rootController: UIViewController?
    var currentController: UIViewController?
    var menu: SideMenuViewController?
    
    static func appDelegate() -> AppDelegate {
        
        return UIApplication.shared.delegate as! AppDelegate
    }

    var window: UIWindow?
    
    private override init() {
        super.init()
        debugInizializationMessage(someClass: self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        createWindow()
        setRootController()
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    func createWindow() {
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
    }
    
    func setRootController() {

        ModelsManager.shared.logIn {
            let menuModule = SideMenuModule.assembly()
            menu = menuModule.view as? SideMenuViewController
            
            let tabBarController = BaseTabBarController()
            
            let ud = UserDefaults.standard
            let ip = ud.string(forKey: "ipipip")
            let account = NLAccount(adress: Adress(ip: ip ?? "192.168.0.170"))
            account.login = Login(login: "admin1")
            account.password = Password(password: "admin1")
            PreferencesService.shared.currentAccount = account
            
            let rooms = RoomModule.assembly(type: .room)
            let roomsView = rooms.view
            roomsView.tabBarItem = UITabBarItem(title: "Комната", image: #imageLiteral(resourceName: "room"), selectedImage: #imageLiteral(resourceName: "roomA"))
            
            let house = RoomModule.assembly(type: .house)
            let houseView = house.view
            houseView.tabBarItem = UITabBarItem(title: "Дом", image: #imageLiteral(resourceName: "house"), selectedImage: #imageLiteral(resourceName: "houseA"))
            
            let timers = TimerModule.assembly()
            let timersView = timers.view
            timersView.tabBarItem = UITabBarItem(title: "Таймеры", image:  #imageLiteral(resourceName: "timer"), selectedImage: #imageLiteral(resourceName: "timerA"))
            
            let autos = AutoModule.assembly()
            let autosView = autos.view
            autosView.tabBarItem = UITabBarItem(title: "Автоматика", image: #imageLiteral(resourceName: "auto"), selectedImage: #imageLiteral(resourceName: "autoA"))
            
            window?.rootViewController = tabBarController
            
            let controllers = [houseView, roomsView, timersView, autosView]
            
            UITabBar.appearance().tintColor = lightGreen
            
            tabBarController.viewControllers = controllers.map { BaseNavigationController(rootViewController: $0)}
        }
    }

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "PRF64BD")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}


//
//  ViewController.swift
//  newNL
//
//  Created by Глеб Сергей on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        AppDelegate.appDelegate().currentController = self
        AppDelegate.appDelegate().rootController = self
        
        let roomController = RoomModule.assembly()
        let view = roomController.view
        AppDelegate.appDelegate().currentController = view
        navigationController?.present(view, animated: true, completion: nil)
        
        
        
        let account = NLAccount(adress: Adress(ip: "134.17.24.191"))
        PreferencesService.shared.currentAccount = account
        
//        let requestModel = BaseRequestModel(currentUrlString: ApiManager.shared.getUrlForFile(prefix: .deviceBin),
//                                            method: .GET,
//                                            data: nil,
//                                            parameters: nil)
//        
//        let userRequestModel = BaseRequestModel(currentUrlString: ApiManager.shared.getUrlForFile(prefix: .userBin),
//                                                method: .GET,
//                                                data: nil,
//                                                parameters: nil)
//        
//        let timerRequestModel = BaseRequestModel(currentUrlString: ApiManager.shared.getUrlForFile(prefix: .timerBin),
//                                                 method: .GET,
//                                                 data: nil,
//                                                 parameters: nil)
//        let autosRequestModel = BaseRequestModel(currentUrlString: ApiManager.shared.getUrlForFile(prefix: .autoBin),
//                                                 method: .GET,
//                                                 data: nil,
//                                                 parameters: nil)
//        let scenarioRequestModel = BaseRequestModel(currentUrlString: ApiManager.shared.getUrlForFile(prefix: .presetBin),
//                                                    method: .GET,
//                                                    data: nil,
//                                                    parameters: nil)
//        
//        RequestManager.shared.download(requestModel: requestModel,
//                                          expectedFile: .deviceBin) { (result: Result_<NLBaseDevicesList, ApiError>) in
//                                            
//                                            RequestManager.shared.download(requestModel: userRequestModel, expectedFile: .userBin, completionHandler: { (result: Result_<NLBaseDevicesList, ApiError>) in
//                                                
//                                                guard let value = result.value else {
//                                                    
//                                                    self.presentAlertWithError(error: ApiError(error: .noData), handler: nil)
//                                                    return
//                                                }
//                                                
//                                                
//                                                
//                                                ModelsManager.shared.devices = value as? NLBaseDevicesList
//                                                
//                                                RequestManager.shared.download(requestModel: timerRequestModel,
//                                                                                  expectedFile: .timerBin,
//                                                                                  completionHandler: { (result: Result_<NLTimerList, ApiError>) in
//                                                                                    ModelsManager.shared.timers = result.value as? NLTimerList
//                                                                                    RequestManager.shared.download(requestModel: autosRequestModel,
//                                                                                                                      expectedFile: .autoBin, completionHandler: { (result: Result_<NLTimerList, ApiError> ) in
//                                                                                                                        ModelsManager.shared.autos = result.value as? NLAutoList
//                                                                                                                        RequestManager.shared.download(requestModel: scenarioRequestModel,
//                                                                                                                                                          expectedFile: .presetBin,
//                                                                                                                                                          completionHandler: { (result: Result_<NLScenarioList, ApiError>) in
//                                                                                                                                                            print(result.value)
//                                                                                                                                                            let devices = ModelsManager.shared.devices?.devices
//                                                                                                                                                            let house = ModelsManager.shared.house
//                                                                                                                                                            let rooms = ModelsManager.shared.rooms
//                                                                                                                                                            let timer = ModelsManager.shared.timers
//                                                                                                                                                            let scenarious = ModelsManager.shared.scenarious
//                                                                                                                                                            let autos = ModelsManager.shared.autos
//                                                                                                                                                            print("a")
//                                                                                                                        })
//                                                                                                                        
//                                                                                    })
//                                                })
//                                            })
//                                            
//                                            
//                                            
//                                            
//                                            
//                                            
//                                            
//                                            
//                                            
//        }
        
        //        account.isActual { (isActual) in
        //            print()
        //        }
        //
        
        //        print(ModelsManager.shared.currentAccount)
        //        print(ModelsManager.shared.devices)
        //        print(ModelsManager.shared.house)
        //        print(ModelsManager.shared.rooms)
        
        
        //        StoreFileManager.shared.saveData(data: Data()) { (error) in
        //            print(error)
        //        }
        //
        //        let account = NLAccount(adress: Adress(ip: "192.168.0.1"))
        
        //        let userDefaults = UserDefaults.standard
        //
        //        let data = NSKeyedArchiver.archivedData(withRootObject: account)
        //        userDefaults.set(data, forKey: NLAccount.NLACCOUNT_KEY_PATH)
        //
        //        userDefaults.synchronize()
        //
        //        guard let decodedObject = userDefaults.object(forKey: NLAccount.NLACCOUNT_KEY_PATH) as? Data else {
        //
        //            return
        //        }
        
        //
        //        _ = NSKeyedUnarchiver.unarchiveObject(with: decodedObject) as? NLAccount
        //
        //        print()
        //
        //
        //        self.showLoadingView(with: "Hello", screenBlocking: true, show: true)
        //        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
        //            self.showLoadingView(with: nil, screenBlocking: false, show: false)
        //        }
        
    }
    
    
}


//
//  ChooseItemChooseItemAssembly.swift
//  home control
//
//  Created by Sergey on 18/03/2019.
//  Copyright © 2019 nooTechnick. All rights reserved.
//

import UIKit


protocol ChooseItemModuleInputProtocol: ModuleInputProtocol {

}

protocol ChooseItemModuleOutputProtocol {

}


final class ChooseItemModule: Module {

    private(set) var view: UIViewController
    private(set) var input: ChooseItemModuleInputProtocol?

    private init(view: UIViewController, input: ChooseItemModuleInputProtocol?) {
        self.view = view
        self.input = input
    }

    static func assembly(with itemsArray: [MenuCellModel], handler: @escaping ((Any) -> ())) ->  ChooseItemModule {
        itemsArray.last?.isLast = true
        let viewModel = ChooseItemViewModel(source: itemsArray)
        let view = ChooseItemViewController()
        view.handler = handler
        

        view.viewModel = viewModel
	
        let module = ChooseItemModule(view: view, input: viewModel)
        return module
    }
    
    static func assembly() -> ChooseItemModule {
        return ChooseItemModule(view: UIViewController(), input: nil)
    }

}

//
//  ChooseItemChooseItemViewModel.swift
//  home control
//
//  Created by Sergey on 18/03/2019.
//  Copyright © 2019 nooTechnick. All rights reserved.
//

class ChooseItemViewModel: BaseViewModel, ChooseItemModuleInputProtocol, ChooseItemViewOutputProtocol {
    
    var source: [MenuCellModel]
    
    init(source: [MenuCellModel]) {
        self.source = source
        self.source.last?.isLast = true
    }
    
    func getItemsCount() -> Int {
        return source.count
    }
    
    func getItemFor(row: Int) -> MenuCellModel {
        return source[row]
    }
    
}

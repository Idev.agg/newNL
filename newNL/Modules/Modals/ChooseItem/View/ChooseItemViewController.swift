//
//  ChooseItemChooseItemViewController.swift
//  home control
//
//  Created by Sergey on 18/03/2019.
//  Copyright © 2019 nooTechnick. All rights reserved.
//

import UIKit


protocol ChooseItemViewInputProtocol: class {

    func setupInitialState()
}

protocol ChooseItemViewOutputProtocol {
    
    func getItemsCount() -> Int
    func getItemFor(row: Int) -> MenuCellModel
    
}

class ChooseItemViewController: BaseModalViewController, ChooseItemViewInputProtocol {
    
    @IBOutlet var itemsTable: UITableView!
    @IBOutlet var itemsTableHeight: NSLayoutConstraint!
    @IBOutlet var closeButton: UIButton!
    
    private static let cellHeight: CGFloat = 50
    
    var handler: ((Any) -> ())?
    
    var viewModel: ChooseItemViewOutputProtocol!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCell()
        setTableHeight()
        itemsTable.separatorStyle = .none
        itemsTable.becomeRounded(radius: CORNER_RADIUS)
        closeButton.becomeStrocked(with: white)
        closeButton.setTitle("Закрыть", for: .normal)
    }
    
    func setTableHeight() {
        let itemsCount = CGFloat(viewModel.getItemsCount())
        let neededHeight = itemsCount * ChooseItemViewController.cellHeight
        guard itemsTable.frame.maxY > 100 + neededHeight else {
            itemsTableHeight.constant = itemsTable.frame.maxY - 100
            return
        }
        itemsTableHeight.constant = neededHeight
    }
    
    func registerCell() {
        itemsTable.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: MenuTableViewCell.REUSE_ID)
    }
    
    func setupInitialState() {
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        self.removeModal()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let firstTouch = touches.first else { return }
        let location = firstTouch.location(in: view)
        if !itemsTable.frame.contains(location), !self.closeButton.frame.contains(location) {
            self.removeModal()
        }
    }
    
}

extension ChooseItemViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.getItemsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell().cast(object: viewModel.getItemFor(row: indexPath.row), tableView: tableView, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ChooseItemViewController.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let item = viewModel.getItemFor(row: indexPath.row)
        self.removeModal()
        self.handler?(item)
    }
    
}

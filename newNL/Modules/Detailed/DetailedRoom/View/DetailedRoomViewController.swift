//
//  DetailedRoomDetailedRoomViewController.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol DetailedRoomViewInputProtocol: class {

    /**
        @author Sergey
        Setup initial state of the view
    */

    func setupInitialState()
}

protocol DetailedRoomViewOutputProtocol {

    /**
        @author Sergey
        Notify presenter that view is ready
    */

}

class DetailedRoomViewController: BaseViewController, DetailedRoomViewInputProtocol {

    var output: DetailedRoomViewOutputProtocol!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    // MARK: DetailedRoomViewInputProtocol
    func setupInitialState() {
    }
}

//
//  DetailedRoomDetailedRoomAssembly.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol DetailedRoomModuleInputProtocol: ModuleInputProtocol {

}

protocol DetailedRoomModuleOutputProtocol {

}


final class DetailedRoomModule: Module {

    private(set) var view: UIViewController
    private(set) var input: DetailedRoomModuleInputProtocol

    private init(view: DetailedRoomViewController, input: DetailedRoomModuleInputProtocol) {

	self.view = view
	self.input = input
    }

    static func assembly() ->  DetailedRoomModule{

        let router = DetailedRoomRouter()
        let presenter = DetailedRoomPresenter()
        let view = DetailedRoomViewController()
        let interactor = DetailedRoomInteractor()

        view.output = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        router.transitionHandler = view
	
        let module = DetailedRoomModule(view: view, input: presenter)
        return module

    }

}

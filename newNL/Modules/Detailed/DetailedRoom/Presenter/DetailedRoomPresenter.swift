//
//  DetailedRoomDetailedRoomPresenter.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

class DetailedRoomPresenter: BasePresenter, DetailedRoomModuleInputProtocol, DetailedRoomViewOutputProtocol {

    weak var view: DetailedRoomViewInputProtocol!
    var interactor: DetailedRoomInteractorProtocol!
    var router: DetailedRoomRouterProtocol!
    var output: DetailedRoomModuleOutputProtocol!

}

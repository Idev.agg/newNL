//
//  DetailedTimerDetailedTimerInteractor.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit

protocol DetailedTimerInteractorProtocol: class {

    func saveTimers(data: Data, handler: @escaping ((Bool) -> ()))
    
}

class DetailedTimerInteractor: BaseInteractor, DetailedTimerInteractorProtocol {

    func saveTimers(data: Data, handler: @escaping ((Bool) -> ())) {
        RequestManager.shared.requestTurnControl(taskType: .uploadTask, file: .timerBin, data: data, isFromTurn: false) { (result) in
            if result.value is CommandResponse {
                if (result.value as! CommandResponse).success {
                    handler(true)
                }
            }
        }
    }

}

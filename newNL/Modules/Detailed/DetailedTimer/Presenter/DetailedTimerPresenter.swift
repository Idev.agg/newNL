//
//  DetailedTimerDetailedTimerPresenter.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit

class DetailedTimerPresenter: BasePresenter, DetailedTimerModuleInputProtocol, DetailedTimerViewOutputProtocol {

    weak var view: DetailedTimerViewInputProtocol!
    var interactor: DetailedTimerInteractorProtocol!
    var router: DetailedTimerRouterProtocol!
    var output: DetailedTimerModuleOutputProtocol!
    
    private var timer: NLTimer?
    private var model: NLTimerModel
    
    private var nameModel: SourceModel?
    private var actionModel: SourceModel?
    private var timeModel: SourceModel?
    private var blockModel: SourceModel?
    
    init(timer: NLTimer?) {
        model = NLTimerModel(timer: timer)
        super.init()
        self.timer = timer
    }
    
    func sourceArray() -> [SourceModel] {
        var source = [SourceModel]()
        if nameModel == nil {
            nameModel = SourceModel(title: "Название таймера", value: model.name, dataType: .string)
        } else {
            nameModel?.value = model.name
        }
        source.append(nameModel!)
        
        if actionModel == nil {
            actionModel = SourceModel(title: "Действие", value: timer?.timerType, dataType: .choosable)
            actionModel?.selectingList = TimerType.choosable
        } else {
            actionModel?.value = model.timerType
        }
        source.append(actionModel!)
        
        if let timerType = model.timerType, timerType != .unknown {
            
            if timeModel == nil {
                if model.calendarObj == nil {
                    model.calendarObj = ComplexCalendarObject(type: timerType)
                }
                timeModel = SourceModel(title: "Настройки времени", value: model.calendarObj, dataType: .complexCalendar)
            } else {
                timeModel?.value = model.calendarObj
            }
            if timeModel != nil {
                source.append(timeModel!)
            }
            if model.commands == nil {
                model.commands = [NLCommonCommand]()
            }
            switch model.timerType! {
            case .on, .off, .onAndOff, .sunrise, .dusk:
                blockModel = SourceModel(title: "Применить для блоков", value: nil, dataType: .device)
                var devices = [NLBaseDevice]()
                blockModel?.selectingList = ModelsManager.shared.getDevices()?.filter({ (device) -> Bool in
                    return !(device is NLRXDevice)
                })
                
                for cmd in model.commands! {
                    guard let device = cmd.getDeviceFromCommand() else { break }
                    devices.append(device)
                }
                blockModel?.value = devices
            case .onAuto, .offAuto, .onAndOffAuto:
                blockModel = SourceModel(title: "Автоматизация", value: model.commands?.first?.getAutoFromCommand(), dataType: .choosable)
                blockModel?.selectingList = ModelsManager.shared.getAutos()
            case .runScenario:
                blockModel = SourceModel(title: "Сценарий", value: model.commands?.first?.getScenarioFromCommand(), dataType: .scenario)
                blockModel?.selectingList = ModelsManager.shared.getScenarious()
            case .unknown:
                break
            }
            if blockModel != nil {
                source.append(blockModel!)
            }
            
        }
        
        return source
    }
    
    func itemsCount() -> Int {
        return sourceArray().count
    }
    
    func getItemFor(_ row: Int) -> SourceModel {
        return sourceArray()[row]
    }
    
    func selectItemAt(_ row: Int) {
    }
    
    func back() {
        router.goBack()
    }
    
    private func prepareData(timer: NLTimer) {
        var data = StoreFileManager.shared.getTimerBinData() ?? Data()
        if (data.count == 0) {
            data = Prefixes.timerBin.getStringPrefix().toDataWithCount(count: 6) + Data.emptyDataWithCount(count: 8192)
        }
        data = data.byAddingTimer(timer)
        interactor.saveTimers(data: data) { [weak self] (success) in
            (self?.view as? BaseViewController)?.presentSimpleAlert(with: "", text: "Таймер успешно сохранен", acceptTitle: "Ок", acceptHandler: {
                self?.output.didSaveTimer()
                self?.router.goBack()
            })
        }
        
    }
    
    func saveTimer() {
        if validateSource() {
            prepareData(timer: NLTimer(model: model))
        }
    }
    
    private func validateSource() -> Bool {
        let nameStr = nameModel?.value as? String ?? String.empty
        guard nameStr.count > 0 else {
            nameModel?.error = DataError(error: .emptyField)
            view.reloadUI()
            return false
        }
        let withSameName = ModelsManager.shared.getTimers()?.filter({ (timer) -> Bool in
            return timer.name == nameStr && self.timer?.name != timer.name
        })
        guard withSameName?.count == 0 else {
            nameModel?.error = DataError(error: .alsoUsedName)
            view.reloadUI()
            return false
        }
        nameModel?.error = nil
        guard actionModel?.value != nil else {
            actionModel?.error = DataError(error: .emptyField)
            view.reloadUI()
            return false
        }
        actionModel?.error = nil
        guard let arrCmds = blockModel?.value as? [Any], arrCmds.count > 0 else {
            if blockModel?.value != nil {
                blockModel?.error = nil
                return true
            }
            if let type = model.timerType {
                let error: ErrorDiscriptionsWithCodes = {
                    switch type {
                    case .onAuto, .offAuto, .onAndOffAuto:
                        return .emptyAuto
                    case .runScenario:
                        return .emptyScenario
                    default:
                        return .emptyBlock
                    }
                }()
                (view as? BaseViewController)?.presentAlertWithError(error: CommonError(error: error), handler: nil)
            }
            view.reloadUI()
            return false
        }
        blockModel?.error = nil
        return true
    }
    
    func changeTime(_ obj: ComplexCalendarObject) {
        model.calendarObj = obj
        view.reloadUI()
    }
    
    func showSourceArrayForRow(_ row: Int) {
        guard let list = getItemFor(row).selectingList else { return }
        let array = list.map { MenuCellModel(value: $0) }
        let module = ChooseItemModule.assembly(with: array) { [weak self] (selected) in
            print("")
            guard let entityTitle = self?.getItemFor(row).title else { return }
            switch entityTitle {
            case "Действие":
                self?.model.timerType = TimerType.choosable.first(where: { (timer) -> Bool in
                    return timer.displayName == (((selected as? MenuCellModel)?.value) ?? String.empty).displayName
                })
                self?.actionModel?.error = nil
                self?.view.reloadUI()
            case "Применить для блоков":
                let device = list.first(where: { (eventer) -> Bool in
                    return (eventer as? NLBaseDevice)?.getShortName() == ((selected as? MenuCellModel)?.value)?.displayName
                }) as? NLBaseDevice
                self?.blockModel?.error = nil
                guard let type = self?.model.timerType else { return }
                let command = NLCommonCommand(device: device, scenario: nil, auto: nil, timerType: type)
                if let commands = self?.model.commands, !commands.contains(command) {
                    self?.model.commands?.append(command)
                }
                self?.view.reloadUI()
            case "Сценарий":
                let scenario = ModelsManager.shared.getScenarious()?.first(where: { (scenario) -> Bool in
                    scenario.displayName == ((selected as? MenuCellModel)?.value)?.displayName
                })
                guard let type = self?.model.timerType else { return }
                self?.blockModel?.error = nil
                self?.model.commands = [NLCommonCommand(device: nil, scenario: scenario, auto: nil, timerType: type)]
                self?.view.reloadUI()
            case "Автоматизация":
                let auto = ModelsManager.shared.getAutos()?.first(where: { (auto) -> Bool in
                    auto.displayName == ((selected as? MenuCellModel)?.value)?.displayName
                })
                guard let type = self?.model.timerType else { return }
                self?.blockModel?.error = nil
                self?.model.commands = [NLCommonCommand(device: nil, scenario: nil, auto: auto, timerType: type)]
                self?.view.reloadUI()
            default:
                return
            }
        }
        router.presentChooseItem(module: module)
    }
    
    func setTimerName(_ name: String?) {
        model.name = name
        nameModel?.error = nil
        view.reloadUI()
    }

}

extension Data {
    
    func byAddingTimer(_ timer: NLTimer) -> Data {
        var data = self
        let i = timer.number ?? 0
        
        let firstByte = NLTimer.FIRST_SETTINGS_BYTE + i * NLTimer.SETTINGS_LENGHT
        
        if let commands = timer.commands {
            for i in 0..<NLTimer.COMMANDS_COUNT {
                let start = firstByte + NLTimer.FIRST_COMMAND_SETTINGS_BYTE + i * NLCommonCommand.lENGHT
                var cmdDat = Data.emptyDataWithCount(count: 14)
                if i < commands.count {
                    cmdDat = commands[i].realValue
                }
                for n in 0..<cmdDat.count {
                    data[start + n] = cmdDat[n]
                }
                
            }
        }
        data[firstByte + NLTimer.TYPE] = {
            switch timer.timerType! {
            case .onAndOff, .onAndOffAuto:
                return 1
            default:
                return 0
            }
        }()
        data[firstByte + NLTimer.STATE] = timer.isActive?.realValue ?? Data.nullByte
        data[firstByte + NLTimer.ON_TIME_H] = UInt8(timer.onTime?.hours ?? 0)
        data[firstByte + NLTimer.ON_TIME_M] = UInt8(timer.onTime?.minutes ?? 0)
        data[firstByte + NLTimer.OFF_TIME_H] = UInt8(timer.offTime?.hours ?? 0)
        data[firstByte + NLTimer.OFF_TIME_M] = UInt8(timer.offTime?.minutes ?? 0)
        data[firstByte + NLTimer.DAYS] = timer.calendar?.realValue ?? Data.nullByte
        
        let nameStart = NLTimer.FIRST_NAME_BYTE + NLTimer.NAME_DESCRIBE_LENGHT * i
        
        let name = timer.name?.toDataWithCount(count: NLTimer.NAME_DESCRIBE_LENGHT)
        if let name = name {
            for i in 0..<name.count {
                data[nameStart + i] = name[i]
            }
        }
        
        return data
    }
    
    func byRemovingTimer(_ timer: NLTimer) -> Data {
        var data = self
        let i = timer.number ?? 0
        
        let firstByte = NLTimer.FIRST_SETTINGS_BYTE + i * NLTimer.SETTINGS_LENGHT
        
        for i in firstByte..<firstByte + NLTimer.SETTINGS_LENGHT {
            data[i] = Data.emptyByte
        }
        
        
        let nameStart = NLTimer.FIRST_NAME_BYTE + NLTimer.NAME_DESCRIBE_LENGHT * i
        let nameFinish = nameStart + NLTimer.NAME_DESCRIBE_LENGHT
        
        for i in nameStart..<nameFinish {
            data[i] = Data.emptyByte
        }
        
        return data
    }
}

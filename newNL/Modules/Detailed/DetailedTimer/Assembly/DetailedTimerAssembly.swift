//
//  DetailedTimerDetailedTimerAssembly.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol DetailedTimerModuleInputProtocol: ModuleInputProtocol {

}

protocol DetailedTimerModuleOutputProtocol {
    
    func didSaveTimer()

}


final class DetailedTimerModule: Module {
    
    private(set) var view: UIViewController
    private(set) var input: DetailedTimerModuleInputProtocol?

    private init(view: UIViewController, input: DetailedTimerModuleInputProtocol?) {

        self.view = view
        self.input = input
    }

    static func assembly(timer: NLTimer?, output: DetailedTimerModuleOutputProtocol) ->  DetailedTimerModule{

        let router = DetailedTimerRouter()
        let presenter = DetailedTimerPresenter(timer: timer)
        let view = DetailedTimerViewController()
        let interactor = DetailedTimerInteractor()

        view.output = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        presenter.output = output
        router.transitionHandler = view
	
        let module = DetailedTimerModule(view: view, input: presenter)
        return module

    }
    
    static func assembly() -> DetailedTimerModule {
        return DetailedTimerModule.init(view: UIViewController(), input: nil)
    }

}

//
//  DetailedTimerDetailedTimerViewController.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol DetailedTimerViewInputProtocol: class {

    func setupInitialState()
    func reloadUI()
    
}

protocol DetailedTimerViewOutputProtocol {

    func itemsCount() -> Int
    func getItemFor(_ row: Int) -> SourceModel
    func selectItemAt(_ row: Int)
    func back()
    func saveTimer()
    func setTimerName(_ name: String?)
    func showSourceArrayForRow(_ row: Int)
    func changeTime(_ obj: ComplexCalendarObject)
    
}

class DetailedTimerViewController: BaseViewController, DetailedTimerViewInputProtocol {

    var output: DetailedTimerViewOutputProtocol!
    private var tableView = UITableView()

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.separatorStyle = .none
        view.addSubview(tableView)
        tableView.snp.makeConstraints { (maker) in
            maker.top.equalTo(navigationView.snp.bottom)
            maker.left.equalToSuperview()
            maker.right.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    override func configureNavigation() {
        super.configureNavigation()
        let backButton = Button.back
        let saveButton = Button(text: "Сохранить", image: nil, imagePosition: nil, strocked: true)
        backButton.configure { [weak self] in
            self?.hideKeyboard()
            self?.output.back()
        }
        saveButton.configure { [weak self] in
            self?.hideKeyboard()
            self?.output.saveTimer()
        }
        self.navigationView.configure(titleText: titleText, left: backButton, right: saveButton)
    }

    func reloadUI() {
        self.tableView.reloadData()
    }

    // MARK: DetailedTimerViewInputProtocol
    func setupInitialState() {
    }
}

extension DetailedTimerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.itemsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell().cast(object: output.getItemFor(indexPath.row), tableView: tableView, indexPath: indexPath)
        if cell is ChooseDevicesTableViewCell {
            (cell as! ChooseDevicesTableViewCell).delegate = self
        } else if cell is ChooseScenarioTableViewCell {
            (cell as! ChooseScenarioTableViewCell).delegate = self
        } else if cell is CellWithTextField {
            (cell as! CellWithTextField).parameterNameTextField.delegate = self
        } else if cell is ComplexCalendarTableViewCell {
            (cell as! ComplexCalendarTableViewCell).delegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output.showSourceArrayForRow(indexPath.row)
    }
    
}

extension DetailedTimerViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        output.setTimerName(textField.text)
    }
    
}

extension DetailedTimerViewController: ChooseDevicesTableViewCellDelegate {
    
    func didSelectCellWithDevice(_ device: NLBaseDevice?, isAddTapped: Bool, cell: UITableViewCell) {
        let ip = tableView.indexPath(for: cell)
        guard let wrappedIndexPath = ip else { return }
        output.showSourceArrayForRow(wrappedIndexPath.row)
    }
    
}

extension DetailedTimerViewController: ChooseScenarioTableViewCellDelegate {
    
    func didSelectCellWithScenario(_ device: NLScenario?, isAddTapped: Bool, cell: UITableViewCell) {
        let ip = tableView.indexPath(for: cell)
        guard let wrappedIndexPath = ip else { return }
        output.showSourceArrayForRow(wrappedIndexPath.row)
    }
    
}
 
extension DetailedTimerViewController: ComplexCalendarTableViewCellDelegate {
    
    func didChangeTime(_ obj: ComplexCalendarObject) {
        hideKeyboard()
        output.changeTime(obj)
    }
 
}

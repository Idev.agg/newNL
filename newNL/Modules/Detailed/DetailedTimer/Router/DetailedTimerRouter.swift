//
//  DetailedTimerDetailedTimerRouter.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

protocol DetailedTimerRouterProtocol: BaseRouterProtocol {
    
    func goBack()
    func presentChooseItem(module: ChooseItemModule)

}

class DetailedTimerRouter: BaseRouter, DetailedTimerRouterProtocol {
    
    func goBack() {
        transitionHandler.closeCurrent()
    }

    func presentChooseItem(module: ChooseItemModule) {
        transitionHandler.presentModallyOnSelf(module: module)
    }
    
}

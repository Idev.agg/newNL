//
//  DetailedRoomsDetailedRoomsPresenter.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

class DetailedRoomsPresenter: BasePresenter, DetailedRoomsModuleInputProtocol, DetailedRoomsViewOutputProtocol {

    weak var view: DetailedRoomsViewInputProtocol!
    var interactor: DetailedRoomsInteractorProtocol!
    var router: DetailedRoomsRouterProtocol!
    var output: DetailedRoomsModuleOutputProtocol!

}

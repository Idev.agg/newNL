//
//  DetailedRoomsDetailedRoomsViewController.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol DetailedRoomsViewInputProtocol: class {

    /**
        @author Sergey
        Setup initial state of the view
    */

    func setupInitialState()
}

protocol DetailedRoomsViewOutputProtocol {

    /**
        @author Sergey
        Notify presenter that view is ready
    */

}

class DetailedRoomsViewController: BaseViewController, DetailedRoomsViewInputProtocol {

    var output: DetailedRoomsViewOutputProtocol!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    // MARK: DetailedRoomsViewInputProtocol
    func setupInitialState() {
    }
}

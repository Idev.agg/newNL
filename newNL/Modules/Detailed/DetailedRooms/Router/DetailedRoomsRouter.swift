//
//  DetailedRoomsDetailedRoomsRouter.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

protocol DetailedRoomsRouterProtocol: BaseRouterProtocol {

}

class DetailedRoomsRouter: BaseRouter, DetailedRoomsRouterProtocol {

}

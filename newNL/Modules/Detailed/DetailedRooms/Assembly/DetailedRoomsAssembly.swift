//
//  DetailedRoomsDetailedRoomsAssembly.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol DetailedRoomsModuleInputProtocol: ModuleInputProtocol {

}

protocol DetailedRoomsModuleOutputProtocol {

}


final class DetailedRoomsModule: Module {

    private(set) var view: UIViewController
    private(set) var input: DetailedRoomsModuleInputProtocol

    private init(view: DetailedRoomsViewController, input: DetailedRoomsModuleInputProtocol) {

	self.view = view
	self.input = input
    }

    static func assembly() ->  DetailedRoomsModule{

        let router = DetailedRoomsRouter()
        let presenter = DetailedRoomsPresenter()
        let view = DetailedRoomsViewController()
        let interactor = DetailedRoomsInteractor()

        view.output = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        router.transitionHandler = view
	
        let module = DetailedRoomsModule(view: view, input: presenter)
        return module

    }

}

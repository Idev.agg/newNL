//
//  DetailedAutoDetailedAutoAssembly.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol DetailedAutoModuleInputProtocol: ModuleInputProtocol {
    
}

protocol DetailedAutoModuleOutputProtocol {
    func didSaveAuto()
}


final class DetailedAutoModule: Module {

    private(set) var view: UIViewController
    private(set) var input: DetailedAutoModuleInputProtocol?

    private init(view: UIViewController, input: DetailedAutoModuleInputProtocol?) {
        
        self.view = view
        self.input = input
    }

    static func assembly(auto: NLAuto?, output: DetailedAutoModuleOutputProtocol) ->  DetailedAutoModule {

        let router = DetailedAutoRouter()
        let presenter = DetailedAutoPresenter(auto: auto)
        let view = DetailedAutoViewController()
        let interactor = DetailedAutoInteractor()

        view.output = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        presenter.output = output
        router.transitionHandler = view
	
        let module = DetailedAutoModule(view: view, input: presenter)
        return module

    }
    
    static func assembly() -> DetailedAutoModule {
        return DetailedAutoModule.init(view: UIViewController(), input: nil)
    }

}

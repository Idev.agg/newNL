//
//  DetailedAutoDetailedAutoPresenter.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit

class DetailedAutoPresenter: BasePresenter, DetailedAutoModuleInputProtocol, DetailedAutoViewOutputProtocol {

    weak var view: DetailedAutoViewInputProtocol!
    var interactor: DetailedAutoInteractorProtocol!
    var router: DetailedAutoRouterProtocol!
    var output: DetailedAutoModuleOutputProtocol!
    
    private var nameModel: SourceModel?
    private var typeModel: SourceModel?
    private var eventerModel: SourceModel?
    private var checkedParameterModel: SourceModel?
    private var commandModel: SourceModel?
    private var blockModel: SourceModel?
    
    func sourceArray() -> [SourceModel] {
        var array = [SourceModel]()
        
        if nameModel == nil {
            nameModel = SourceModel(title: "Название автоматики", value: model.name, dataType: .string)
        } else {
            nameModel?.value = model.name
        }
        array.append(nameModel!)
        if typeModel == nil {
            typeModel = SourceModel(title: "Тип", value: model.autoType?.stringValue, dataType: .choosable)
            typeModel?.selectingList = AutoType.choosable
        } else {
            typeModel?.value = model.autoType?.stringValue
        }
        array.append(typeModel!)
        
        if model.autoType != nil {
            if eventerModel == nil {
                eventerModel = SourceModel(title: "Датчик/Пульт", value: [model.eventer], dataType: .device)
                eventerModel?.selectingList = ModelsManager.shared.getDevices()?.filter({ (device) -> Bool in
                    return device is NLRXDevice
                }) ?? [NLBaseDevice]()
            } else {
                eventerModel?.value = [model.eventer]
            }
            array.append(eventerModel!)
            if model.eventer != nil {
                let title = model.eventer?.subType == .control ? "Команда с пульта" : nil
                let value = model.parameterOrIncomeCommand
                if checkedParameterModel == nil {
                    checkedParameterModel = SourceModel(title: title, value: value, dataType: DataType.dataTypeForParameterOf(eventer: model.eventer!, type: model.autoType!))
                    if checkedParameterModel?.dataType == .choosable {
                        checkedParameterModel?.selectingList = ControllCommands.selectable
                    }
                } else {
                    checkedParameterModel?.value = value
                }
                array.append(checkedParameterModel!)
                if checkedParameterModel!.dataType == .choosable, model.eventer?.subType != .control {
                    array.removeLast()
                }
                if commandModel == nil {
                    commandModel = SourceModel(title: "Команда блоку", value: model.command?.command.toAutoCommand().displayName, dataType: .choosable)
                    commandModel?.selectingList = AllowedAutoCommands.selectable
                } else {
                    commandModel?.value = model.command?.command.toAutoCommand().displayName
                    
                }
                array.append(commandModel!)
                if let command = model.command, command.command != .unknown {
                    if command.command == .runScenario {
                        blockModel = SourceModel(title: "Сценарий", value: model.command?.getScenarioFromCommand(), dataType: .scenario)
                        blockModel?.selectingList = ModelsManager.shared.getScenarious()
                        array.append(blockModel!)
                    } else {
                        blockModel = SourceModel(title: "Блок", value: [model.command?.getDeviceFromCommand()], dataType: .device)
                        blockModel?.selectingList = ModelsManager.shared.getDevices()?.filter({ (device) -> Bool in
                            return !(device is NLRXDevice)
                        }) ?? [NLBaseDevice]()
                        array.append(blockModel!)
                    }
                }
                
            }
        }
        
        return array
    }
    
    private var auto: NLAuto?
    private var model: DetailedAutoViewModel
    
    init(auto: NLAuto?) {
        self.auto = auto
        self.model = DetailedAutoViewModel(auto: auto)
    }
    
    func getTitle() -> String {
        guard auto != nil else {
            return "Добавление автоматизации"
        }
        return "Редактирование автоматизации"
    }
    
    func viewLoaded() {
        view.reloadUI()
    }
    
    func getRowsNumber() -> Int {
        return sourceArray().count
    }
    
    private func removeErrors() {
        nameModel?.error = nil
        typeModel?.error = nil
        eventerModel?.error = nil
        checkedParameterModel?.error = nil
        commandModel?.error = nil
        blockModel?.error = nil
    }
    
    func getItemForRow(row: Int) -> SourceModel {
        return sourceArray()[row]
    }
    
    func pressedItemAt(index: Int) {
    }
    
    func back() {
        router.back()
    }
    
    func saveAuto() {
        if validateModel() {
            prepareData(auto: NLAuto(model: model))
        }
    }
    
    func setAutoName(_ name: String?) {
        model.name = name
        nameModel?.error = nil
        view.reloadUI()
    }
    
    func setParameterOrIncomeCommand(_ param: Int) {
        model.parameterOrIncomeCommand = param
    }
    
    private func validateModel() -> Bool {
        let nameStr = nameModel?.value as? String ?? String.empty
        guard nameStr.count > 0 else {
            nameModel?.error = DataError(error: .emptyField)
            view.reloadUI()
            return false
        }
        let withSameName = ModelsManager.shared.getAutos()?.filter({ (auto) -> Bool in
            return auto.name == nameStr && self.auto?.name != auto.name
        })
        guard withSameName?.count == 0 else {
            nameModel?.error = DataError(error: .alsoUsedName)
            view.reloadUI()
            return false
        }
        nameModel?.error = nil
        guard typeModel?.value != nil else {
            typeModel?.error = DataError(error: .emptyField)
            view.reloadUI()
            return false
        }
        typeModel?.error = nil
        let eventer = (eventerModel?.value as? [NLBaseDevice])?.first
        guard eventer != nil else {
            (view as? BaseViewController)?.presentAlertWithError(error: CommonError(error: .emptyEventer), handler: nil)
            return false
        }
        guard commandModel?.value != nil else {
            commandModel?.error = DataError(error: .emptyField)
            view.reloadUI()
            return false
        }
        commandModel?.error = nil
        guard let cmd = model.command?.command else {
            (view as? BaseViewController)?.presentAlertWithError(error: CommonError(error: .emptyField), handler: nil)
            return false
        }
        if cmd == .runScenario {
            let scenario = blockModel?.value as? NLScenario
            guard scenario != nil else {
                (view as? BaseViewController)?.presentAlertWithError(error: CommonError(error: .emptyScenario), handler: nil)
                return false
            }
        } else {
            let block = (blockModel?.value as? [NLBaseDevice])?.first
            guard block != nil else {
                (view as? BaseViewController)?.presentAlertWithError(error: CommonError(error: .emptyBlock), handler: nil)
                return false
            }
        }
        return true
    }
    
    func showSourceArrayForRow(_ row: Int) {
        guard let list = getItemForRow(row: row).selectingList else { return }
        let array = list.map { MenuCellModel(value: $0.displayName) }
        let module = ChooseItemModule.assembly(with: array) { [weak self] (selected) in
            guard let entityTitle = self?.getItemForRow(row: row).title else { return }
            switch entityTitle {
            case "Тип":
                self?.model.autoType = AutoType.choosable.first(where: { (auto) -> Bool in
                    return auto.displayName == ((selected as? MenuCellModel)?.value as? String)
                })
                self?.model.parameterOrIncomeCommand = nil
                self?.typeModel?.error = nil
                self?.view.reloadUI()
            case "Команда блоку":
                let command = AllowedAutoCommands.selectable.first(where: { (command) -> Bool in
                    return command.displayName == ((selected as? MenuCellModel)?.value as? String)
                })
                self?.model.command = NLCommonCommand(device: nil, scenario: nil, command: command)
                self?.commandModel?.error = nil
                self?.view.reloadUI()
            case "Датчик/Пульт":
                self?.model.eventer = list.first(where: { (eventer) -> Bool in
                    return (eventer as? NLBaseDevice)?.getShortName() == ((selected as? MenuCellModel)?.value as? String)
                }) as? NLRXDevice
                self?.eventerModel?.error = nil
                self?.view.reloadUI()
            case "Блок":
                let device = list.first(where: { (eventer) -> Bool in
                    return (eventer as? NLBaseDevice)?.getShortName() == ((selected as? MenuCellModel)?.value as? String)
                }) as? NLBaseDevice
                guard let command = self?.model.command else {
                    self?.model.command = NLCommonCommand(device: device, scenario: nil, command: nil)
                    self?.view.reloadUI()
                    return
                }
                self?.blockModel?.error = nil
                self?.model.command = NLCommonCommand(device: device, scenario: nil, command: command.command.toAutoCommand())
                self?.view.reloadUI()
            case "Сценарий":
                let scenario = ModelsManager.shared.getScenarious()?.first(where: { (scenario) -> Bool in
                    scenario.displayName == ((selected as? MenuCellModel)?.value as? String)
                })
                guard let command = self?.model.command else {
                    self?.model.command = NLCommonCommand(device: nil, scenario: scenario, command: nil)
                    self?.view.reloadUI()
                    return
                }
                self?.blockModel?.error = nil
                self?.model.command = NLCommonCommand(device: nil, scenario: scenario, command: command.command.toAutoCommand())
                self?.view.reloadUI()
            default:
                return
            }
        }
        router.presentChooseItem(module: module)
    }
    
    private func prepareData(auto: NLAuto) {
        var data = StoreFileManager.shared.getAutoBinData() ?? Data()
        if (data.count == 0) {
            data = Prefixes.autoBin.getStringPrefix().toDataWithCount(count: 6) + Data.emptyDataWithCount(count: 12288)
        }
        data = data.byAddingAuto(auto: auto)
        interactor.saveAutos(data: data) { [weak self] (success) in
            (self?.view as? BaseViewController)?.presentSimpleAlert(with: "", text: "Автоматизация успешно сохранена", acceptTitle: "Ок", acceptHandler: {
                self?.output.didSaveAuto()
                self?.router.back()
            })
        }
        
    }
    
}

extension Data {
    
    func byAddingAuto(auto: NLAuto) -> Data {
        var data = self
        let i = auto.number
        var startByte = NLAuto.FIRST_SETTINGS_BYTE + i * NLAuto.AUTO_LENGHT
        if i > 14 {
            startByte += 1
        }
        let startCommandByte = startByte + NLAuto.FIRST_COMMAND_BYTE
        let nameStart = NLAuto.FIRST_NAME_BYTE + NLAuto.NAME_LENGHT * i
        let nameData = auto.name?.toDataWithCount(count: NLAuto.NAME_LENGHT)
        if let nameData = nameData, nameData.count > 0 {
            for i in 0..<nameData.count {
                data[nameStart + i] = nameData[i]
            }
        }
        data[startByte + NLAuto.TYPE] = auto.autoType.realValue
        data[startByte + NLAuto.STATE] = auto.isActive.realValue
        data[startByte + NLAuto.EVENTER_TYPE] = UInt8(1)
        data[startByte + NLAuto.EVENTER_CHANEL] = UInt8(auto.eventer.chanel)
        data[startByte + NLAuto.PARAMETER] = UInt8(auto.parameterOrIncomeCommand)
        let commandData = auto.command.realValue
        for i in 0..<commandData.count {
            data[startCommandByte + i] = commandData[i]
        }
        return data
    }
    
    func byRemovingAuto(auto: NLAuto) -> Data {
        var data = self
        let i = auto.number
        var startByte = NLAuto.FIRST_SETTINGS_BYTE + i * NLAuto.AUTO_LENGHT
        if i > 14 {
            startByte += 1
        }
        let startCommandByte = startByte + NLAuto.FIRST_COMMAND_BYTE
        let finishCommandByte = startCommandByte + NLCommonCommand.lENGHT
        let nameStart = NLAuto.FIRST_NAME_BYTE + NLAuto.NAME_LENGHT * i
        let nameFinish = nameStart + NLAuto.NAME_LENGHT - 1
        
        for i in startByte...finishCommandByte {
            data[i] = Data.emptyByte
        }
        for i in nameStart...nameFinish {
            data[i] = Data.emptyByte
        }
        return data
    }
}

extension Bool {
    
    var realValue: UInt8 {
        switch self {
        case true:
            return 0
        case false:
            return 255
        }
    }
    
}

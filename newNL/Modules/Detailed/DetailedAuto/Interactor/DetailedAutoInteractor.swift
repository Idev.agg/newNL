//
//  DetailedAutoDetailedAutoInteractor.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit

protocol DetailedAutoInteractorProtocol: class {

    func saveAutos(data: Data, handler: @escaping ((Bool) -> ()))
    
}

class DetailedAutoInteractor: BaseInteractor, DetailedAutoInteractorProtocol {
    
    func saveAutos(data: Data, handler: @escaping ((Bool) -> ())) {
        RequestManager.shared.requestTurnControl(taskType: .uploadTask, file: .autoBin, data: data, isFromTurn: false) { (result) in
            if result.value is CommandResponse {
                if (result.value as! CommandResponse).success {
                    handler(true)
                }
            }
        }
    }

}



//
//  DetailedAutoDetailedAutoViewController.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol DetailedAutoViewInputProtocol: class {

    func setupInitialState()
    func reloadUI()

}

protocol DetailedAutoViewOutputProtocol {

    func viewLoaded()
    func getTitle() -> String
    func getRowsNumber() -> Int
    func getItemForRow(row: Int) -> SourceModel
    func pressedItemAt(index: Int)
    func showSourceArrayForRow(_ row: Int)
    func saveAuto()
    func setParameterOrIncomeCommand(_ param: Int)
    func setAutoName(_ name: String?)
    
    func back()
    
}

class DetailedAutoViewController: BaseViewController, DetailedAutoViewInputProtocol {

    var output: DetailedAutoViewOutputProtocol!

    @IBOutlet var tableView: UITableView!
    
    override var titleText: String {
        guard let output = output else {
            return "Добавление автоматизации"
        }
        return output.getTitle()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        output.viewLoaded()        
    }
    
    override func configureNavigation() {
        let backButton = Button.back
        let saveButton = Button(text: "Сохранить", image: nil, imagePosition: nil, strocked: true)
        backButton.configure { [weak self] in
            self?.hideKeyboard()
            self?.output.back()
        }
        saveButton.configure { [weak self] in
            self?.hideKeyboard()
            self?.output.saveAuto()
        }
        self.navigationView.configure(titleText: titleText, left: backButton, right: saveButton)
    }

    func reloadUI() {
        self.tableView.reloadData()
    }

    // MARK: DetailedAutoViewInputProtocol
    func setupInitialState() {
    }
}

extension DetailedAutoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.getRowsNumber()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell().cast(object: output.getItemForRow(row: indexPath.row), tableView: tableView, indexPath: indexPath)
        if cell is ChooseDevicesTableViewCell {
            (cell as! ChooseDevicesTableViewCell).delegate = self
        } else if cell is ChooseScenarioTableViewCell {
            (cell as! ChooseScenarioTableViewCell).delegate = self
        } else if cell is SetParametersTableViewCell {
            (cell as! SetParametersTableViewCell).delegate = self
        } else if cell is CellWithTextField {
            (cell as! CellWithTextField).parameterNameTextField.delegate = self
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let entity = output.getItemForRow(row: indexPath.row)
        if entity.dataType == .choosable {
            output.showSourceArrayForRow(indexPath.row)
        }
    }
    
}

extension DetailedAutoViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        output.setAutoName(textField.text)
    }
    
}

extension DetailedAutoViewController: ChooseDevicesTableViewCellDelegate {
    
    func didSelectCellWithDevice(_ device: NLBaseDevice?, isAddTapped: Bool, cell: UITableViewCell) {
        let ip = tableView.indexPath(for: cell)
        guard let wrappedIndexPath = ip else { return }
        output.showSourceArrayForRow(wrappedIndexPath.row)
    }
    
}

extension DetailedAutoViewController: ChooseScenarioTableViewCellDelegate {
    
    func didSelectCellWithScenario(_ device: NLScenario?, isAddTapped: Bool, cell: UITableViewCell) {
        let ip = tableView.indexPath(for: cell)
        guard let wrappedIndexPath = ip else { return }
        output.showSourceArrayForRow(wrappedIndexPath.row)
    }
    
}

extension DetailedAutoViewController: SetParametersTableViewCellDelegate {
    
    func didUpdateValue(_ value: Int) {
        output.setParameterOrIncomeCommand(value)
    }
    
}

//
//  DetailedAutoViewModel.swift
//  newNL
//
//  Created by aggrroo on 14.11.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

class DetailedAutoViewModel: BaseViewModel {
    
    private static let defaultTemperature = 25
    private static let defaultHumidity = 70
    private static let defaultCommand = 255
    
    var name: String?
    var autoType: AutoType?
    var isActive: Bool = true
    var eventer: NLRXDevice?
    var parameterOrIncomeCommand: Int?
    var command: NLCommonCommand?
    var number: Int?
    
    override init() {
        super.init()
    }
    
    convenience init(auto: NLAuto?) {
        self.init()
        guard let auto = auto else {
            var completedNumbers = [Int]()
            if let autos = ModelsManager.shared.getAutos(), autos.count > 0 {
                for auto in autos {
                    completedNumbers.append(auto.number)
                }
            }
            var autoNumber = 0
            if completedNumbers.count > 0 {
                autoNumber = {
                    for i in 0..<NLAuto.MAX_AUTOS_COUNT {
                        if !completedNumbers.contains(i) {
                            return i
                        }
                    }
                    return 0
                }()
            }
            self.number = autoNumber
            return
        }
        self.name = auto.name
        self.autoType = auto.autoType
        self.isActive = auto.isActive
        self.eventer = auto.eventer
        self.parameterOrIncomeCommand = auto.parameterOrIncomeCommand
        self.command = auto.command
        self.number = auto.number
    }
    
    func getName() -> String {
        return name ?? String.empty
    }
    
    func getAutoType() -> AutoType? {
        return autoType
    }
    
    func getActivityStatus() -> Bool {
        return isActive
    }
    
    func getEventer() -> NLRXDevice? {
        return eventer
    }
    
    func getParameter() -> Int? {
        guard parameterOrIncomeCommand == nil else {
            
            return parameterOrIncomeCommand
        }
        
        guard let autoType = autoType else {
            
            return nil
        }
        
        switch autoType {
        case .cooling, .heating:
            return DetailedAutoViewModel.defaultTemperature
        case .draining, .humidification:
            return DetailedAutoViewModel.defaultHumidity
        case .event:
            return DetailedAutoViewModel.defaultCommand
        case .unknown:
            return nil
        }
    }
    
    func getCommand() -> NLCommonCommand? {
        return command
    }
    
}

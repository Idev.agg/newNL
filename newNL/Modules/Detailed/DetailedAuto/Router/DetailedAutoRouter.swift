//
//  DetailedAutoDetailedAutoRouter.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

protocol DetailedAutoRouterProtocol: BaseRouterProtocol {
    
    func presentChooseItem(module: ChooseItemModule)
    func back()

}

class DetailedAutoRouter: BaseRouter, DetailedAutoRouterProtocol {
    
    func back() {
        transitionHandler.closeCurrent()
    }
    
    func presentChooseItem(module: ChooseItemModule) {
        transitionHandler.presentModallyOnSelf(module: module)
    }

}

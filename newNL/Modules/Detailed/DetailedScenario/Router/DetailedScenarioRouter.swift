//
//  DetailedScenarioDetailedScenarioRouter.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

protocol DetailedScenarioRouterProtocol: BaseRouterProtocol {

}

class DetailedScenarioRouter: BaseRouter, DetailedScenarioRouterProtocol {

}

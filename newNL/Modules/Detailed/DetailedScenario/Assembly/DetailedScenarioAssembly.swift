//
//  DetailedScenarioDetailedScenarioAssembly.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol DetailedScenarioModuleInputProtocol: ModuleInputProtocol {

}

protocol DetailedScenarioModuleOutputProtocol {

}


final class DetailedScenarioModule: Module {

    private(set) var view: UIViewController
    private(set) var input: DetailedScenarioModuleInputProtocol

    private init(view: DetailedScenarioViewController, input: DetailedScenarioModuleInputProtocol) {

	self.view = view
	self.input = input
    }

    static func assembly() ->  DetailedScenarioModule{

        let router = DetailedScenarioRouter()
        let presenter = DetailedScenarioPresenter()
        let view = DetailedScenarioViewController()
        let interactor = DetailedScenarioInteractor()

        view.output = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        router.transitionHandler = view
	
        let module = DetailedScenarioModule(view: view, input: presenter)
        return module

    }

}

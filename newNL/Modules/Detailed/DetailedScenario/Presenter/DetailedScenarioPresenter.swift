//
//  DetailedScenarioDetailedScenarioPresenter.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

class DetailedScenarioPresenter: BasePresenter, DetailedScenarioModuleInputProtocol, DetailedScenarioViewOutputProtocol {

    weak var view: DetailedScenarioViewInputProtocol!
    var interactor: DetailedScenarioInteractorProtocol!
    var router: DetailedScenarioRouterProtocol!
    var output: DetailedScenarioModuleOutputProtocol!

}

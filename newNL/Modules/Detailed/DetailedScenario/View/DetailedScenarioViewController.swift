//
//  DetailedScenarioDetailedScenarioViewController.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol DetailedScenarioViewInputProtocol: class {

    /**
        @author Sergey
        Setup initial state of the view
    */

    func setupInitialState()
}

protocol DetailedScenarioViewOutputProtocol {

    /**
        @author Sergey
        Notify presenter that view is ready
    */

}

class DetailedScenarioViewController: BaseViewController, DetailedScenarioViewInputProtocol {

    var output: DetailedScenarioViewOutputProtocol!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    // MARK: DetailedScenarioViewInputProtocol
    func setupInitialState() {
    }
}

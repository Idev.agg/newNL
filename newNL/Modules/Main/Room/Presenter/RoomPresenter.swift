//
//  RoomRoomPresenter.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit

enum ScreenType {
    case house, room
}

class RoomPresenter: BasePresenter, RoomModuleInputProtocol, RoomViewOutputProtocol {

    weak var view: RoomViewInputProtocol!
    var interactor: RoomInteractorProtocol!
    var router: RoomRouterProtocol!
    var output: RoomModuleOutputProtocol!
    
    private var devices: [NLBaseDevice]?
    private var scenarious: [NLScenario]?
    private var room: NLRoom?
    private var house: NLHouse?
    private var type: ScreenType
    
    init(type: ScreenType) {
        self.type = type
        super.init()
    }
    
    var scenariousLoaded: Bool = false
    
    var devicesLoaded: Bool = false
    
    func viewLoaded() {
        interactor.fetchData(delegate: self, infoDelegate: self)
    }
    
    func getDevicesCount() -> Int {
        return devices?.count ?? 0
    }
    
    func getTitle() -> String? {
        switch self.type {
        case .house:
            return self.house?.name
        case .room:
            return self.room?.name
        }
    }
    
    func getScenariousCount() -> Int {
        return scenarious?.count ?? 0
    }
    
    func getDevice(number: Int) -> NLBaseDevice? {
        guard let devices = devices else { return nil }
        return devices[number]
    }
    
    func getScenario(number: Int) -> NLScenario? {
        guard let scenarious = scenarious else { return nil }
        return scenarious[number]
    }
    
    func getScenarious() -> [NLScenario]? {
        return scenarious
    }
    
    func didSelectCell(_ ip: IndexPath) {
        switch ip.section {
        case 1:
            guard let scenario = getScenario(number: ip.item) else { return }
            let cmd = scenario.runCommand()
            interactor.sendCommandToController(cmd) { [weak self] in
                self?.view.reloadData()
            }
        case 3:

            let device = getDevice(number: ip.item)
            switch device?.deviceType {
            case .ftx, .tx:
                let cmd = device?.prepareCommandStr(command: .switching) ?? String.empty
                interactor.sendCommandToController(cmd) { [weak self] in
                    Thread.performAfter(seconds: 0.1) {
                        self?.interactor.updateStates()
                    }
                }
            default:
                return
            }
        default:
            return
        }
    }

}

extension RoomPresenter: DevicesReceiveDelegate, HouseInfoReceiveDelegate {
    
    func setupDevices(devices: [NLBaseDevice]?) {
        switch type {
        case .house:
            self.devices = devices
        case .room:
            self.devices = self.room?.getDevicesList()
        }
    }
    
    func didReceiveDevices(devices: [NLBaseDevice]?) {
        self.setupDevices(devices: devices)
        devicesLoaded = true
        ModelsManager.shared.updateStates()
        view.reloadData()
        
    }
    
    func didReceiveScenarious(scenarious: [NLScenario]?) {
        self.scenarious = scenarious
        scenariousLoaded = true
        view.reloadData()
        
    }
    
    func didReceiveHouseInfo(house: NLHouse?, rooms: [NLRoom]?) {
        self.house = house
        if let house = house, let rooms = house.getVisibleRooms() {
            self.room = rooms[house.currentRoomIndex]
        }
        self.setupDevices(devices: house?.getDeviceList())
        view.reloadData()
    }
    
    func didUpdateStates() {
        view.reloadData()
    }
    
}

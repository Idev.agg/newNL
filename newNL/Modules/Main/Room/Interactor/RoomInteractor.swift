//
//  RoomRoomInteractor.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//


protocol RoomInteractorProtocol: class {
    
    func fetchData(delegate: DevicesReceiveDelegate, infoDelegate: HouseInfoReceiveDelegate)
    func sendCommandToController(_ cmdStr: String, handler: @escaping Closure)
    func updateStates()
    
}

class RoomInteractor: BaseInteractor, RoomInteractorProtocol {
    
    var modelsManager: ModelsManager
    var requestManager: RequestManager
    
    init(modelsManager: ModelsManager = ModelsManager.shared, reqManager: RequestManager = RequestManager.shared) {
        self.modelsManager = modelsManager
        self.requestManager = reqManager
    }
    
    func fetchData(delegate: DevicesReceiveDelegate, infoDelegate: HouseInfoReceiveDelegate) {
        modelsManager.devicesDelegate = delegate
        modelsManager.houseInfoDelegate = infoDelegate
        modelsManager.fetchDevicesInfo()
        modelsManager.fetchHouseInfo()
        modelsManager.fetchScenarious()
    }
    
    func sendCommandToController(_ cmdStr: String, handler: @escaping Closure) {
        requestManager.sendCommand(strCommand: cmdStr) { (result) in
            handler()
        }
    }
    
    func updateStates() {
        modelsManager.updateStates()
    }

}

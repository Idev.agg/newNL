//
//  RoomRoomViewController.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol RoomViewInputProtocol: AlertPresenterProtocol, LoadingViewPresenterProtocol {
    
    func reloadData()
    func setupInitialState()
    
}

protocol RoomViewOutputProtocol {
    
    var scenariousLoaded: Bool {get set}
    var devicesLoaded: Bool {get set}
    
    func viewLoaded()
    func getDevicesCount() -> Int
    func getScenarious() -> [NLScenario]?
    func getScenariousCount() -> Int
    func getDevice(number: Int) -> NLBaseDevice?
    func getScenario(number: Int) -> NLScenario?
    
    func getTitle() -> String?
    
    func didSelectCell(_ ip: IndexPath)
    
}

class RoomViewController: BaseViewController, RoomViewInputProtocol {

    var output: RoomViewOutputProtocol!

    @IBOutlet weak var contentCollection: UICollectionView!
    
    let progress = ProgressView()
    
    override var isMenuAvailable: Bool {
        return true
    }
    
    override var titleText: String {
        return self.tabBarController?.selectedIndex == 0 ? "Дом" : "Комната"
    }
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        setupInitialState()

        
        configureViews()
    }
    
    private func registerCells() {
        contentCollection.register(UINib(nibName: "DeviceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: DeviceCollectionViewCell.REUSE_ID)
        contentCollection.register(UINib(nibName: "FDeviceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: FDeviceCollectionViewCell.REUSE_ID)
        contentCollection.register(UINib(nibName: "ThermostatCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ThermostatCollectionViewCell.REUSE_ID)
        contentCollection.register(UINib(nibName: "TemperatureSensorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: TemperatureSensorCollectionViewCell.REUSE_ID)
        contentCollection.register(UINib(nibName: "HeaderCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: HeaderCollectionViewCell.REUSE_ID)
        contentCollection.register(UINib(nibName: "ScenariousCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ScenariousCollectionViewCell.REUSE_ID)
    }
    
    func reloadData() {
        self.contentCollection.reloadData()
        if let title = output.getTitle() {
            self.setTitleText(text: title)
            var itemTitle = title
            let parts = title.split(separator: Character(String.space))
            if parts.count > 1, let firstPart = parts.first, firstPart.count > 10 {
                itemTitle = "\(firstPart)"
            } else if title.count > 20 {
                itemTitle = "\(title.suffix(20))"
            }
            self.title = itemTitle
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        output.viewLoaded()
    }
    
    
    private func configureViews() {
    }

    // MARK: RoomViewInputProtocol
    func setupInitialState() {
    }
}

extension RoomViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 4 // remake
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return section != 3 ? 1 : output.getDevicesCount()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        switch indexPath.section {
        case 0,2:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: HeaderCollectionViewCell().reuseIdentifier!, for: indexPath)
            (cell as! HeaderCollectionViewCell).configure(section: indexPath.section, isFavorites: false)
        case 1:
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: ScenariousCollectionViewCell().reuseIdentifier!, for: indexPath)
            (cell as! ScenariousCollectionViewCell).configureWitshScenarious(scenarious: output.getScenarious()) { [weak self] in
                self?.output.didSelectCell(indexPath)
            }
        case 3:
            guard let device = output.getDevice(number: indexPath.row) else { return cell }
            cell = cell.cast(object: device, collection: collectionView, indexPath: indexPath)
        default:
            return cell
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        output.didSelectCell(indexPath)
    }
    
    
}

extension RoomViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return section != 3 ? UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0) : UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        switch indexPath.section {
        case 0, 2:
            return CGSize(width: UIScreen.main.bounds.width, height: size50)
        case 1:
            return CGSize(width: UIScreen.main.bounds.width, height: size120)
        case 3:
            let size = (UIScreen.main.bounds.width - size20 * 2) / 3.2
            return CGSize(width: size, height: size)
        default:
            return CGSize(width: zeroConstant, height: zeroConstant)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return size10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return size5
    }
    
    
}


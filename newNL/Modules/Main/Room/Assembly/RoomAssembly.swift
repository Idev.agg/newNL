//
//  RoomRoomAssembly.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol RoomModuleInputProtocol: ModuleInputProtocol {

}

protocol RoomModuleOutputProtocol {

}


final class RoomModule: Module {

    private(set) var view: UIViewController
    private(set) var input: RoomModuleInputProtocol?

    private init(view: UIViewController, input: RoomModuleInputProtocol?) {

	self.view = view
	self.input = input
    }

    static func assembly(type: ScreenType) ->  RoomModule {

        let router = RoomRouter()
        let presenter = RoomPresenter(type: type)
        let view = RoomViewController()
        let interactor = RoomInteractor()

        view.output = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        router.transitionHandler = view
	
        let module = RoomModule(view: view, input: presenter)
        return module

    }
    
    static func assembly() -> RoomModule {
        return RoomModule.init(view: UIViewController(), input: nil)
    }

}

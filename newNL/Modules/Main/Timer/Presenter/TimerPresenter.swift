//
//  TimerTimerPresenter.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit

class TimerPresenter: BasePresenter, TimerModuleInputProtocol, TimerViewOutputProtocol {

    weak var view: TimerViewInputProtocol!
    var interactor: TimerInteractorProtocol!
    var router: TimerRouterProtocol!
    var output: TimerModuleOutputProtocol!
    
    private var timers: [NLTimer]?
    
    func viewLoaded() {
        interactor.getTimers(delegate: self)
    }

    func getRowsCount() -> Int {
        return timers?.count ?? 0
    }
    
    func getTimer(for row: Int) -> NLTimer? {
        guard let timers = timers else {
            return nil
        }
        return timers[row]
    }
    
    func selectTimer(row: Int) {
        let module = DetailedTimerModule.assembly(timer: getTimer(for: row), output: self)
        router.pushTimerDetails(module: module)
    }
    
    func showSettingsForRow(_ row: Int) {
        guard let timers = timers else { return }
        let timerIsActive = timers[row].isActive ?? false
        let models: [MenuCellModel] = [timerIsActive ? AutoActions.deactivate : AutoActions.activate, AutoActions.delete].map { $0.menuCellModel }
        models.last?.isLast = true
        
        let chooseItemModule = ChooseItemModule.assembly(with: models) { [weak self] (selectedModel) in
            if selectedModel is MenuCellModel {
                if (selectedModel as! MenuCellModel) == AutoActions.deactivate.menuCellModel {
                    timers[row].isActive = !timerIsActive
                    self?.updateTimer(timers[row])
                } else if (selectedModel as! MenuCellModel) == AutoActions.activate.menuCellModel {
                    timers[row].isActive = !timerIsActive
                    self?.updateTimer(timers[row])
                } else if (selectedModel as! MenuCellModel) == AutoActions.delete.menuCellModel {
                    self?.removeTimer(timers[row])
                }
            }
        }
        router.showSettings(module: chooseItemModule)
    }
    
    func addTimer() {
        let module = DetailedTimerModule.assembly(timer: nil, output: self)
        router.pushTimerDetails(module: module)
    }
     
    private func removeTimer(_ timer: NLTimer) {
        var data = StoreFileManager.shared.getTimerBinData() ?? Data()
        if (data.count == 0) {
            data = Prefixes.timerBin.getStringPrefix().toDataWithCount(count: 6) + Data.emptyDataWithCount(count: 8192)
        }
        data = data.byRemovingTimer(timer)
        interactor.saveTimers(data: data) { [weak self] (success) in
            (self?.view as? BaseViewController)?.presentSimpleAlert(with: "", text: "Таймер успешно удален", acceptTitle: "Ок", acceptHandler: {
                guard let wrappedSelf = self else { return }
                ModelsManager.shared.removeTimers()
                wrappedSelf.interactor.getTimers(delegate: wrappedSelf)
            })
        }
    }
    
    private func updateTimer(_ timer: NLTimer) {
        var data = StoreFileManager.shared.getTimerBinData() ?? Data()
        if (data.count == 0) {
            data = Prefixes.timerBin.getStringPrefix().toDataWithCount(count: 6) + Data.emptyDataWithCount(count: 12288)
        }
        data = data.byAddingTimer(timer)
        interactor.saveTimers(data: data) { [weak self] (success) in
            guard let wrappedSelf = self else { return }
            ModelsManager.shared.removeTimers()
            wrappedSelf.interactor.getTimers(delegate: wrappedSelf)
        }
    }
    
    func getSectionsCount() -> Int {
        return timers?.count ?? 0 > 0 ? 2 : 1
    }
    
}

extension TimerPresenter: TimerReceiveDelegate {
    
    func didReceiveTimers(timers: [NLTimer]?) {
        self.timers = timers
        view.reloadUI()
    }
    
}

extension TimerPresenter: DetailedTimerModuleOutputProtocol {
    
    func didSaveTimer() {
        ModelsManager.shared.removeTimers()
        interactor.getTimers(delegate: self)
    }
    
    
}

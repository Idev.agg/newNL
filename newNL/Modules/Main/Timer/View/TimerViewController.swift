//
//  TimerTimerViewController.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol TimerViewInputProtocol: AlertPresenterProtocol, LoadingViewPresenterProtocol {
    
    func reloadUI()
    func setupInitialState()
    
}

protocol TimerViewOutputProtocol {
    
    func viewLoaded()
    func getRowsCount() -> Int
    func getTimer(for row: Int) -> NLTimer?
    func selectTimer(row: Int)
    func showSettingsForRow(_ row: Int)
    func getSectionsCount() -> Int
    func addTimer()

}

class TimerViewController: BaseViewController, TimerViewInputProtocol {

    var output: TimerViewOutputProtocol!
    
    override var titleText: String {
        get {
            return "Таймеры"
        }
    }
    
    override var isMenuAvailable: Bool {
        return true
    }
    
    @IBOutlet weak var timersTable: UITableView!
    

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareViews()
        output.viewLoaded()
        self.showLoadingView(show: true, description: "загрузка")
    }
    
    func setNavigationButtons() {
        rightButtonHandler = {
            debugPrintText(text: "right timer")
        }
    }

    func prepareViews() {
        timersTable.register(UINib(nibName: "TimerTableViewCell", bundle: nil), forCellReuseIdentifier: "timerCell")
        timersTable.tableFooterView = UIView()
    }

    func reloadUI() {
        timersTable.reloadData()
        self.showLoadingView(show: false, description: "загрузка")
    }

    // MARK: TimerViewInputProtocol
    func setupInitialState() {
        
    }
}

extension TimerViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return output.getSectionsCount()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if output.getSectionsCount() == 2, section == 0 {
            return output.getRowsCount()
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if output.getSectionsCount() == 2, indexPath.section == 0 {
            guard let cell = timersTable.dequeueReusableCell(withIdentifier: "timerCell") as? TimerTableViewCell, let timer = output.getTimer(for: indexPath.row) else {
                let cell = UITableViewCell()
                cell.backgroundColor = UIColor.yellow
                return UITableViewCell()
            }
            cell.configure(with: timer)
            cell.delegate = self
            return cell
        } else {
            var cell = timersTable.dequeueReusableCell(withIdentifier: "addAutoCell") as? AddAutoOrTimerTableViewCell
            if cell == nil {
                cell = AddAutoOrTimerTableViewCell(delegate: self, reuseIdentifier: "addAutoCell")
            }
            return cell ?? UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output.selectTimer(row: indexPath.row)
    }
    
}

extension TimerViewController: CellWithSettingsDelegate {
    
    func settingsPressed(cell: UITableViewCell) {
        guard let row = timersTable.indexPath(for: cell)?.row else { return }
        output.showSettingsForRow(row)
    }
    
}

extension TimerViewController: AddAutoOrTimerTableViewCellDelegate {
    
    func addAutoOrTimerCellPressed() {
        output.addTimer()
    }

}

//
//  TimerTimerAssembly.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol TimerModuleInputProtocol: ModuleInputProtocol {

}

protocol TimerModuleOutputProtocol {
}


final class TimerModule: Module {

    private(set) var view: UIViewController
    private(set) var input: TimerModuleInputProtocol

    private init(view: TimerViewController, input: TimerModuleInputProtocol) {

	self.view = view
	self.input = input
    }

    static func assembly() ->  TimerModule{

        let router = TimerRouter()
        let presenter = TimerPresenter()
        let view = TimerViewController()
        let interactor = TimerInteractor()

        view.output = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        router.transitionHandler = view
	
        let module = TimerModule(view: view, input: presenter)
        return module

    }

}

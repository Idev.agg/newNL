//
//  TimerTimerRouter.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

protocol TimerRouterProtocol: BaseRouterProtocol {
    
    func pushTimerDetails(module: DetailedTimerModule)
    func showSettings(module: ChooseItemModule)
        
}

class TimerRouter: BaseRouter, TimerRouterProtocol {
    
    func pushTimerDetails(module: DetailedTimerModule) {
        transitionHandler.push(module: module)
    }
    
    func showSettings(module: ChooseItemModule) {
        transitionHandler.presentModallyOnSelf(module: module)
    }

}

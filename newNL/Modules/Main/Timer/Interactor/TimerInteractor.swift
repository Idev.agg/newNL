//
//  TimerTimerInteractor.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit

protocol TimerInteractorProtocol: class {
    
    func getTimers(delegate: TimerReceiveDelegate)
    func saveTimers(data: Data, handler: @escaping ((Bool) -> ()))

}

class TimerInteractor: BaseInteractor, TimerInteractorProtocol {
    
    var modelsManager: ModelsManager
    
    init(modelsManager: ModelsManager = ModelsManager.shared) {
        self.modelsManager = modelsManager
    }
    
    func getTimers(delegate: TimerReceiveDelegate) {
        modelsManager.timersDelegate = delegate
        modelsManager.fetchAutos()
        modelsManager.fetchTimers()
    }
    
    func saveTimers(data: Data, handler: @escaping ((Bool) -> ())) {
        RequestManager.shared.requestTurnControl(taskType: .uploadTask, file: .timerBin, data: data, isFromTurn: false) { (result) in
            if result.value is CommandResponse {
                if (result.value as! CommandResponse).success {
                    handler(true)
                }
            }
        }
    }
    
}

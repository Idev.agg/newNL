//
//  SideMenuSideMenuInteractor.swift
//  Juno-iOS
//
//  Created by Сергей Глеб on 17/12/2018.
//  Copyright © 2018 elatesoftware. All rights reserved.
//


protocol SideMenuInteractorProtocol: class {

    func getRoomsInfo(delegate: HouseInfoReceiveDelegate)
    
}

class SideMenuInteractor: BaseInteractor, SideMenuInteractorProtocol {
    
    func getRoomsInfo(delegate: HouseInfoReceiveDelegate) {
        ModelsManager.shared.houseInfoDelegate = delegate
        ModelsManager.shared.fetchHouseInfo()
    }

}

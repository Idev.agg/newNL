//
//  SideMenuSideMenuPresenter.swift
//  Juno-iOS
//
//  Created by Сергей Глеб on 17/12/2018.
//  Copyright © 2018 elatesoftware. All rights reserved.
//

import UIKit

enum SideMenuStatus {
    case hidden, showen
}

protocol SideMenuDelegate: class {
    
    func didDisplay()
    func didHide(withSelection: Bool)
    func tappedToHide(withSelection: Bool)
    
}

class SideMenuPresenter: BasePresenter, SideMenuModuleInputProtocol, SideMenuViewOutputProtocol {
    
    weak var view: SideMenuViewInputProtocol!
    var interactor: SideMenuInteractorProtocol!
    var router: SideMenuRouterProtocol!
    var output: SideMenuModuleOutputProtocol!
    
    internal weak var delegate: SideMenuDelegate?
    
    var status: SideMenuStatus = .hidden
    
    private var settingsArray: [MenuCellModel] {
        let arr = [MenuCellModel(image: #imageLiteral(resourceName: "roomWhite"), text: "Настройка комнат", textColor: white),
        MenuCellModel(image: #imageLiteral(resourceName: "sets"), text: "Настройки", textColor: white),
        MenuCellModel(image: #imageLiteral(resourceName: "info"), text: "О программе", textColor: white)]
        arr.last?.isLast = true
        return arr
    }
    
    private var rooms: [NLRoom]?
    private var house: NLHouse?
    
    func menuLoaded() {
    }
    
    func fetchHouseInfo() {
        self.interactor.getRoomsInfo(delegate: self)
    }
    
    func didClose(withSelection: Bool) {
        status = .hidden
        guard let delegate = delegate else { return }
        delegate.didHide(withSelection: withSelection)
    }
    
    func selectRoom(index: Int) {
        self.house?.currentRoomIndex = index
    }
    
    func becomeVisible() {
        status = .showen
        guard let delegate = delegate else { return }
        delegate.didDisplay()
        fetchHouseInfo()
    }
    
    func didTappedHide(withSelection: Bool) {
        delegate?.tappedToHide(withSelection: withSelection)
    }
    
    func getHouseName() -> String {
        return house?.name ?? String.empty
    }
    
    func getRoomsCount() -> Int {
        return rooms?.count ?? 0
    }
    
    func getRoomFor(row: Int) -> MenuCellModel {
        guard let rooms = rooms else {
            return MenuCellModel()
        }
        return MenuCellModel(room: rooms[row])
    }
    
    func getSettingFor(row: Int) -> MenuCellModel {
        return settingsArray[row]
    }
    
    func getSettingsCount() -> Int {
        return settingsArray.count
    }
    
}

extension SideMenuPresenter: HouseInfoReceiveDelegate {
    
    func didReceiveHouseInfo(house: NLHouse?, rooms: [NLRoom]?) {
        self.house = house
        self.rooms = house?.getVisibleRooms()
        self.view.reloadUi()
    }
    
}


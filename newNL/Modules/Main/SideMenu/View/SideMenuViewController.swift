//
//  SideMenuSideMenuViewController.swift
//  Juno-iOS
//
//  Created by Сергей Глеб on 17/12/2018.
//  Copyright © 2018 elatesoftware. All rights reserved.
//

import UIKit


protocol SideMenuViewInputProtocol: class {
    
    func setupInitialState()
    func reloadUi()
    
}

protocol SideMenuViewOutputProtocol {
    
    var status: SideMenuStatus { get }
    var delegate: SideMenuDelegate? { get set }
    
    func becomeVisible()
    func menuLoaded()
    func didClose(withSelection: Bool)
    func didTappedHide(withSelection: Bool)
    func fetchHouseInfo()
    func selectRoom(index: Int)
    
    func getHouseName() -> String
    func getRoomsCount() -> Int
    func getSettingsCount() -> Int
    func getRoomFor(row: Int) -> MenuCellModel
    func getSettingFor(row: Int) -> MenuCellModel
    
}

class SideMenuViewController: UIViewController, SideMenuViewInputProtocol {
    
    @IBOutlet private var tapViewWidth: NSLayoutConstraint!
    @IBOutlet private weak var tapView: UIView!
    @IBOutlet var houseName: UILabel!
    @IBOutlet var settingsTable: UITableView!
    @IBOutlet var roomsTable: UITableView!
    
    
    var activeWidth: CGFloat {
        return UIScreen.main.bounds.width - tapViewWidth.constant
    }

    var status: SideMenuStatus { return output.status }
    weak var delegate: SideMenuDelegate? {
        didSet {
            output.delegate = delegate
        }
    }

    var output: SideMenuViewOutputProtocol!
    

    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        registerCells()
        setupUI()
        output.menuLoaded()
    }
    
    func registerCells() {
        roomsTable.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: MenuTableViewCell.REUSE_ID)
        settingsTable.register(UINib(nibName: "MenuTableViewCell", bundle: nil), forCellReuseIdentifier: MenuTableViewCell.REUSE_ID)
    }

    func becomeVisible() {
        output.becomeVisible()
    }
    
    func didHide(withSelection: Bool) {
        output.didClose(withSelection: withSelection)
    }
    
    func reload() {
        roomsTable.reloadData()
        settingsTable.reloadData()
        houseName.text = output.getHouseName()
    }
    
    private func setupUI() {
        view.layoutSubviews()
        setTap()
    }
    
    private func setTap() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(emptyZoneTapped))
        tapView.addGestureRecognizer(tap)
    }
    
    @objc private func emptyZoneTapped(sender: UITapGestureRecognizer) {
        output.didTappedHide(withSelection: false)
    }
    
    // MARK: SideMenuViewInputProtocol
    func setupInitialState() {
    }
    
    func reloadUi() {
        self.roomsTable.reloadData()
        self.houseName.text = output.getHouseName()
    }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == roomsTable ? output.getRoomsCount() : output.getSettingsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = tableView == settingsTable ? output.getSettingFor(row: indexPath.row) : output.getRoomFor(row: indexPath.row)
        let cell = UITableViewCell().cast(object: model, tableView: tableView, indexPath: indexPath)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView == settingsTable ? size50 : UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == roomsTable {
            output.selectRoom(index: indexPath.row)
            output.didTappedHide(withSelection: true)
        }
        debugPrintText(text: "\(indexPath)")
    }
    
}

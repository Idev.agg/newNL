//
//  SideMenuSideMenuAssembly.swift
//  Juno-iOS
//
//  Created by Сергей Глеб on 17/12/2018.
//  Copyright © 2018 elatesoftware. All rights reserved.
//

import UIKit


protocol SideMenuModuleInputProtocol: ModuleInputProtocol {

}

protocol SideMenuModuleOutputProtocol {
    
}


final class SideMenuModule: Module {

    private(set) var view: UIViewController
    private(set) var input: SideMenuModuleInputProtocol

    private init(view: SideMenuViewController, input: SideMenuModuleInputProtocol) {

	self.view = view
	self.input = input
    }

    static func assembly() ->  SideMenuModule{

        let router = SideMenuRouter()
        let presenter = SideMenuPresenter()
        let view = SideMenuViewController()
        let interactor = SideMenuInteractor()

        view.output = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        router.transitionHandler = view
	
        let module = SideMenuModule(view: view, input: presenter)
        return module

    }

}

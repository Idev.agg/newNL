//
//  MenuCellModel.swift
//  newNL
//
//  Created by aggrroo on 3/17/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

protocol Displayable {
    
    var displayName: String { get }
    
}

class MenuCellModel: Equatable {

    var image: UIImage?
    var text: String?
    var value: Displayable?
    var textColor: UIColor?
    var isLast: Bool = false
    
    init(image: UIImage? = nil, text: String? = nil, textColor: UIColor? = black) {
        self.image = image
        self.text = text
        self.textColor = textColor
    }
    
    convenience init(value: Displayable) {
        self.init()
        self.value = value
    }
    
    convenience init(room: NLRoom) {
        self.init()
        if let index = room.iconIndex, index < Images.roomsIcons.count {
            image = Images.roomsIcons[index]
        }
        text = room.name
    }
    
    static func == (lhs: MenuCellModel, rhs: MenuCellModel) -> Bool {
        if lhs.value != nil, rhs.value != nil {
            return lhs.value?.displayName == rhs.value?.displayName
        } else {
            return lhs.text == rhs.text
        }
    }
    
}

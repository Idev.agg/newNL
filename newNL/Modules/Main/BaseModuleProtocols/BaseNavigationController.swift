//
//  BaseNavigationController.swift
//  newNL
//
//  Created by aggrroo on 26.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation
import UIKit

class BaseNavigationController: UINavigationController {
    
    var currentController: BaseViewController? {
        return self.topViewController as? BaseViewController
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setNavigationBarHidden(true, animated: false)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        interactivePopGestureRecognizer?.delegate = self
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var preferredInterfaceOrientationForPresentation: UIInterfaceOrientation {
        return .portrait
    }
    
}

extension BaseNavigationController: UIGestureRecognizerDelegate, UINavigationControllerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        guard gestureRecognizer == interactivePopGestureRecognizer else { return true }
        return viewControllers.count > 1
    }
    
    
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
}

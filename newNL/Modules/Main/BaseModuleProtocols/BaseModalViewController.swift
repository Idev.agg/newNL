//
//  BaseModalViewController.swift
//  newNL
//
//  Created by aggrroo on 3/18/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class BaseModalViewController: UIViewController {
    
    @IBOutlet var shadowView: UIView?
    @IBOutlet var contentView: UIView?
    @IBOutlet var contentTopConstraint: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.contentTopConstraint?.constant = UIScreen.main.bounds.height
    }

    override func viewWillAppear(_ animated: Bool) {
        appearAnimated()
    }
    
    func appearAnimated() {
        self.shadowView?.alpha = zeroConstant
        self.contentTopConstraint?.constant = zeroConstant
        UIView.animate(withDuration: BASE_ANIMATION_DURATION / 2) { [weak self] in
            self?.shadowView?.alpha = ALPHA_60
            self?.view.layoutSubviews()
        }
    }
    
}

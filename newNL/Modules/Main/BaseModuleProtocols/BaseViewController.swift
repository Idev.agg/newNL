//
//  BaseViewController.swift
//  newNL
//
//  Created by aggrroo on 25.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit
import Foundation



class BaseViewController: UIViewController {
    
    @IBOutlet var navigationView: NavigationView!
    @IBOutlet var menuView: UIView?
    var tap: UITapGestureRecognizer?
    
    @IBOutlet var bottomConstraint: NSLayoutConstraint?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        
        return .lightContent
    }
    
    var isMenuAvailable: Bool {
        return false
    }
    
    var titleText: String {
        get {
            return String.empty
        }
    }
    
    
    var rightButtonHandler: (() -> ())?
    var leftButtonHandler: (() -> ())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuView?.isHidden = true
        
        self.navigationController?.viewControllers.count == 1 ? configureMainNavigation() : configureNavigation()
        
        guard let navigation = navigationController, navigation.viewControllers.count > 1 else { return }
        view.layoutIfNeeded()
    }
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        commonInit()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    @objc func keyboardWillAppear(_ notification: Notification) {
        if let keyboardFrame: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
            let keyboardRectangle = keyboardFrame.cgRectValue
            let keyboardHeight = keyboardRectangle.height
            bottomConstraint?.constant = keyboardHeight
        }
        tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        view.addGestureRecognizer(tap!)
    }
    
    @objc func keyboardWillHide() {
        self.bottomConstraint?.constant = 0
        guard tap != nil else { return }
        view.removeGestureRecognizer(tap!)
        tap = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AppDelegate.appDelegate().currentController = self
    }
    
    func commonInit() {
        debugInizializationMessage(someClass: self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
    func isTabBarHidden() -> Bool { return self.navigationController?.tabBarController?.tabBar.isHidden ?? true }
    
    func getTabBarHeight() -> CGFloat { return self.navigationController?.tabBarController?.tabBar.frame.height ?? 0 }

    func setTitleText(text: String) {
        self.navigationView.title.text = text
    }
    
    func configureNavigation() {
        if (navigationView == nil) {
            navigationView = NavigationView()
            view.backgroundColor = black
            view.addSubview(navigationView)
            navigationView.autoPinEdge(toSuperviewEdge: .left)
            navigationView.autoPinEdge(toSuperviewEdge: .right)
            navigationView.autoPin(toTopLayoutGuideOf: self, withInset: 0)
            navigationView.autoSetDimension(.height, toSize: 50)
        }
        navigationView.configure(titleText: titleText, left: nil, right: nil)
    }
    
    func configureMainNavigation() {
        let menuButton = Button.menu
        menuButton.configure { [weak self] in
            guard self?.tabBarController is BaseTabBarController else { return }
            (self?.tabBarController as! BaseTabBarController).revealMenu()
        }
        let settingsButton = Button.settings
        settingsButton.configure { [weak self] in
            let arr = [MenuCellModel(text: "Привязка"), MenuCellModel(text: "Сценарии"), MenuCellModel(text: "IP")]
            arr.last?.isLast = true
            let chooseItemModule = ChooseItemModule.assembly(with: arr, handler: { (item) in
                if (item as? MenuCellModel)?.text == "Привязка" {
                    let module = AttachmentFirstStepModule.assembly()
                    AttachmentManager.shared.prepare()
                    self?.push(module: module)
                } else if (item as? MenuCellModel)?.text == "IP" {
                    //TEMP
                    let ud = UserDefaults.standard
                    
                    let alert = UIAlertController(title: "Введите IP", message: nil, preferredStyle: .alert)
                    alert.addTextField(configurationHandler: { (textField) in
                    })
                    let save = UIAlertAction(title: "Сохранить", style: .default, handler: { (action) in
                        ud.set(alert.textFields![0].text, forKey: "ipipip")
                        ud.synchronize()
                    })
                    alert.addAction(save)
                    self?.present(alert, animated: true, completion: nil)
                }
            })
            self?.presentModallyOnSelf(module: chooseItemModule)
        }
        navigationView.configure(titleText: titleText, left: menuButton, right: settingsButton)
    }
    
    private func showMenu() {
        debugPrintText(text: "menu")
    }
    
}



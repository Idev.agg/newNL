//
//  TransitionHandler.swift
//  newNL
//
//  Created by aggrroo on 26.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation
import UIKit

protocol ModuleTransitionHandlerProtocol: class {
    
    func push<T: BaseModule>(module: T)
    func closeCurrent()
    func presentModallyOnSelf<T: BaseModule>(module: T)
    func removeModal()
    func toRoot()
    
}

extension ModuleTransitionHandlerProtocol {
    
    // call methods from VC extension :ModuleTHP
    
}

extension UIViewController: ModuleTransitionHandlerProtocol {
    
    func push<T>(module: T) where T : BaseModule {
        guard let navigationController = self.navigationController else { return }
        guard let view = module.view as? UIViewController else { return }
        view.hidesBottomBarWhenPushed = true
        navigationController.pushViewController(view, animated: true)
    }
    
    func closeCurrent() {
        guard let navigationController = self.navigationController, navigationController.viewControllers.count > 1 else { return }
        navigationController.popViewController(animated: true)
    }
    
    func presentModallyOnSelf<T>(module: T) where T : BaseModule {
        guard let modalView = module.view as? UIViewController else { return }
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        modalView.modalPresentationStyle = .overFullScreen
        self.present(modalView, animated: false, completion: nil)
    }
    
    func removeModal() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func toRoot() {
        guard let navigationController = self.navigationController else { return }
        navigationController.popToRootViewController(animated: true)
    }
    
}

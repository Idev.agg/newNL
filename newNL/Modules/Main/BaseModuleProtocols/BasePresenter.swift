//
//  BasePresenter.swift
//  newNL
//
//  Created by aggrroo on 26.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

class BasePresenter {
    
    init() {
        debugInizializationMessage(someClass: self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
}

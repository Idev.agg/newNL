//
//  BaseRouter.swift
//  newNL
//
//  Created by aggrroo on 26.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

protocol BaseRouterProtocol: class {
    
    // base navigation
    
}

class BaseRouter: BaseRouterProtocol {
    
    weak var transitionHandler: ModuleTransitionHandlerProtocol!
    
    init(transitionHandler: ModuleTransitionHandlerProtocol? = nil) {
        self.transitionHandler = transitionHandler
        debugInizializationMessage(someClass: self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
}

//
//  BaseModuleProtocols.swift
//  newNL
//
//  Created by aggrroo on 25.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

protocol ModuleInputProtocol: class {} // empty??

protocol BaseModule: class {
    
    associatedtype View
    
    var view: View { get }
    
    static func assembly() -> Self
    
}

protocol Module: BaseModule {
    
    associatedtype Input
    
    var input: Input { get }
    
}

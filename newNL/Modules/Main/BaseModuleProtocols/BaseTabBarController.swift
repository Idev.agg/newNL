//
//  BaseTabBarController.swift
//  newNL
//
//  Created by aggrroo on 26.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation
import UIKit

class BaseTabBarController: UITabBarController {
    
    var menuView: UIView?
    var shadowView: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        menuView = UIView()
        shadowView = UIView.initWith(color: gray)
        shadowView!.alpha = zeroConstant
        view.layoutIfNeeded()
        view.addSubviewWithZeroConstraintsToSuperview(subview: menuView!)
        menuView?.addSubviewWithZeroConstraintsToSuperview(subview: shadowView!)
        menuView?.isHidden = true
        setupMenu()
        tabBar.barTintColor = .black
        tabBar.barStyle = .blackOpaque
        tabBar.backgroundColor = .black
        tabBar.isTranslucent = false
        view.addGestureRecognizer(panGesture)
        view.addGestureRecognizer(screenEdgeGesture)
    }
    
    //not works 
    
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//        guard let items = tabBar.items else { return }
//        for item in items {
//            if let view = item.value(forKey: "view") as? UIView {
//                for subview in view.subviews {
//                    if subview is UILabel {
//                        (subview as! UILabel).minimumScaleFactor = 0.5
//                        (subview as! UILabel).numberOfLines = 2
//                    }
//                }
//            }
//        }
//    }
    
}

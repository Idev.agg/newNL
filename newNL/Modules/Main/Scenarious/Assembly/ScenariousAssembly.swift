//
//  ScenariousScenariousAssembly.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol ScenariousModuleInputProtocol: ModuleInputProtocol {

}

protocol ScenariousModuleOutputProtocol {

}


final class ScenariousModule: Module {

    private(set) var view: UIViewController
    private(set) var input: ScenariousModuleInputProtocol

    private init(view: ScenariousViewController, input: ScenariousModuleInputProtocol) {

	self.view = view
	self.input = input
    }

    static func assembly() ->  ScenariousModule{

        let router = ScenariousRouter()
        let presenter = ScenariousPresenter()
        let view = ScenariousViewController()
        let interactor = ScenariousInteractor()

        view.output = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        router.transitionHandler = view
	
        let module = ScenariousModule(view: view, input: presenter)
        return module

    }

}

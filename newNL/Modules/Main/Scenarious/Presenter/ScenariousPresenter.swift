//
//  ScenariousScenariousPresenter.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

class ScenariousPresenter: BasePresenter, ScenariousModuleInputProtocol, ScenariousViewOutputProtocol {

    weak var view: ScenariousViewInputProtocol!
    var interactor: ScenariousInteractorProtocol!
    var router: ScenariousRouterProtocol!
    var output: ScenariousModuleOutputProtocol!

}

//
//  ScenariousScenariousViewController.swift
//  home control
//
//  Created by Sergey on 14/11/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol ScenariousViewInputProtocol: class {

    /**
        @author Sergey
        Setup initial state of the view
    */

    func setupInitialState()
}

protocol ScenariousViewOutputProtocol {

    /**
        @author Sergey
        Notify presenter that view is ready
    */

}

class ScenariousViewController: BaseViewController, ScenariousViewInputProtocol {

    var output: ScenariousViewOutputProtocol!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }


    // MARK: ScenariousViewInputProtocol
    func setupInitialState() {
    }
}

//
//  AutoAutoRouter.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

protocol AutoRouterProtocol: BaseRouterProtocol {

    func showDetailedAuto(module: DetailedAutoModule)
    func showSettings(module: ChooseItemModule)
    
}

class AutoRouter: BaseRouter, AutoRouterProtocol {
    
    func showDetailedAuto(module: DetailedAutoModule) {
        transitionHandler.push(module: module)
    }
    
    func showSettings(module: ChooseItemModule) {
        transitionHandler.presentModallyOnSelf(module: module)
    }

}

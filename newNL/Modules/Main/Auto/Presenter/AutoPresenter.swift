//
//  AutoAutoPresenter.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit

enum AutoActions {
    case delete, activate, deactivate
    
    var menuCellModel: MenuCellModel {
        switch self {
        case .delete:
            return MenuCellModel(text: "Удалить")
        case .activate:
            return MenuCellModel(text: "Включить")
        case .deactivate:
            return MenuCellModel(text: "Выключить")
        }
    }
    
}

class AutoPresenter: BasePresenter, AutoModuleInputProtocol, AutoViewOutputProtocol {

    weak var view: AutoViewInputProtocol!
    var interactor: AutoInteractorProtocol!
    var router: AutoRouterProtocol!
    var output: AutoModuleOutputProtocol!
    
    private var autos: [NLAuto]?
    
    func viewLoaded() {
        view.showLoadingView(show: true, description: "загрузка")
        interactor.getAutos(delegate: self)
    }
    
    func getRowsCount(section: Int) -> Int {
        if getSectionsCount() == 2, section == 0 {
            return autos?.count ?? 0
        } else {
            return 1
        }
    }
    
    func getSectionsCount() -> Int {
        return (autos?.count ?? 0 > 0) ? 2 : 1
    }
    
    func getAuto(for row: Int) -> NLAuto? {
        guard let autos = autos else { return nil }
        return autos[row]
    }
    
    func selectAuto(row: Int) {
        guard let autos = autos else { return }
        let detailedAutoModel = DetailedAutoModule.assembly(auto: autos[row], output: self)
        router.showDetailedAuto(module: detailedAutoModel)
    }
    
    func addAuto() {
        let detailedAutoModel = DetailedAutoModule.assembly(auto: nil, output: self)
        router.showDetailedAuto(module: detailedAutoModel)
    }
    
    func settingsPressed(row: Int) {
        guard var autos = autos else { return }
        let autoActive = autos[row].isActive
        let models: [MenuCellModel] = [autoActive ? AutoActions.deactivate : AutoActions.activate, AutoActions.delete].map { $0.menuCellModel }
        models.last?.isLast = true
        let chooseItemModule = ChooseItemModule.assembly(with: models) { [weak self] (selectedModel) in
            if selectedModel is MenuCellModel {
                if (selectedModel as! MenuCellModel) == AutoActions.deactivate.menuCellModel {
                    debugPrint("deactivate")
                    autos[row].isActive = !autoActive
                    self?.updateAuto(autos[row])
                } else if (selectedModel as! MenuCellModel) == AutoActions.activate.menuCellModel {
                    debugPrint("activate")
                    autos[row].isActive = !autoActive
                    self?.updateAuto(autos[row])
                } else if (selectedModel as! MenuCellModel) == AutoActions.delete.menuCellModel {
                    debugPrint("delete")
                    self?.removeAuto(autos[row])
                }
            }
        }
        router.showSettings(module: chooseItemModule)
    }
     
    private func removeAuto(_ auto: NLAuto) {
        var data = StoreFileManager.shared.getAutoBinData() ?? Data()
        if (data.count == 0) {
            data = Prefixes.autoBin.getStringPrefix().toDataWithCount(count: 6) + Data.emptyDataWithCount(count: 12288)
        }
        data = data.byRemovingAuto(auto: auto)
        interactor.saveAutos(data: data) { [weak self] (success) in
            (self?.view as? BaseViewController)?.presentSimpleAlert(with: "", text: "Автоматизация успешно удалена", acceptTitle: "Ок", acceptHandler: {
                guard let wrappedSelf = self else { return }
                ModelsManager.shared.removeAutos()
                wrappedSelf.interactor.getAutos(delegate: wrappedSelf)
            })
        }
    }
    
    private func updateAuto(_ auto: NLAuto) {
        var data = StoreFileManager.shared.getAutoBinData() ?? Data()
        if (data.count == 0) {
            data = Prefixes.autoBin.getStringPrefix().toDataWithCount(count: 6) + Data.emptyDataWithCount(count: 12288)
        }
        data = data.byAddingAuto(auto: auto)
        interactor.saveAutos(data: data) { [weak self] (success) in
            guard let wrappedSelf = self else { return }
            ModelsManager.shared.removeAutos()
            wrappedSelf.interactor.getAutos(delegate: wrappedSelf)
        }
    }

}


extension AutoPresenter: AutoReceiveDelegate {
    
    func didReceiveAutos(autos: [NLAuto]?) {
        self.autos = autos
        view.reloadUI()
    }
    
}

extension AutoPresenter: DetailedAutoModuleOutputProtocol {
    
    func didSaveAuto() {
        ModelsManager.shared.removeAutos()
        interactor.getAutos(delegate: self)
    }
  
}

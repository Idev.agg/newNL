//
//  AutoAutoViewController.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol AutoViewInputProtocol: AlertPresenterProtocol, LoadingViewPresenterProtocol {
    
    func reloadUI()
    func setupInitialState()
}

protocol AutoViewOutputProtocol {

    func viewLoaded()
    func getSectionsCount() -> Int
    func getRowsCount(section: Int) -> Int
    func getAuto(for row: Int) -> NLAuto?
    func selectAuto(row: Int)
    func addAuto()

    func settingsPressed(row: Int)
    
}

class AutoViewController: BaseViewController, AutoViewInputProtocol {

    var output: AutoViewOutputProtocol!
    
    override var isMenuAvailable: Bool {
        return true
    }

    @IBOutlet weak var autoTable: UITableView!
    
    override var titleText: String {
        get {
            return "Автоматика"
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        autoTable.delaysContentTouches = false
        prepareViews()
        output.viewLoaded()
        let footer = UIView()
        footer.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 20)
        self.autoTable.tableFooterView = footer
    }

    func prepareViews() {

        autoTable.register(UINib(nibName: "AutoTableViewCell", bundle: nil), forCellReuseIdentifier: "autoCell")
        autoTable.tableFooterView = UIView()
    }
    
    func reloadUI() {
        autoTable.reloadData()
        self.showLoadingView(show: false)
    }

    func setupInitialState() {
    }
}

extension AutoViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return output.getSectionsCount()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.getRowsCount(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if output.getSectionsCount() == 2, indexPath.section == 0 {
            guard let cell = autoTable.dequeueReusableCell(withIdentifier: "autoCell") as? AutoTableViewCell, let auto = output.getAuto(for: indexPath.row) else {
                return UITableViewCell()
            }
            cell.configure(with: auto)
            cell.delegate = self
            return cell
        } else {
            var cell = autoTable.dequeueReusableCell(withIdentifier: "addAutoCell") as? AddAutoOrTimerTableViewCell
            if cell == nil {
                cell = AddAutoOrTimerTableViewCell(delegate: self, reuseIdentifier: "addAutoCell")
            }
            return cell ?? UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        output.selectAuto(row: indexPath.row)
    }
    
}

extension AutoViewController: CellWithSettingsDelegate {
    
    func settingsPressed(cell: UITableViewCell) {
        let row = autoTable.indexPath(for: cell)?.row
        guard let wrappedRow = row else { return }
        output.settingsPressed(row: wrappedRow)
    }
    
}

extension AutoViewController: AddAutoOrTimerTableViewCellDelegate {
    
    func addAutoOrTimerCellPressed() {
        output.addAuto()
        //TODO:
    }

}

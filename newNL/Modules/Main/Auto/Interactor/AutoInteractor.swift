//
//  AutoAutoInteractor.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit

protocol AutoInteractorProtocol: class {
    
    func getAutos(delegate: AutoReceiveDelegate)
    func saveAutos(data: Data, handler: @escaping ((Bool) -> ()))
    
}

class AutoInteractor: BaseInteractor, AutoInteractorProtocol {
    
    var modelsManager: ModelsManager
    
    init(modelsManager: ModelsManager = ModelsManager.shared) {
        self.modelsManager = modelsManager
    }
    
    func getAutos(delegate: AutoReceiveDelegate) {
        modelsManager.autosDelegate = delegate
        modelsManager.fetchAutos()
    }
    
    func saveAutos(data: Data, handler: @escaping ((Bool) -> ())) {
        RequestManager.shared.requestTurnControl(taskType: .uploadTask, file: .autoBin, data: data, isFromTurn: false) { (result) in
            if result.value is CommandResponse {
                if (result.value as! CommandResponse).success {
                    handler(true)
                }
            }
        }
    }
    
}

//
//  AutoAutoAssembly.swift
//  home control
//
//  Created by Sergey on 13/10/2018.
//  Copyright © 2018 nooTechnick. All rights reserved.
//

import UIKit


protocol AutoModuleInputProtocol: ModuleInputProtocol {

}

protocol AutoModuleOutputProtocol {

}


final class AutoModule: Module {

    private(set) var view: UIViewController
    private(set) var input: AutoModuleInputProtocol

    private init(view: AutoViewController, input: AutoModuleInputProtocol) {

	self.view = view
	self.input = input
    }

    static func assembly() ->  AutoModule {

        let router = AutoRouter()
        let presenter = AutoPresenter()
        let view = AutoViewController()
        let interactor = AutoInteractor()

        view.output = presenter
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        router.transitionHandler = view
	
        let module = AutoModule(view: view, input: presenter)
        return module

    }

}

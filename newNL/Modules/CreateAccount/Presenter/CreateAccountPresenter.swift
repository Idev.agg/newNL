//
//  CreateAccountCreateAccountPresenter.swift
//  New NL
//
//  Created by idev.agg.mf on 26/09/2019.
//  Copyright © 2019 nootechnicka. All rights reserved.
//

import UIKit

protocol CreateAccountPresenterProtocol: class {
    func setupView(_ view: CreateAccountViewProtocol)
}

class CreateAccountPresenter: BasePresenter, CreateAccountPresenterProtocol {

    private weak var view: CreateAccountViewProtocol?
    
    func viewLoaded() {
    }
    
    func setupView(_ view: CreateAccountViewProtocol) {
        self.view = view;
    }
    
}

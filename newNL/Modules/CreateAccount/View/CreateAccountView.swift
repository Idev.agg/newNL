//  CreateAccountCreateAccountView.swift
//  New NL
//
//  Created by idev.agg.mf on 26/09/2019.
//  Copyright © 2019 nootechnicka. All rights reserved.
//

import UIKit

protocol BaseViewProtocol: class {
}

protocol CreateAccountViewProtocol: BaseViewProtocol {}

class CreateAccountViewController: BaseViewController, CreateAccountViewProtocol {

    private let presenter: CreateAccountPresenter = CreateAccountPresenter()

    override func viewDidLoad() {
        super.viewDidLoad()
        presenter.setupView(self)
        presenter.viewLoaded()
    }

    func updateUi() {
    }

}

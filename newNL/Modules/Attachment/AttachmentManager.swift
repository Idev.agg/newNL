//
//  AttachmentManager.swift
//  newNL
//
//  Created by aggrroo on 4/23/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

enum AttachmentStep {
    case first, second, third, final
}

enum AttachmentDeviceType {
    case att_ftx, att_tx, att_rx, att_none
    
    static var cases: [AttachmentDeviceType] {
        return [.att_ftx, .att_tx, .att_rx]
    }
    
    var displayName: String {
        switch self {
        case .att_ftx:
            return "Силовой блок nooLite-F"
        case .att_tx:
            return "Силовой блок nooLite"
        case .att_rx:
            return "Датчик/пульт nooLite"
        case .att_none:
            return String.empty
        }
    }
    
    var stringType: String {
        switch self {
        case .att_ftx:
            return "03"
        case .att_tx:
            return "00"
        case .att_rx:
            return "01"
        case .att_none:
            return String.empty
        }
    }
    
    static var arrDisplayedNames: [String] {
        var array = [String]()
        for `case` in AttachmentDeviceType.cases {
            array.append(`case`.displayName)
        }
        return array
    }
    
}

fileprivate var attachCommand = "01%@%@000000000000000000000000"

class AttachmentManager {
    
    static let shared = AttachmentManager()
    
    var attType: AttachmentDeviceType
    var device: NLBaseDevice?
    var sourceArray: [SourceModel]?
    
    private weak var rxTimer: Timer?
    private var tickCounter = 0
    
    private var closure: ((Bool) -> ())?
    
    private init() {
        self.attType = .att_none
        debugInizializationMessage(someClass: self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
    func prepare() {
        attType = .att_none
        device = nil
        sourceArray = nil
    }
    
    func startAttaching(handler: ((Bool) -> ())?) {
        closure = handler
        switch attType {
        case .att_rx:
            startRxTimer()
        case .att_ftx, .att_tx:
            attach()
        default:
            return
        }
    }
    
    private func startRxTimer() {
        rxTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(rxTimerTick), userInfo: nil, repeats: true)
        rxTimer?.fire()
    }
    
    func stopRxTimer() {
        tickCounter = 0
        rxTimer?.invalidate()
        rxTimer = nil
    }
    
    func textForSecondStepHeader() -> String {
        switch attType {
        case .att_ftx:
            return "Добавление блока nooLite-F"
        case .att_tx:
            return "Добавление блока nooLite"
        case .att_rx:
            return "Добавление датчика/пульта nooLite"
        case .att_none:
            return String.empty
        }
    }
    
    @objc private func rxTimerTick() {
        if tickCounter == 0 {
            attach()
        } else if tickCounter == 26 {
            tickCounter = -1
        } else {
            RequestManager.shared.requestTurnControl(taskType: .dataTask, file: .bindHtm, isFromTurn: false) { [weak self] (result) in
                guard result.error == nil else {
                    return
                }
                guard let bindModel = result.value as? BindModel else {
                    ViewController.presentErrorAlertOnCurrentScreen(error: CommonError.baseError, handler: nil)
                    return
                }
                guard let channel = self?.getChannelForRx() else {
                    ViewController.presentErrorAlertOnCurrentScreen(error: CommonError.baseError, handler: nil)
                    return
                }
                guard bindModel.isRxAttachedToChannel(chan: channel - 1) else {
                    return
                }
                self?.stopRxTimer()
                self?.saveUserPropertiesForChannel(channel: channel - 1)
            }
        }
        tickCounter += 1
    }
    
    private func getChannelForRx() -> Int? {
        // +1 because in attach use -1 for prepare displayable channel
        guard let devices = ModelsManager.shared.getDevices() else {
            return 1 // 0 + 1 see above
        }
        var completedChannels = [Int]()
        for device in devices {
            if device.deviceType == .rx {
                completedChannels.append(device.chanel)
            }
        }
        for i in 0..<64 {
            if !completedChannels.contains(i) {
                return i + 1 // see above
            }
        }
        return nil
    }
    
    private func attach() {
        var channel = 0
        var chan = self.sourceArray?.first(where: { $0.title == "Номер канала" })?.value as? Int
        if attType == .att_rx {
            chan = getChannelForRx()
        }
        channel = chan != nil ? (chan! - 1) : 0
        let commandStr = String(format: attachCommand, attType.stringType, String(format:"%02X", channel))
        RequestManager.shared.sendCommand(strCommand: commandStr) { [weak self] (result) in
            if result.value is CommandResponse {
                if (result.value as! CommandResponse).success {
                    if let attType = self?.attType {
                        if attType == .att_tx, self?.closure != nil {
                            self?.closure?(true)
                        } else if attType == .att_ftx {
                            BaseViewController.showLoadingViewOnCurrentScreen(show: true, description: "Привязка")
                            Thread.performOnMainAfter(seconds: 1, {
                                self?.checkFtxAttach()
                            })
                        }
                    }
                }
            }
        }
    }
    
    func saveTxData(handler: ((Bool) -> ())?) {
        closure = handler
        let chan = self.sourceArray?.first(where: { $0.title == "Номер канала" })?.value as? Int
        let deviceType = self.sourceArray?.first(where: {$0.title == "Тип блока"})?.value as? DeviceSubType
        guard let channel = chan, let type = deviceType, let intType = type.integerType else { return }
        let hexChan = String(format: "%02X", channel - 1)
        let hexType = String(format: "%02X", intType)
        let strCommand = "010600\(hexChan)00000000\(hexType)000000000000"
        RequestManager.shared.sendCommand(strCommand: strCommand) { (result) in
            if result.value is CommandResponse {
                if (result.value as! CommandResponse).success {
                    self.saveUserPropertiesForChannel(channel: channel - 1)
                }
            }
        }
    }
    
    private func checkFtxAttach() {
        
        RequestManager.shared.requestTurnControl(taskType: .dataTask, file: .bindHtm, needShowProgress: true, isFromTurn: false, completionHandler: { [weak self] (result) in
            guard result.error == nil else {
                return
            }
            guard let bindModel = result.value as? BindModel else {
                return
            }
            guard bindModel.error == nil else {
                ViewController.presentErrorAlertOnCurrentScreen(error: bindModel.error!, handler: {
                    self?.closure?(false)
                })
                return
            }
            guard let ftxId = bindModel.ftxId else {
                ViewController.presentErrorAlertOnCurrentScreen(error: CommonError.baseError, handler: nil)
                return
            }
            RequestManager.shared.requestTurnControl(taskType: .dataTask, file: .deviceBin, needShowProgress: true, isFromTurn: false, completionHandler: { (deviceResult) in
                guard deviceResult.error == nil else {
                    ViewController.presentErrorAlertOnCurrentScreen(error: deviceResult.error!, handler: nil)
                    return
                }
                guard let deviceList = deviceResult.value as? NLBaseDevicesList else {
                    ViewController.presentErrorAlertOnCurrentScreen(error: CommonError.baseError, handler: nil)
                    return
                }
                guard let devices = deviceList.devices else {
                    ViewController.presentErrorAlertOnCurrentScreen(error: CommonError.baseError, handler: nil)
                    return
                }
                let foundedDevice = devices.first(where: {
                    if ($0 is NLFTXDevice) {
                        return ($0 as! NLFTXDevice).id == ftxId
                    }
                    return false
                })
                guard foundedDevice != nil else {
                    ViewController.presentErrorAlertOnCurrentScreen(error: CommonError.baseError, handler: nil)
                    return
                }
                self?.saveUserPropertiesForChannel(channel: foundedDevice!.chanel)
            })
        })
    }
    
    func cancelAttachForRX() {
        stopRxTimer()
        guard let channel = getChannelForRx() else { return }
        let commandStr = String(format: attachCommand, "02", String(format:"%02X", channel))
        RequestManager.shared.sendCommand(strCommand: commandStr) { (result) in
        }
    }
    
    private func saveUserPropertiesForChannel(channel: Int) {
        var chan = self.sourceArray?.first(where: { $0.title == "Номер канала" })?.value as? Int
        if attType == .att_rx {
            chan = getChannelForRx()
        }
        let channel = chan != nil ? (chan! - 1) : 0
        guard let sourceArray = sourceArray else {
            ViewController.presentErrorAlertOnCurrentScreen(error: CommonError.baseError, handler: nil)
            return
        }
        let data = StoreFileManager.shared.getUserBinData()?.byAddingUserProperties(type: attType, channel: channel, sourceArray: sourceArray)
        RequestManager.shared.requestTurnControl(taskType: .uploadTask, file: .userBin, data: data, isFromTurn: false) { [weak self] (result) in
            if result.value is CommandResponse {
                if (result.value as! CommandResponse).success {
                    if (self?.closure != nil) {
                        self?.closure?(true)
                    }
                }
            }
        }
    }
    
}

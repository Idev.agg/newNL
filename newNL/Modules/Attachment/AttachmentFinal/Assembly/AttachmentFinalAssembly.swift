//
//  AttachmentFinalAttachmentFinalAssembly.swift
//  home control
//
//  Created by Sergey on 26/05/2019.
//  Copyright © 2019 nooTechnick. All rights reserved.
//

import UIKit


protocol AttachmentFinalModuleInputProtocol: ModuleInputProtocol {

}

protocol AttachmentFinalModuleOutputProtocol {

}


final class AttachmentFinalModule: Module {

    private(set) var view: UIViewController
    private(set) var input: AttachmentFinalModuleInputProtocol

    private init(view: AttachmentFinalViewController, input: AttachmentFinalModuleInputProtocol) {

	self.view = view
	self.input = input
    }

    static func assembly() ->  AttachmentFinalModule{

        let viewModel = AttachmentFinalViewModel()
        let view = AttachmentFinalViewController()

        view.output = viewModel
	
        let module = AttachmentFinalModule(view: view, input: viewModel)
        return module

    }

}

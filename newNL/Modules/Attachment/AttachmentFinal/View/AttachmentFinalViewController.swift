//
//  AttachmentFinalAttachmentFinalViewController.swift
//  home control
//
//  Created by Sergey on 26/05/2019.
//  Copyright © 2019 nooTechnick. All rights reserved.
//

import UIKit


protocol AttachmentFinalViewInputProtocol: class {

    /**
        @author Sergey
        Setup initial state of the view
    */

    func setupInitialState()
}

protocol AttachmentFinalViewOutputProtocol {

    /**
        @author Sergey
        Notify presenter that view is ready
    */

}

class AttachmentFinalViewController: BaseViewController, AttachmentFinalViewInputProtocol {

    var output: AttachmentFinalViewOutputProtocol!
    
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = white
        scrollView.addSubview(stackView)
        stackView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20))
        stackView.autoAlignAxis(toSuperviewAxis: .vertical)
        stackView.addArrangedSubview(headerView)
        return scrollView
    }()
    
    private var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        stack.spacing = 20
        return stack
    }()
    
    lazy var headerView: UIView = {
        let header = UIView()
        
        let text = AttachmentManager.shared.textForSecondStepHeader()
        let topLabel = UILabel.initWith(text: text, font: UIFont.boldSystemFont(ofSize: 15), textColor: black, numberOfLines: 0)
        
        header.addSubview(topLabel)
        topLabel.autoPinEdgesToSuperviewEdges(with: .init(top: 20, left: 0, bottom: 0, right: 0), excludingEdge: .bottom)
        
        let attProgressView = AttachmentProgressView()
        header.addSubview(attProgressView)
        attProgressView.autoPinEdge(toSuperviewEdge: .left)
        attProgressView.autoPinEdge(toSuperviewEdge: .right)
        attProgressView.setupWithStep(.final)
        attProgressView.autoPinEdge(.top, to: .bottom, of: topLabel, withOffset: 10)
        
        let botLabel = UILabel.initWith(text: "Привязка завершена", font: UIFont.systemFont(ofSize: 15), textColor: black, numberOfLines: 0, align: .center)
        header.addSubview(botLabel)
        botLabel.autoPinEdgesToSuperviewEdges(with: .init(top: 0, left: 20, bottom: 10, right: 20), excludingEdge: .top)
        botLabel.autoPinEdge(.top, to: .bottom, of: attProgressView, withOffset: 50)
        return header
    }()

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        showSuccessStatus()
    }
    
    override func loadView() {
        super.loadView()
        view.addSubview(scrollView)
        scrollView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        scrollView.autoPin(toTopLayoutGuideOf: self, withInset: 50)
    }
    
    override func configureNavigation() {
        super.configureNavigation()
        navigationView.configure(titleText: "Добавление", left: nil, right: nil)
    }

    private func showSuccessStatus() {
        ModelsManager.shared.removeDevices()
        let deviceName = (AttachmentManager.shared.sourceArray?.first(where: {
            $0.title == "Название"
        })?.value as? String) ?? String.empty
        let roomName = (AttachmentManager.shared.sourceArray?.first(where: {
            $0.title == "Расположение"
        })?.value as? NLRoom)?.name ?? String.empty
        let message = String(format: "Устройство \"%@\" успешно привязано в комнату \"%@\"", deviceName, roomName)
        presentSimpleAlert(with: "Успешно", text: message, acceptTitle: "Ок", acceptHandler: { [weak self] in
            self?.navigationController?.popToRootViewController(animated: true)
        })
    }
    
    // MARK: AttachmentFinalViewInputProtocol
    func setupInitialState() {
    }
}

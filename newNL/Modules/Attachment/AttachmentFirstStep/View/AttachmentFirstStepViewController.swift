//
//  AttachmentFirstStepAttachmentFirstStepViewController.swift
//  home control
//
//  Created by Sergey on 23/04/2019.
//  Copyright © 2019 nooTechnick. All rights reserved.
//

import UIKit
import PureLayout

protocol AttachmentFirstStepViewInputProtocol: class {
    func setupInitialState()
}

protocol AttachmentFirstStepViewOutputProtocol {

}

class AttachmentFirstStepViewController: BaseViewController, AttachmentFirstStepViewInputProtocol {

    lazy var tableView: UITableView = {
        let table = UITableView.tableViewWith(delegate: self)
        table.setAndLayoutTableHeaderView(header: headerView)
        return table
    }()
    
    lazy var headerView: UIView = {
        let header = UIView()
        
        let topLabel = UILabel.initWith(text: "Добавление устройства", font: UIFont.boldSystemFont(ofSize: 15), textColor: black, numberOfLines: 0)
        header.addSubview(topLabel)
        topLabel.autoPinEdgesToSuperviewEdges(with: .init(top: 20, left: 20, bottom: 0, right: 20), excludingEdge: .bottom)
        
        let attProgressView = AttachmentProgressView()
        attProgressView.setupWithStep(.first)
        header.addSubview(attProgressView)
        attProgressView.autoPinEdge(.top, to: .bottom, of: topLabel, withOffset: 10)
        attProgressView.autoPinEdge(toSuperviewEdge: .left, withInset: 20)
        attProgressView.autoPinEdge(toSuperviewEdge: .right, withInset: 20)
        attProgressView.setupWithStep(.first)
        
        let botLabel = UILabel.initWith(text: "Выберите тип:", font: UIFont.boldSystemFont(ofSize: 15), textColor: black, numberOfLines: 0)
        header.addSubview(botLabel)
        botLabel.autoPinEdgesToSuperviewEdges(with: .init(top: 0, left: 20, bottom: 10, right: 20), excludingEdge: .top)
        botLabel.autoPinEdge(.top, to: .bottom, of: attProgressView, withOffset: 10)
        
        header.addBotLineWithColor(gray)
        
        return header
    }()
    
    var output: AttachmentFirstStepViewOutputProtocol!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func loadView() {
        super.loadView()
        view.addSubview(tableView)
        tableView.autoPinEdgesToSuperviewEdges(with: .zero, excludingEdge: .top)
        tableView.autoPin(toTopLayoutGuideOf: self, withInset: 50)
    }
    
    override func configureNavigation() {
        super.configureNavigation()
        let cancel = Button.cancel
        cancel.configure { [weak self] in
            self?.navigationController?.popToRootViewController(animated: true)
        }
        let next = Button.nextButton
        next.configure { [weak self] in
            guard AttachmentManager.shared.attType != .att_none else {
                self?.presentSimpleAlert(with: nil, text: "Необходимо выбрать тип", acceptTitle: "Ок", acceptHandler: nil)
                return
            }
            let module = AttachmentSecondStepModule.assembly()
            self?.push(module: module)
        }
        navigationView.configure(titleText: "Добавление", left: cancel, right: next)
    }
    // MARK: AttachmentFirstStepViewInputProtocol
    func setupInitialState() {
    }
}

extension AttachmentFirstStepViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AttachmentDeviceType.cases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell().cast(object: AttachmentDeviceType.arrDisplayedNames[indexPath.row], tableView: tableView, indexPath: indexPath, selected: (indexPath.row == AttachmentDeviceType.cases.index(of: AttachmentManager.shared.attType)))
        cell.addBotLineWithColor(gray)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        AttachmentManager.shared.attType = AttachmentDeviceType.cases[indexPath.row]
        tableView.reloadData()
    }
    
}

//
//  AttachmentFirstStepAttachmentFirstStepAssembly.swift
//  home control
//
//  Created by Sergey on 23/04/2019.
//  Copyright © 2019 nooTechnick. All rights reserved.
//

import UIKit


protocol AttachmentFirstStepModuleInputProtocol: ModuleInputProtocol {

}

protocol AttachmentFirstStepModuleOutputProtocol {

}


final class AttachmentFirstStepModule: Module {

    private(set) var view: UIViewController
    private(set) var input: AttachmentFirstStepModuleInputProtocol

    private init(view: AttachmentFirstStepViewController, input: AttachmentFirstStepModuleInputProtocol) {

	self.view = view
	self.input = input
    }

    static func assembly() ->  AttachmentFirstStepModule{

        let viewModel = AttachmentFirstStepViewModel()
        let view = AttachmentFirstStepViewController()

        view.output = viewModel
	
        let module = AttachmentFirstStepModule(view: view, input: viewModel)
        return module

    }

}

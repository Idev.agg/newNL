//
//  AttachmentSecondStepAttachmentSecondStepViewModel.swift
//  home control
//
//  Created by Sergey on 23/04/2019.
//  Copyright © 2019 nooTechnick. All rights reserved.
//

class AttachmentSecondStepViewModel: BaseViewModel, AttachmentSecondStepModuleInputProtocol, AttachmentSecondStepViewOutputProtocol {
    
    var sourceArray = [SourceModel]()
    
    override init() {
        super.init()
        configureSourceArray()
    }
    
    func configureSourceArray() {
        let nameModel = SourceModel(title: "Название", value: nil, dataType: .string)
        nameModel.maxLength = 30
        let placeModel = SourceModel(title: "Расположение", value: nil, dataType: .choosable)
        if let rooms = ModelsManager.shared.getHouse()?.getRoomsWithoutUndistributed() {
            placeModel.selectingList = rooms
        }
        sourceArray.append(nameModel)
        sourceArray.append(placeModel)
        if AttachmentManager.shared.attType == .att_tx {
            let typeBlockModel = SourceModel(title: "Тип блока", value: nil, dataType: .choosable)
            typeBlockModel.selectingList = DeviceSubType.arrayTxTypes
            let channelModel = SourceModel(title: "Номер канала", value: nil, dataType: .choosable)
            channelModel.selectingList = Int.array(first: 1, count: 64)
            sourceArray.append(typeBlockModel)
            sourceArray.append(channelModel)
        }
        AttachmentManager.shared.sourceArray = sourceArray
    }
    
    func itemsCount() -> Int {
        return sourceArray.count
    }
    
    func itemForRow(row: Int) -> SourceModel {
        return sourceArray[row]
    }
    
}

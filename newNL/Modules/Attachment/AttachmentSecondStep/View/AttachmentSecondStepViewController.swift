//
//  AttachmentSecondStepAttachmentSecondStepViewController.swift
//  home control
//
//  Created by Sergey on 23/04/2019.
//  Copyright © 2019 nooTechnick. All rights reserved.
//

import UIKit


protocol AttachmentSecondStepViewInputProtocol: class {

    func setupInitialState()
}

protocol AttachmentSecondStepViewOutputProtocol {

    var sourceArray: [SourceModel] { get set }
    func itemsCount() -> Int
    func itemForRow(row: Int) -> SourceModel

}

class AttachmentSecondStepViewController: BaseViewController, AttachmentSecondStepViewInputProtocol {
    
    lazy var tableView: UITableView = {
        let table = UITableView.tableViewWith(delegate: self)
        table.setAndLayoutTableHeaderView(header: headerView)
        return table
    }()
    
    lazy var headerView: UIView = {
        let header = UIView()
        
        let text = AttachmentManager.shared.textForSecondStepHeader()
        let topLabel = UILabel.initWith(text: text, font: UIFont.boldSystemFont(ofSize: 15), textColor: black, numberOfLines: 0)
        
        header.addSubview(topLabel)
        topLabel.autoPinEdgesToSuperviewEdges(with: .init(top: 20, left: 20, bottom: 0, right: 20), excludingEdge: .bottom)
        
        let attProgressView = AttachmentProgressView()
        attProgressView.setupWithStep(.first)
        header.addSubview(attProgressView)
        attProgressView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0, left: 20, bottom: 40, right: 20), excludingEdge: .top)
        attProgressView.setupWithStep(.second)
        attProgressView.autoPinEdge(.top, to: .bottom, of: topLabel, withOffset: 10)
        
        return header
    }()

    var output: AttachmentSecondStepViewOutputProtocol!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        AttachmentManager.shared.cancelAttachForRX()
    }

    override func loadView() {
        super.loadView()
        view.addSubview(tableView)
        tableView.autoPinEdge(toSuperviewEdge: .left)
        tableView.autoPinEdge(toSuperviewEdge: .right)
        tableView.autoPin(toTopLayoutGuideOf: self, withInset: 50)
        self.bottomConstraint = tableView.autoPinEdge(toSuperviewEdge: .bottom)
    }
    
    override func configureNavigation() {
        super.configureNavigation()
        let back = Button.back
        back.configure { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
        let next = Button.nextButton
        next.configure { [weak self] in
            self?.hideKeyboard()
            guard Validator.validateArray(self?.output.sourceArray) else {
                self?.tableView.reloadData()
                return
            }
            let module = AttachmentThirdStepModule.assembly()
            self?.push(module: module)
        }
        navigationView.configure(titleText: "Добавление", left: back, right: next)
    }

    // MARK: AttachmentSecondStepViewInputProtocol
    func setupInitialState() {
    }
}

extension AttachmentSecondStepViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return output.itemsCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell().cast(object: output.itemForRow(row: indexPath.row), tableView: tableView, indexPath: indexPath, vc: self)
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        hideKeyboard()
        let entity = output.itemForRow(row: indexPath.row)
        if entity.dataType == .choosable, let selectingList = entity.selectingList {
            let arr = selectingList.map{ MenuCellModel(value: $0) }
            arr.last?.isLast = true
            let module = ChooseItemModule.assembly(with: arr) { (selected) in
                guard let selected = selected as? MenuCellModel, let index = arr.firstIndex(of: selected) else { return }
                entity.value = selectingList[index]
                entity.error = nil
                tableView.reloadData()
            }
            self.presentModallyOnSelf(module: module)
        }
    }
    
}

extension AttachmentSecondStepViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        guard let index = textField.indexPath?.row else { return }
        let entity = output.itemForRow(row: index)
        entity.error = nil
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let index = textField.indexPath?.row else { return }
        let entity = output.itemForRow(row: index)
        entity.value = textField.text
        entity.error = nil
        tableView.reloadData()
    }
    
}

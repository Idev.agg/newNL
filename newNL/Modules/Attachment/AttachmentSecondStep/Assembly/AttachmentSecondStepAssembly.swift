//
//  AttachmentSecondStepAttachmentSecondStepAssembly.swift
//  home control
//
//  Created by Sergey on 23/04/2019.
//  Copyright © 2019 nooTechnick. All rights reserved.
//

import UIKit


protocol AttachmentSecondStepModuleInputProtocol: ModuleInputProtocol {

}

protocol AttachmentSecondStepModuleOutputProtocol {

}


final class AttachmentSecondStepModule: Module {

    private(set) var view: UIViewController
    private(set) var input: AttachmentSecondStepModuleInputProtocol

    private init(view: AttachmentSecondStepViewController, input: AttachmentSecondStepModuleInputProtocol) {

	self.view = view
	self.input = input
    }

    static func assembly() ->  AttachmentSecondStepModule{

        let viewModel = AttachmentSecondStepViewModel()
        let view = AttachmentSecondStepViewController()

        view.output = viewModel
	
        let module = AttachmentSecondStepModule(view: view, input: viewModel)
        return module

    }

}

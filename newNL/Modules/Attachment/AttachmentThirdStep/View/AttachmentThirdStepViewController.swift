//
//  AttachmentThirdStepAttachmentThirdStepViewController.swift
//  home control
//
//  Created by Sergey on 23/04/2019.
//  Copyright © 2019 nooTechnick. All rights reserved.
//

import UIKit


protocol AttachmentThirdStepViewInputProtocol: class {

    func setupInitialState()
}

protocol AttachmentThirdStepViewOutputProtocol {
    
    

}

class AttachmentThirdStepViewController: BaseViewController, AttachmentThirdStepViewInputProtocol {

    var output: AttachmentThirdStepViewOutputProtocol!

    // MARK: Life cycle
    private lazy var scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.backgroundColor = white
        scrollView.addSubview(stackView)
        stackView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20))
        stackView.autoAlignAxis(toSuperviewAxis: .vertical)
        stackView.addArrangedSubview(headerView)
        configureContentView()
        return scrollView
    }()
    
    private var attachButton: UIButton = {
        let button = UIButton.buttonWith(title: "Привязка", target: self, selector: #selector(attachTapped))
        button.autoSetDimension(.height, toSize: 40)
        return button
    }()
    
    private var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        stack.spacing = 20
        return stack
    }()
    
    lazy var headerView: UIView = {
        let header = UIView()
        
        let text = AttachmentManager.shared.textForSecondStepHeader()
        let topLabel = UILabel.initWith(text: text, font: UIFont.boldSystemFont(ofSize: 15), textColor: black, numberOfLines: 0)
        
        header.addSubview(topLabel)
        topLabel.autoPinEdgesToSuperviewEdges(with: .init(top: 20, left: 0, bottom: 0, right: 0), excludingEdge: .bottom)
        
        let attProgressView = AttachmentProgressView()
        header.addSubview(attProgressView)
        attProgressView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), excludingEdge: .top)
        attProgressView.setupWithStep(.third)
        attProgressView.autoPinEdge(.top, to: .bottom, of: topLabel, withOffset: 10)
        
        return header
    }()
    
    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func loadView() {
        super.loadView()
        view.addSubview(scrollView)
        scrollView.autoPinEdge(toSuperviewEdge: .left)
        scrollView.autoPinEdge(toSuperviewEdge: .right)
        scrollView.autoPin(toTopLayoutGuideOf: self, withInset: 50)
        self.bottomConstraint = scrollView.autoPinEdge(toSuperviewEdge: .bottom)
    }
    
    override func configureNavigation() {
        super.configureNavigation()
        let back = Button.back
        back.configure { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
        navigationView.configure(titleText: "Добавление", left: back, right: nil)
    }
    
    private func configureContentView() {
        switch AttachmentManager.shared.attType {
        case .att_ftx:
            let infoLabel = UILabel.initWith(text: "Нажмите сервисную кнопку на блоке, затем нажмите кнопку \"Привязка\"", font: UIFont.systemFont(ofSize: 16), textColor: black)
            stackView.addArrangedSubview(infoLabel)
            stackView.addArrangedSubview(attachButton)
        case .att_tx:
            let firstStepLabel = UILabel.initWith(text: "a. Нажмите сервисную кнопку на блоке, затем нажмите кнопку \"Привязка\"", font: UIFont.systemFont(ofSize: 16), textColor: black)
            stackView.addArrangedSubview(firstStepLabel)
            stackView.addArrangedSubview(attachButton)
            
            let secondStepLabel = UILabel.initWith(text: "b. Нажмите сервисную кнопку на блоке еще 2 раза", font: UIFont.systemFont(ofSize: 16), textColor: black)
            stackView.addArrangedSubview(secondStepLabel)
            
            let thirdStepLabel = UILabel.initWith(text: "c. Проверьте работу блока", font: UIFont.systemFont(ofSize: 16), textColor: black)
            stackView.addArrangedSubview(thirdStepLabel)
            
            let onNooLiteButton = UIButton.buttonWith(title: "Включить", target: self, selector: #selector(onNooLitePressed))
            onNooLiteButton.autoSetDimension(.height, toSize: 40)
            let offNooLiteButton = UIButton.buttonWith(title: "Выключить", target: self, selector: #selector(offNooLitePressed))
            offNooLiteButton.autoSetDimension(.height, toSize: 40)
            let controlButtonsStackView = UIStackView(arrangedSubviews: [onNooLiteButton, offNooLiteButton])
            offNooLiteButton.autoMatch(.width, to: .width, of: onNooLiteButton)
            controlButtonsStackView.axis = .horizontal
            controlButtonsStackView.distribution = .fill
            controlButtonsStackView.spacing = 20
            stackView.addArrangedSubview(controlButtonsStackView)
            
            let fourthStepLabel = UILabel.initWith(text: "d. Если блок включается, завершите привязку. Иначе, повторите пункты a-c", font: UIFont.systemFont(ofSize: 16), textColor: black)
            stackView.addArrangedSubview(fourthStepLabel)
            
            let finishAttachButton = UIButton.buttonWith(title: "Завершить", target: self, selector: #selector(saveNooLite))
            stackView.addArrangedSubview(finishAttachButton)
            finishAttachButton.autoSetDimension(.height, toSize: 40)
        case .att_rx:
            let viewWithSpinner = UIView()
            viewWithSpinner.autoSetDimension(.height, toSize: 70)
            let spinner = Spinner()
            spinner.autoSetDimensions(to: CGSize(width: 60, height: 60))
            spinner.frame.size = CGSize(width: 60, height: 60)
            viewWithSpinner.addSubview(spinner)
            spinner.autoAlignAxis(toSuperviewAxis: .vertical)
            spinner.autoAlignAxis(toSuperviewAxis: .horizontal)
            spinner.setup()
            spinner.startAnimating()
            stackView.addArrangedSubview(viewWithSpinner)
            stackView.addArrangedSubview(UILabel.initWith(text: "Для датчика:", font: UIFont.systemFont(ofSize: 16, weight: .bold), textColor: black))
            stackView.addArrangedSubview(UILabel.initWith(text: "Нажмите сервисную кнопку на датчике", font: UIFont.systemFont(ofSize: 16, weight: .regular), textColor: black))
            stackView.addArrangedSubview(UILabel.initWith(text: "Для пульта:", font: UIFont.systemFont(ofSize: 16, weight: .bold), textColor: black))
            stackView.addArrangedSubview(UILabel.initWith(text: "Нажмите сервисную кнопку на пульте, затем тот сенсор или кнопку, которую хотите привязать", font: UIFont.systemFont(ofSize: 16, weight: .regular), textColor: black))
            attachTapped()
        case .att_none:
            return
        }
        stackView.addSpaceBelow()
    }
    
    private func prepareCommandStr(command: PowerBlockCommand) -> String {
        let channel = (AttachmentManager.shared.sourceArray?.first(where: {
            $0.title == "Номер канала"
        })?.value as? Int)
        guard let wrappedChannel = channel else { return String.empty }
        let hexChannel = String(format:"%02X", wrappedChannel - 1)
        return String(format: "00000000%@%@000000000000000000", hexChannel, command.hexCmd)
    }
    
    @objc private func onNooLitePressed() {
        let strCommand = prepareCommandStr(command: .on)
        RequestManager.shared.requestTurnControl(taskType: .sendCommand, file: .command, strCommand: strCommand, isFromTurn: false) { (result) in
        }
    }
    
    @objc private func offNooLitePressed() {
        let strCommand = prepareCommandStr(command: .off)
        RequestManager.shared.requestTurnControl(taskType: .sendCommand, file: .command, strCommand: strCommand, isFromTurn: false) { (result) in
        }
    }
    
    @objc private func saveNooLite() {
        AttachmentManager.shared.saveTxData { [weak self] (saved) in
            self?.goToFinal()
        }
    }

    @objc private func attachTapped() {
        AttachmentManager.shared.startAttaching { [weak self] (attached) in
            if AttachmentManager.shared.attType == .att_ftx, !attached {
                self?.navigationController?.popToRootViewController(animated: true)
            } else if attached, AttachmentManager.shared.attType != .att_tx {
                self?.goToFinal()
            }
        }
    }
    
    private func goToFinal() {
        let module = AttachmentFinalModule.assembly()
        self.push(module: module)
    }

    // MARK: AttachmentThirdStepViewInputProtocol
    func setupInitialState() {
    }
}

//
//  AttachmentThirdStepAttachmentThirdStepAssembly.swift
//  home control
//
//  Created by Sergey on 23/04/2019.
//  Copyright © 2019 nooTechnick. All rights reserved.
//

import UIKit


protocol AttachmentThirdStepModuleInputProtocol: ModuleInputProtocol {

}

protocol AttachmentThirdStepModuleOutputProtocol {

}


final class AttachmentThirdStepModule: Module {

    private(set) var view: UIViewController
    private(set) var input: AttachmentThirdStepModuleInputProtocol

    private init(view: AttachmentThirdStepViewController, input: AttachmentThirdStepModuleInputProtocol) {

	self.view = view
	self.input = input
    }

    static func assembly() ->  AttachmentThirdStepModule{

        let viewModel = AttachmentThirdStepViewModel()
        let view = AttachmentThirdStepViewController()

        view.output = viewModel
	
        let module = AttachmentThirdStepModule(view: view, input: viewModel)
        return module

    }

}

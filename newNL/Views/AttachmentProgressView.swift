//
//  AttachmentProgressView.swift
//  newNL
//
//  Created by aggrroo on 4/23/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

let borderColorCurrent = black
let borderColorDeactive = gray
let borderColorComplete = clear

enum ViewLookType {
    case inactive, current, complete
}

class AttachmentProgressView: UIView {
    
    private var firstStepView = GradientLabel()
    private var secondStepView = GradientLabel()
    private var thirdStepView = GradientLabel()
    private var line1_2 = UIView()
    private var line2_3 = UIView()
    
    private var attachmentStep: AttachmentStep = .first
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init() {
        self.init(frame: .zero)
        firstStepView = GradientLabel.initWith(text: "1", font: UIFont.boldSystemFont(ofSize: 24), textColor: gray,  align: .center)
        addSubview(firstStepView)
        secondStepView = GradientLabel.initWith(text: "2", font: UIFont.boldSystemFont(ofSize: 24), textColor: gray,  align: .center)
        addSubview(secondStepView)
        thirdStepView = GradientLabel.initWith(text: "3", font: UIFont.boldSystemFont(ofSize: 24), textColor: gray,  align: .center)
        addSubview(thirdStepView)
        
        addSubview(line1_2)
        addSubview(line2_3)
        
        // constraints
        // sizes
        firstStepView.autoSetDimensions(to: CGSize(width: 40, height: 40))
        secondStepView.autoSetDimensions(to: CGSize(width: 40, height: 40))
        thirdStepView.autoSetDimensions(to: CGSize(width: 40, height: 40))
        line1_2.autoSetDimensions(to: CGSize(width: 40, height: 1))
        line2_3.autoSetDimensions(to: CGSize(width: 40, height: 1))
        
        // horizontal axis of all views
        line1_2.autoAlignAxis(.horizontal, toSameAxisOf: firstStepView)
        secondStepView.autoAlignAxis(.horizontal, toSameAxisOf: firstStepView)
        line2_3.autoAlignAxis(.horizontal, toSameAxisOf: firstStepView)
        thirdStepView.autoAlignAxis(.horizontal, toSameAxisOf: firstStepView)
        
        // main constraints

        firstStepView.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 10, left: 0, bottom: 40, right: 0), excludingEdge: .right)
        line1_2.autoPinEdge(.left, to: .right, of: firstStepView)
        secondStepView.autoPinEdge(.left, to: .right, of: line1_2)
        line2_3.autoPinEdge(.left, to: .right, of: secondStepView)
        thirdStepView.autoPinEdge(.left, to: .right, of: line2_3)
        
        firstStepView.becomeStrocked(with: borderColorDeactive)
        secondStepView.becomeStrocked(with: borderColorDeactive)
        thirdStepView.becomeStrocked(with: borderColorDeactive)
        line1_2.backgroundColor = borderColorDeactive
        line2_3.backgroundColor = borderColorDeactive
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    private func configureView(_ label: GradientLabel, type: ViewLookType) {
        switch type {
        case .inactive:
            label.textColor = borderColorDeactive
            label.becomeStrocked(with: borderColorDeactive)
        case .current:
            label.textColor = borderColorCurrent
            label.becomeStrocked(with: borderColorCurrent)
        case .complete:
            label.textColor = white
            label.becomeStrocked(with: borderColorComplete)
            label.setup(with: [lightGreen.cgColor, darkGreen.cgColor])
            label.makeGradientDiagonaled()
        }
    }
    
    func setupWithStep(_ step: AttachmentStep) {
        attachmentStep = step
        configureUi()
    }
    
    private func configureUi() {
        switch attachmentStep {
        case .first:
            configureView(firstStepView, type: .current)
        case .second:
            configureView(firstStepView, type: .complete)
            line1_2.backgroundColor = borderColorCurrent
            configureView(secondStepView, type: .current)
        case .third:
            configureView(firstStepView, type: .complete)
            line1_2.backgroundColor = borderColorCurrent
            configureView(secondStepView, type: .complete)
            line2_3.backgroundColor = borderColorCurrent
            configureView(thirdStepView, type: .current)
        case .final:
            configureView(firstStepView, type: .complete)
            line1_2.backgroundColor = borderColorCurrent
            configureView(secondStepView, type: .complete)
            line2_3.backgroundColor = borderColorCurrent
            configureView(thirdStepView, type: .complete)
        }
    }
    
    
}

//
//  GradientView.swift
//  JUNO
//
//  Created by Sergey on 12/13/18.
//  Copyright © 2018 elatesoftware. All rights reserved.
//

import UIKit

class GradientView: UIView {
  
  override class var layerClass: AnyClass {
    return CAGradientLayer.self
  }
  
  func setup(with colors: [CGColor]) {
    guard let layer = layer as? CAGradientLayer else { return }
    layer.colors = colors
    layer.startPoint = CGPoint(x: 0, y: 1)
    layer.endPoint = CGPoint(x: 1, y: 1)
  }
  
}

extension UIView {
  
  func becomeRounded() {
    layer.cornerRadius = bounds.height / 2
  }
  
  func makeTransparent(a: CGFloat) {
    alpha = a
  }
  
  func makeGradientDiagonaled() {
    guard layer is CAGradientLayer else { return }
    (layer as! CAGradientLayer).becomeDiagonaled()
  }
  
}

class GradientLabel: UILabel {
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    func setup(with colors: [CGColor]) {
        guard let layer = layer as? CAGradientLayer else { return }
        layer.colors = colors
        layer.startPoint = CGPoint(x: 0, y: 1)
        layer.endPoint = CGPoint(x: 1, y: 1)
    }
    
}

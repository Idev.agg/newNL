//
//  AutoTableViewCell.swift
//  newNL
//
//  Created by aggrroo on 20.10.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

protocol CellWithSettingsDelegate: class {
    
    func settingsPressed(cell: UITableViewCell)
    
}

class AutoTableViewCell: UITableViewCell {
    
    
    @IBOutlet private var name: UILabel!
    @IBOutlet private var commandCompleter: UILabel!
    @IBOutlet private var actionTypeImage: UIImageView!
    @IBOutlet private var action: UILabel!
    @IBOutlet private var stack: UIStackView!
    
    weak var delegate: CellWithSettingsDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layoutIfNeeded()
        selectionStyle = .none
    }
    
    func configure(with auto: NLAuto) {
        name.text = auto.name
        actionTypeImage.image = getImage(type: auto.autoType)
        commandCompleter.text = auto.command.getShortName()
        action.text = getActionText(type: auto.autoType, parameter: auto.parameterOrIncomeCommand)
        name.textColor = .black
        commandCompleter.textColor = .black
        action.textColor = .black
        stack.alpha = auto.isActive ? SIMPLE_APLHA : ALPHA_20
    }
    
    @IBAction func settingsPressed(_ sender: UIButton) {
        delegate?.settingsPressed(cell: self)
    }
    
}

fileprivate extension AutoTableViewCell {
    
    func getImage(type: AutoType) -> UIImage {
        let image: UIImage? = {
            switch type {
            case .cooling, .heating: return UIImage.init(named: "temperature")
            case .humidification, .draining: return UIImage.init(named: "humidity")
            case .event: return UIImage.init(named: "attention")
            default: return UIImage.init(named: "question")
            }
        }()
        return image ?? UIImage()
    }
    
    func getActionText(type: AutoType, parameter: Int) -> String {
        let text: String = {
            switch type {
            case .cooling: return "Охлаждение до \(parameter)°C"
            case .heating: return "Нагрев до \(parameter)°C"
            case .humidification: return "Увлажнение до \(parameter)%"
            case .draining: return "Осушение до \(parameter)%"
            case .event: return "Событие"
            default: return "Неизвестный тип"
            }
        }()
        return text
    }
    
}

protocol AddAutoOrTimerTableViewCellDelegate: class {
    func addAutoOrTimerCellPressed()
}

class AddAutoOrTimerTableViewCell: BaseTableViewCell {

    private weak var delegate: AddAutoOrTimerTableViewCellDelegate?
    
    private var button = UIButton()
    
    init(delegate: AddAutoOrTimerTableViewCellDelegate, reuseIdentifier: String?) {
        super.init(style: .default, reuseIdentifier: reuseIdentifier)
        self.delegate = delegate
        button.setTitle("Добавить", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.becomeRounded()
        button.becomeStrocked(with: UIColor.black)
        button.addTarget(self, action: #selector(addPressed), for: .touchUpInside)
        contentView.addSubview(button)
        button.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview().offset(20)
            maker.bottom.equalToSuperview()
            maker.left.equalToSuperview().offset(20)
            maker.right.equalToSuperview().offset(-20)
            maker.height.equalTo(48)
        }
        
    }
    
    @objc private func addPressed() {
        guard let delegate = delegate else {
            return
        }
        delegate.addAutoOrTimerCellPressed()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

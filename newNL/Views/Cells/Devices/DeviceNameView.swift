//
//  DeviceNameView.swift
//  newNL
//
//  Created by aggrroo on 2/24/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class DeviceNameView: NibView {
    
    @IBOutlet weak var roomNameLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!

    func configure(name: String, roomName: String) {
        roomNameLabel.text = roomName
        nameLabel.text = name
    }
    
}

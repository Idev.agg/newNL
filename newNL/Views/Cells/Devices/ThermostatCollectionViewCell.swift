//
//  ThermostatCollectionViewCell.swift
//  newNL
//
//  Created by aggrroo on 2/24/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class ThermostatCollectionViewCell: BaseCollectionViewCell {

    @IBOutlet var deviceNameView: DeviceNameView!
    
    static let REUSE_ID = "thermostatCell"
    
    override var reuseIdentifier: String? {
        return "thermostatCell"
    }
    
    func configureWith(thermostat: Thermostat) {
        deviceNameView.configure(name: thermostat.name ?? String.empty, roomName: thermostat.getRoomName())
    }

}

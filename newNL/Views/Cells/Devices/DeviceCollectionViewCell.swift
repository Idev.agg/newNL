//
//  DeviceCollectionViewCell.swift
//  newNL
//
//  Created by aggrroo on 2/24/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class BaseCollectionViewCell: UICollectionViewCell {
    
    override var reuseIdentifier: String? {
        return "baseCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = CORNER_RADIUS
        layer.borderWidth = BORDER_WIDTH
        layer.borderColor = black.cgColor
    }
   
}

class AddDeviceCollectionViewCell: BaseCollectionViewCell {
    
    static var REUSE_ID = "addDeviceCell"
        
    override var reuseIdentifier: String? {
        return "addDeviceCell"
    }
    
    private var image = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(image)
        image.contentMode = .center
        image.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        image.image = UIImage.init(named: "plus")
        image.becomeRounded()
        image.becomeStrocked(with: UIColor.black)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class AddScenarioCollectionViewCell: BaseCollectionViewCell {
    
    static var REUSE_ID = "addScenarioCell"
        
    override var reuseIdentifier: String? {
        return "addScenarioCell"
    }
    
    private var image = UIImageView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        contentView.addSubview(image)
        image.contentMode = .center
        image.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        image.image = UIImage.init(named: "plus")
        image.becomeRounded()
        image.becomeStrocked(with: UIColor.black)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

class DeviceCollectionViewCell: BaseCollectionViewCell {
    
    static let REUSE_ID = "deviceCell"
        
    override var reuseIdentifier: String? {
        return "deviceCell"
    }
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var deviceNameView: DeviceNameView!
    
    
    func configureWith(device: NLTXDevice) {
        deviceNameView.configure(name: device.name ?? String.empty, roomName: device.getRoomName())
        guard let index = device.iconIndex, index <= Images.devicesIcons.count else {
            image.image = Images.devicesIcons[0]
            return
        }
        image.image = Images.devicesIcons[index]
    }
    
    func configureWith(sensor: NLRXDevice) {
        deviceNameView.configure(name: sensor.name ?? String.empty, roomName: sensor.getRoomName())
        image.image = {
            switch sensor.subType {
            case .leakSensor:
                return Images.sensorsIcons[7]
            case .lightSensor:
                return Images.scenarioIcons[2]
            case .motionSensor:
                return Images.sensorsIcons[4]
            case .control:
                return Images.devicesIcons[6]
            case .openingSensor:
                return Images.sensorsIcons[0]
            default:
                return UIImage()
            }
        }()
        
    }

}

//
//  FDeviceCollectionViewCell.swift
//  newNL
//
//  Created by aggrroo on 2/24/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class FDeviceCollectionViewCell: BaseCollectionViewCell {

    @IBOutlet var deviceNameView: DeviceNameView!
    @IBOutlet var imageVIew: UIImageView!
    @IBOutlet var stateImageView: UIImageView!
    @IBOutlet var content: UIView!
    
    static let REUSE_ID = "fDeviceCell"
    
    override var reuseIdentifier: String? {
        return "fDeviceCell"
    }
    
    func configureWith(fDevice: NLFTXDevice) {
        deviceNameView.configure(name: fDevice.name ?? String.empty, roomName: fDevice.getRoomName())
        layer.borderWidth = 0
        content.becomeStrocked(with: black)
        let count = Images.devicesIcons.count
        if let ind = fDevice.iconIndex, ind < count {
            imageVIew.image = Images.devicesIcons[ind]
        }
        switch fDevice.currentState {
        case .on:
            if fDevice.setts != "00" {
                stateImageView.image = UIImage.init(named: "nooOn")
            } else {
                stateImageView.image = UIImage(named: "nooOff")
            }
        case .off:
            stateImageView.image = UIImage(named: "nooNoConnection")
        default:
            stateImageView.image = UIImage(named: "nooNoConnection")
        }
    }

}

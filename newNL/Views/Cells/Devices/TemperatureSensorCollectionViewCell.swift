//
//  TemperatureSensorCollectionViewCell.swift
//  newNL
//
//  Created by aggrroo on 2/24/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class TemperatureSensorCollectionViewCell: BaseCollectionViewCell {
    
    @IBOutlet var deviceNameView: DeviceNameView!
    
    static let REUSE_ID = "temperatureSensCell"

    @IBOutlet var temperatureImageView: UIImageView!
    @IBOutlet var humidityImageVIew: UIImageView!
    
    @IBOutlet var temperatureLabel: UILabel!
    @IBOutlet var humidityLabel: UILabel!
    
    
    
    override var reuseIdentifier: String? {
        return "temperatureSensCell"
    }

    func configureWith(sensor: NLRXDevice) {
        deviceNameView.configure(name: sensor.name ?? String.empty, roomName: sensor.getRoomName())
        temperatureImageView.image = Images.baseSensorsImages[0]
        temperatureLabel.text = "-/-°С"
        humidityImageVIew.image = nil
        humidityLabel.text = nil
        switch sensor.subType {
        case .temperatureAndHumiditySensor:
            humidityImageVIew.image = Images.baseSensorsImages[1]
            humidityLabel.text = "-/-%"
        default:
            return
        }
    }
}

//
//  DayView.swift
//  newNL
//
//  Created by aggrroo on 20.10.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation
import UIKit

class DayView: UICollectionViewCell {
    
    private static let CORNER_RADIUS: CGFloat = 4
    
    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }    
    
    func configure(day: Bit, name: String) {
        let fontSize = bounds.height * 0.6
        nameLabel.font = UIFont.systemFont(ofSize: fontSize, weight: .thin)
        borderView.layer.cornerRadius = DayView.CORNER_RADIUS
        borderView.layer.borderWidth = BORDER_WIDTH
        borderView.layer.borderColor = UIColor.clear.cgColor
        switch day.rawValue {
        case 0:
            nameLabel.text = name
            nameLabel.alpha = ALPHA_60
        case 1:
            nameLabel.textColor = .black
            nameLabel.text = name
            borderView.layer.borderColor = UIColor.black.cgColor
            nameLabel.alpha = SIMPLE_APLHA
        default:
            return
        }
    }
}

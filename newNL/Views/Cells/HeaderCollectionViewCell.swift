//
//  HeaderCollectionViewCell.swift
//  newNL
//
//  Created by aggrroo on 2/24/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class HeaderCollectionViewCell: UICollectionViewCell {
    
    static let REUSE_ID = "headerCell"

    override var reuseIdentifier: String? {
        return "headerCell"
    }

    @IBOutlet weak var imageWidth: NSLayoutConstraint!
    @IBOutlet weak var toImageConstraint: NSLayoutConstraint!
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var label: UILabel!

    func configure(section: Int, isFavorites: Bool) {
        imageWidth.constant = isFavorites ? 20 : 0
        toImageConstraint.constant = isFavorites ? 10 : 0
        backgroundColor = section == 0 ? UIColor.groupTableViewBackground : Colors.white.uiValue
        label.text = section == 0 ? "Сценарии" : "Устройства"
    }
}

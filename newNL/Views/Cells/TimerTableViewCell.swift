//
//  TimerTableViewCell.swift
//  newNL
//
//  Created by aggrroo on 19.10.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

class TimerTableViewCell: UITableViewCell {

    @IBOutlet private var name: UILabel!
    @IBOutlet private var daysCollection: UICollectionView!
    @IBOutlet private var timeView: UILabel!
    @IBOutlet private var stack: UIStackView!
    
    weak var delegate: CellWithSettingsDelegate?
    
    private var timer: NLTimer?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        daysCollection.register(UINib(nibName: "DayView", bundle: nil), forCellWithReuseIdentifier: "dayCell")
        layoutIfNeeded()
        selectionStyle = .none
    }

    func configure(with timer: NLTimer) {
        self.timer = timer
        name.text = timer.name
        name.textColor = black
        let timeString: NSMutableAttributedString = {
            guard let type = timer.timerType else { return String.empty.attributed }
            switch type {
            case .on:
                guard let onTime = timer.onTime else { return String.empty.attributed }
                let timeStr = "Включить в ".attributedLight16
                timeStr.append(onTime.getStringTime().attributedThin24)
                return timeStr
            case .off:
                guard let onTime = timer.onTime else { return String.empty.attributed }
                let timeStr = "Выключить в ".attributedLight16
                timeStr.append(onTime.getStringTime().attributedThin24)
                return timeStr
            case .onAndOff:
                guard let onTime = timer.onTime, let offTime = timer.offTime else { return String.empty.attributed }
                let timeStr = "Включить в ".attributedLight16
                timeStr.append(onTime.getStringTime().attributedThin24)
                timeStr.append( "\r\nВыключить в ".attributedLight16)
                timeStr.append(offTime.getStringTime().attributedThin24)
                return timeStr
            case .onAuto:
                guard let onTime = timer.onTime else { return String.empty.attributed }
                let timeStr = "Включить автоматизацию в ".attributedLight16
                timeStr.append(onTime.getStringTime().attributedThin24)
                return timeStr
            case .offAuto:
                guard let onTime = timer.onTime else { return String.empty.attributed }
                let timeStr = "Выключить автоматизацию в ".attributedLight16
                timeStr.append(onTime.getStringTime().attributedThin24)
                return timeStr
            case .onAndOffAuto:
                guard let onTime = timer.onTime, let offTime = timer.offTime else { return String.empty.attributed }
                let timeStr = "Включить автоматизацию в ".attributedLight16
                timeStr.append(onTime.getStringTime().attributedThin24)
                timeStr.append( "\r\nВыключить автоматизацию в ".attributedLight16)
                timeStr.append(offTime.getStringTime().attributedThin24)
                return timeStr
            case .runScenario:
                guard let onTime = timer.onTime else { return String.empty.attributed }
                let timeStr = "Вызвать сценарий в ".attributedLight16
                timeStr.append(onTime.getStringTime().attributedThin24)
                return timeStr
            case .sunrise:
                guard let onTime = timer.onTime else { return String.empty.attributed }
                let timeStr = "Рассвет в ".attributedLight16
                timeStr.append(onTime.getStringTime().attributedThin24)
                return timeStr
            case .dusk:
                guard let onTime = timer.onTime else { return String.empty.attributed }
                let timeStr = "Закат в ".attributedLight16
                timeStr.append(onTime.getStringTime().attributedThin24)
                return timeStr
            case .unknown:
                return String.empty.attributed
            }
        }()
        timeView.attributedText = timeString
        timeView.textColor = black
        daysCollection.reloadData()
        stack.alpha = (timer.isActive ?? false) ? SIMPLE_APLHA : ALPHA_20
    }

    @IBAction func settingsPressed(_ sender: UIButton) {
        delegate?.settingsPressed(cell: self)
    }
    
}

extension TimerTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Day.DAYS_COUNT
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dayCell", for: indexPath) as? DayView else { return UICollectionViewCell() }
        guard let days = timer?.calendar?.days, let day = days[indexPath.row].value else { return cell }
        cell.configure(day: day, name: NLCalendar.DAYS_SHORT_NAMES[indexPath.row])
        return cell
    }
    
}

extension TimerTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 20, height: 20)
    }
    
}

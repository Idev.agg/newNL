//
//  SetParametersCollectionViewCell.swift
//  newNL
//
//  Created by Сергей Глеб on 10/3/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

protocol SetParametersTableViewCellDelegate: class {
    func didUpdateValue(_ value: Int)
}

class SetParametersTableViewCell: BaseTableViewCell {
    
    static let REUSE_ID = "setParametersCell"
    
    override var reuseIdentifier: String? {
        return "setParametersCell"
    }
    weak var delegate: SetParametersTableViewCellDelegate?
    private var bgView = UIView()
    private lazy var minusButton = UIButton.buttonWith(title: nil, image: UIImage.init(named: "minus"), strocked: false, target: self, selector: #selector(buttonTapped(_:)))
    private lazy var plusButton = UIButton.buttonWith(title: nil, image: UIImage.init(named: "plus"), strocked: false, target: self, selector: #selector(buttonTapped(_:)))
    private lazy var valueLabel = UILabel.initWith(text: nil, font: UIFont.systemFont(ofSize: 15, weight: .light), textColor: .black, align: .center)
    private lazy var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fillProportionally
        stack.spacing = 0
        return stack
    }()
    private lazy var mainStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .equalSpacing
        stack.spacing = 0
        return stack
    }()
    private lazy var titleLabel = UILabel.initWith(text: nil, font: UIFont.systemFont(ofSize: 15, weight: .medium), textColor: .black)
    
    private var currentValue = 0
    private var currentType: DataType?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(titleLabel)
        titleLabel.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.left.equalToSuperview().offset(20)
            maker.right.equalToSuperview().offset(-20)
        }
        contentView.addSubview(mainStackView)
        mainStackView.snp.makeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom).offset(5)
            maker.bottom.equalToSuperview().offset(-15)
            maker.left.equalToSuperview().offset(20)
            maker.right.lessThanOrEqualToSuperview().offset(-20)
        }
        mainStackView.addArrangedSubview(bgView)
        mainStackView.addArrangedSubview(UIView())
        
        bgView.addSubview(stackView)
        bgView.becomeRounded(radius: 6)
        bgView.becomeStrocked(with: .black)
        stackView.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }

        let firstSeparator = UIView.initWith(color: .black)
        let secondSeparator = UIView.initWith(color: .black)
        
        stackView.addArrangedSubview(minusButton)
        stackView.addArrangedSubview(firstSeparator)
        stackView.addArrangedSubview(valueLabel)
        stackView.addArrangedSubview(secondSeparator)
        stackView.addArrangedSubview(plusButton)
        
        firstSeparator.snp.makeConstraints { (maker) in
            maker.width.equalTo(1)
        }
        secondSeparator.snp.makeConstraints { (maker) in
            maker.width.equalTo(1)
        }
        minusButton.snp.makeConstraints { (maker) in
            maker.size.equalTo(CGSize(width: 60, height: 44))
        }
        valueLabel.snp.makeConstraints { (maker) in
            maker.width.equalTo(90)
        }
        plusButton.snp.makeConstraints { (maker) in
            maker.size.equalTo(CGSize(width: 60, height: 44))
        }
    }
    
    @objc private func buttonTapped(_ button: UIButton) {
        let isPlus = button == plusButton
        let value = isPlus ? currentValue + 1 : currentValue - 1
        if isValueEditable(newValue: value) {
            currentValue = value
            guard let delegate = delegate else { return }
            delegate.didUpdateValue(currentValue)
        }
        valueLabel.text = currentValue.displayName
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func isValueEditable(newValue: Int) -> Bool {
        guard let currentType = currentType else { return false }
        var canEdit = true
        switch currentType {
        case .temperatureParameter:
            canEdit = newValue >= -40 && newValue <= 100
        case .humidityParameter:
            canEdit = newValue >= 0 && newValue <= 100
        default:
            canEdit = false
        }
        return canEdit
    }
    
    func configure(entity: SourceModel?, type: DataType) {
        currentValue = entity?.value as? Int ?? defaultValueForType(type)
        currentType = type
        valueLabel.text = currentValue.displayName
        titleLabel.text = titleForType(type)
        guard let delegate = delegate else { return }
        delegate.didUpdateValue(currentValue)
    }
    
    func defaultValueForType(_ type: DataType) -> Int {
        switch type {
        case .temperatureParameter:
            return 25
        case .humidityParameter:
            return 70
        default:
            return 0
        }
    }
    
    func titleForType(_ type: DataType) -> String {
        switch type {
        case .temperatureParameter:
            return "Температура (-40°...100°)"
        case .humidityParameter:
            return "Влажность (0%...100%)"
        default:
            return String.empty
        }
    }
    
}

//
//  MenuTableViewCell.swift
//  newNL
//
//  Created by aggrroo on 3/17/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class MenuTableViewCell: BaseTableViewCell {
    
    
    @IBOutlet var icon: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var botLine: UIView!
    
    @IBOutlet var spaceToImage: NSLayoutConstraint!
    
    static let REUSE_ID = "menuTableCell"

    override var reuseIdentifier: String? {
        return "menuTableCell"
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = .clear
        selectionStyle = .none
    }
    
    override func prepareForReuse() {
        icon.isHidden = false
        icon.image = nil
        title.text = nil
    }
    
    func configureWith(model: MenuCellModel) {
        icon.isHidden = model.image == nil
        icon.image = model.image
        if model.value != nil {
            title.text = model.value?.displayName
        } else {
            title.text = model.text
        }
        title.textColor = model.textColor
        botLine.backgroundColor = model.isLast ? UIColor.clear : model.textColor
        spaceToImage.constant = model.image == nil ? -size30 : size5
    }
}

//
//  ScenariousCollectionViewCell.swift
//  newNL
//
//  Created by aggrroo on 2/24/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class ScenariousCollectionViewCell: UICollectionViewCell {
    
    static let REUSE_ID = "scenariousCell"
        
    override var reuseIdentifier: String? {
        return "scenariousCell"
    }
    
    @IBOutlet var collection: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        collection.register(UINib(nibName: "ScenarioCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ScenarioCollectionViewCell.REUSE_ID)
    }
    
    private var handler: (() -> ())?
    private var scenarious: [NLScenario]?
    private var lastUsed: NLScenario?
    
    func configureWitshScenarious(scenarious: [NLScenario]?, selectAction: @escaping (() -> ())) {
        self.scenarious = scenarious
        handler = selectAction
        collection.reloadData()
    }

}

extension ScenariousCollectionViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return scenarious?.count ?? 0 + 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ScenarioCollectionViewCell().reuseIdentifier!, for: indexPath) as? ScenarioCollectionViewCell
        guard let wrappedCell = cell else { return UICollectionViewCell() }
        guard indexPath.row != (scenarious?.count ?? 1) - 1 else {
            wrappedCell.configureAddingCell()
            return wrappedCell
        }
        guard let scenarious = scenarious else { return wrappedCell }
        return wrappedCell.cast(object: scenarious[indexPath.row], collection: collectionView, indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            handler?()
        }
    }
    
}

extension ScenariousCollectionViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 20)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width / 2.5, height: 44)
    }
    
}

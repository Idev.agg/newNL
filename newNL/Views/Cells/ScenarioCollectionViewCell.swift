//
//  ScenarioCollectionViewCell.swift
//  newNL
//
//  Created by aggrroo on 2/24/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class ScenarioCollectionViewCell: BaseCollectionViewCell {

    @IBOutlet var image: UIImageView!
    @IBOutlet var label: UILabel!
    
    static let REUSE_ID = "scenarioCell"
    
    override var reuseIdentifier: String? {
        return "scenarioCell"
    }
    
    func configureWith(scenario: NLScenario) {
        label.text = scenario.name
        guard let index = scenario.iconIndex, index <= Images.scenarioIcons.count else {
            image.image = Images.scenarioIcons[0]
            return
        }
        image.image = Images.scenarioIcons[index]
    }
    
    func configureAddingCell() {
        label.text = "Добавить"
    }

}

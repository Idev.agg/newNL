//
//  Images.swift
//  newNL
//
//  Created by aggrroo on 01.11.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation
import UIKit


class Images {
    
    static let roomsIcons = [#imageLiteral(resourceName: "nooAlcove"),#imageLiteral(resourceName: "nooBathRoom"),#imageLiteral(resourceName: "nooBedRoom"),#imageLiteral(resourceName: "nooChieldrensRoom"),#imageLiteral(resourceName: "nooDinningRoom"),#imageLiteral(resourceName: "nooFirePlace"),#imageLiteral(resourceName: "nooGarage"),#imageLiteral(resourceName: "nooGasPlate"),#imageLiteral(resourceName: "nooHall"),#imageLiteral(resourceName: "nooKitchen"),#imageLiteral(resourceName: "nooLivingRoom"),#imageLiteral(resourceName: "nooLodgy"),#imageLiteral(resourceName: "nooPool"),#imageLiteral(resourceName: "nooStreet"),#imageLiteral(resourceName: "nooToilet"),#imageLiteral(resourceName: "nooWarderobe"),#imageLiteral(resourceName: "nooWashRoom")]
    static let scenarioIcons = [#imageLiteral(resourceName: "backDoor"),#imageLiteral(resourceName: "imhome"),#imageLiteral(resourceName: "morning"),#imageLiteral(resourceName: "night")]
    static let whiteScenarioIcons = [#imageLiteral(resourceName: "backWhiteDoor"),#imageLiteral(resourceName: "imhomeWhite"),#imageLiteral(resourceName: "morningWhite"),#imageLiteral(resourceName: "nightWhite")]
    static let sensorsIcons = [#imageLiteral(resourceName: "doorOff"),#imageLiteral(resourceName: "doorOn"),#imageLiteral(resourceName: "doorSens"),#imageLiteral(resourceName: "moveOn"),#imageLiteral(resourceName: "moveSens"),#imageLiteral(resourceName: "waterOn"),#imageLiteral(resourceName: "waterOff"),#imageLiteral(resourceName: "waterSens")]
    static let devicesIcons: [UIImage] = [#imageLiteral(resourceName: "nooLamp"),#imageLiteral(resourceName: "nooLight"),#imageLiteral(resourceName: "nooBra"),#imageLiteral(resourceName: "nooCetle"),#imageLiteral(resourceName: "nooChandelier"),#imageLiteral(resourceName: "nooComp"),#imageLiteral(resourceName: "nooSocket"),#imageLiteral(resourceName: "nooDoubleSocket"),#imageLiteral(resourceName: "nooFan"),#imageLiteral(resourceName: "nooFloorLamp"),#imageLiteral(resourceName: "nooIron"),#imageLiteral(resourceName: "nooLighter"),#imageLiteral(resourceName: "nooRolls"),#imageLiteral(resourceName: "nooTableLamp"),#imageLiteral(resourceName: "nooTherm"),#imageLiteral(resourceName: "nooThermoSpot"),#imageLiteral(resourceName: "nooTV")]
    static let baseSensorsImages = [#imageLiteral(resourceName: "nooTemp"),#imageLiteral(resourceName: "humidity")]
    
}

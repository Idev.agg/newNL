//
//  Spinner.swift
//  JUNO
//
//  Created by Sergey on 1/10/19.
//  Copyright © 2019 elatesoftware. All rights reserved.
//

import UIKit

class Spinner: UIView {
    
    private var circles = [GradientView]()
    private let baseSize = CGSize(width: 7, height: 7)
    
    private var xCoordsMultipliers: [CGFloat] = [1, cos(.pi/6), cos(.pi/3), 0, -cos(.pi/3), -cos(.pi/6), -1, -cos(.pi/6), -cos(.pi/3), 0, cos(.pi/3), cos(.pi/6)]

    private lazy var timer = Timer.scheduledTimer(timeInterval: BASE_ANIMATION_DURATION / 3, target: self, selector: #selector(animate), userInfo: nil, repeats: true)
    
    func setup() {
        let frame = CGRect(x: 0, y: 0, width: bounds.width + 1, height: bounds.height + 1)
        let gradient = GradientView(frame: frame)
        gradient.setup(with: [lightGreen.cgColor, darkGreen.cgColor])
        gradient.makeGradientDiagonaled()
        for i in 0..<xCoordsMultipliers.count {
            let xMulti = xCoordsMultipliers[i]
            let x = (xMulti + 1) * bounds.height / 2
            let yMulti = sqrt(pow(1, 2) - pow(xMulti, 2))
            var y = (yMulti - 1 ) * bounds.height / 2
            y = i >= 6 ? y + bounds.height : -y
            let circleView = GradientView()
            circleView.frame.size = baseSize
            circleView.center = CGPoint(x: x, y: y)
            circleView.becomeRounded()
            addSubview(circleView)
            circleView.getColor(inoutView: gradient)
            circles.append(circleView)
        }
        clipsToBounds = false
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview()
        guard superview == nil else {
            startAnimating()
            return
        }
        stopAnimating()
    }
    
    func startAnimating() {
        timer.fire()
    }
    
    @objc private func animate() {
        guard let moved = circles.last, circles.count == 12 else { return }
        circles.remove(at: circles.count - 1)
        circles = [moved] + circles
        UIView.animate(withDuration: BASE_ANIMATION_DURATION  / 3) { [weak self] in
            guard let count = self?.circles.count else { return }
            for i in 0..<count {
                switch i {
                case 0: self?.circles[i].transform = CGAffineTransform.init(scaleX: 1.5, y: 1.5)
                case 1: self?.circles[i].transform = CGAffineTransform.init(scaleX: 1.3, y: 1.3)
                case 2: self?.circles[i].transform = CGAffineTransform.init(scaleX: 1.1, y: 1.1)
                case 3: self?.circles[i].transform = CGAffineTransform.init(scaleX: 0.9, y: 0.9)
                case 4: self?.circles[i].transform = CGAffineTransform.init(scaleX: 0.7, y: 0.7)
                case 5: self?.circles[i].transform = CGAffineTransform.init(scaleX: 0.5, y: 0.5)
                default: self?.circles[i].transform = CGAffineTransform.init(scaleX: 0.3, y: 0.3)
                }
            }
        }
    }
    
    func stopAnimating() {
        timer.invalidate()
    }
    
}

private extension GradientView {
    
    func getColor(inoutView: GradientView) {
        guard let gradientLayer = inoutView.layer as? CAGradientLayer else { return }
        layer.backgroundColor = gradientLayer.colorOfPoint(point: center)
    }
    
}

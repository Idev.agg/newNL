//
//  ProgressView.swift
//  JUNO
//
//  Created by Sergey on 1/10/19.
//  Copyright © 2019 elatesoftware. All rights reserved.
//

import UIKit
import SnapKit

class ProgressView: NibView {
    
    @IBOutlet private var descriptionLabel: UILabel!
    @IBOutlet private var spinner: Spinner!
    
    init() {
        super.init(frame: .zero)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup(description: String? = nil) {
        descriptionLabel.text = (description ?? "Описание").uppercased()
        spinner.setup()
    }
    
}

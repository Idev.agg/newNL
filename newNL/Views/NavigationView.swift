//
//  NavigationView.swift
//  newNL
//
//  Created by aggrroo on 2/6/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class NavigationView: NibView {
    
    @IBOutlet var title: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configure(titleText: String?, left: Button?, right: Button?) {
        self.translatesAutoresizingMaskIntoConstraints = false
        if let left = left {
            view.addSubviewWithZero(subview: left, attributes: [.top, .left, .bottom])
            let constraint = NSLayoutConstraint(item: left, attribute: .trailing, relatedBy: .lessThanOrEqual, toItem: title, attribute: .leading, multiplier: 1, constant: -size5)
            view.addConstraint(constraint)
            
        }
        if let right = right {
            view.addSubviewWithZero(subview: right, attributes: [.top, .right, .bottom])
            view.addConstraint(NSLayoutConstraint(item: right, attribute: .leading, relatedBy: .greaterThanOrEqual, toItem: title, attribute: .trailing, multiplier: 1, constant: size5))
        }
        title.text = titleText
        title.textColor = white
    }
}

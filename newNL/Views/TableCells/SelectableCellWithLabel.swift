//
//  CellWithLabel.swift
//  newNL
//
//  Created by aggrroo on 4/23/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

class SelectableCellWithLabel: BaseTableViewCell {
    
    static let REUSE_ID = "SelectableCellWithLabel"
    
    lazy var label: UILabel = {
        return UILabel.initWith(text: nil, font: UIFont.systemFont(ofSize: 15), textColor: black)
    }()
    
    var checkMarkImageView = UIImageView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        configureLabel()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureLabel()
    }
    
    func configureLabel() {
        addSubview(label)
        addSubview(checkMarkImageView)
        label.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 10, left: 20, bottom: 10, right: 50))
        checkMarkImageView.autoPinEdge(toSuperviewEdge: .right, withInset: 20)
        checkMarkImageView.autoSetDimensions(to: CGSize(width: 15, height: 15))
        checkMarkImageView.contentMode = .scaleAspectFit
        checkMarkImageView.autoAlignAxis(toSuperviewAxis: .horizontal)
    }
    
    func configure(entity: Any, selected: Bool) {
        checkMarkImageView.image = selected ? UIImage(named: "checkMark") : nil
        label.text = entity as? String
    }
    
}

//
//  BaseTableViewCell.swift
//  newNL
//
//  Created by aggrroo on 3/9/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit
import PureLayout

protocol BaseTableViewCellProtocol {
    
    func configure(entity: Any)
    
}

class BaseTableViewCell: UITableViewCell, BaseTableViewCellProtocol {
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        selectionStyle = .none
    }
    
    func configure(entity: Any) {
    }
    
}

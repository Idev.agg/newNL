//
//  ParametersTableViewCell.swift
//  newNL
//
//  Created by aggrroo on 3/9/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit
import SnapKit
import PureLayout

class ChoosableParametersTableViewCell: BaseTableViewCell {
    
    static let REUSE_ID = "parametersTableViewCell"
    
    var titleLabel = UILabel()
    var errorLabel = UILabel()
    var content = UIView()
    var parameterNameLabel = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initUi()
    }
    
    func initUi() {
        titleLabel = UILabel.initWith(text: nil, font: .systemFont(ofSize: size15, weight: .medium), textColor: black)
        errorLabel = UILabel.initWith(text: nil, font: .systemFont(ofSize: size13, weight: .light), textColor: red)
        parameterNameLabel = UILabel.initWith(text: nil, font: .systemFont(ofSize: 15), textColor: black)
        let schevron = UIImageView(image: UIImage(named: "right"))
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(content)
        contentView.addSubview(errorLabel)
        content.addSubview(parameterNameLabel)
        content.addSubview(schevron)
        content.becomeStrocked(with: black)
        
        titleLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 20), excludingEdge: .bottom)
        
        parameterNameLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 0), excludingEdge: .right)
        parameterNameLabel.autoSetDimension(.height, toSize: 30, relation: .greaterThanOrEqual)
        schevron.autoPinEdge(toSuperviewEdge: .right)
        schevron.autoSetDimensions(to: CGSize(width: 20, height: 20))
        schevron.contentMode = .scaleAspectFit
        schevron.autoAlignAxis(.horizontal, toSameAxisOf: parameterNameLabel)
        schevron.autoPinEdge(.left, to: .right, of: parameterNameLabel, withOffset: 10)
        
        content.autoPinEdge(toSuperviewEdge: .left, withInset: 20)
        content.autoPinEdge(toSuperviewEdge: .right, withInset: 20)
        content.autoPinEdge(.top, to: .bottom, of: titleLabel, withOffset: 5)
        
        errorLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0, left: 20, bottom: 10, right: 20), excludingEdge: .top)
        errorLabel.autoPinEdge(.top, to: .bottom, of: content, withOffset: 5)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initUi()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        titleLabel.text = nil
        parameterNameLabel.text = nil
        errorLabel.text = nil
        super.prepareForReuse()
    }
    
    override func configure(entity: Any) {
        guard entity is SourceModel else { return }
        parameterNameLabel.text = "Выберите"
        let entity = entity as! SourceModel
        titleLabel.text = entity.title
        content.becomeStrocked(with: (entity.error == nil) ? black : red)
        guard entity.error == nil, entity.value != nil else {
            errorLabel.text = entity.error?.discription
            return
        }
        parameterNameLabel.text = entity.value as? String
        if entity.value is Displayable {
            parameterNameLabel.text = (entity.value as! Displayable).displayName
        }
    }
    
}

class CellWithTextField: BaseTableViewCell {
    
    static let REUSE_ID = "TableViewCellWithTextField"
    
    private var titleLabel = UILabel()
    private var errorLabel = UILabel()
    private var content = UIView()
    var parameterNameTextField = UITextField()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        initUi()
    }
    
    func initUi() {
        titleLabel = UILabel.initWith(text: nil, font: .systemFont(ofSize: size15, weight: .medium), textColor: black)
        errorLabel = UILabel.initWith(text: nil, font: .systemFont(ofSize: size13, weight: .light), textColor: red)
        parameterNameTextField.font = .systemFont(ofSize: 15)
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(content)
        contentView.addSubview(errorLabel)
        content.addSubview(parameterNameTextField)
        content.becomeStrocked(with: black)
        
        titleLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 20), excludingEdge: .bottom)
        
        parameterNameTextField.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 5, left: 10, bottom: 5, right: 10))
        parameterNameTextField.autoSetDimension(.height, toSize: 30)
        
        content.autoPinEdge(toSuperviewEdge: .left, withInset: 20)
        content.autoPinEdge(toSuperviewEdge: .right, withInset: 20)
        content.autoPinEdge(.top, to: .bottom, of: titleLabel, withOffset: 5)
        
        errorLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0, left: 20, bottom: 10, right: 20), excludingEdge: .top)
        errorLabel.autoPinEdge(.top, to: .bottom, of: content, withOffset: 5)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        initUi()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        titleLabel.text = nil
        parameterNameTextField.text = nil
        errorLabel.text = nil
        super.prepareForReuse()
    }
    
    override func configure(entity: Any) {
        guard entity is SourceModel else { return }
        let entity = entity as! SourceModel
        titleLabel.text = entity.title
        parameterNameTextField.placeholder = entity.title
        content.becomeStrocked(with: (entity.error == nil) ? black : red)
        guard entity.error == nil, entity.value != nil else {
            errorLabel.text = entity.error?.discription
            return
        }
        parameterNameTextField.text = entity.value as? String
    }
    
}

protocol ChooseDevicesTableViewCellDelegate: class {
    func didSelectCellWithDevice(_ device: NLBaseDevice?, isAddTapped: Bool, cell: UITableViewCell)
}

class ChooseDevicesTableViewCell: BaseTableViewCell {
    
    static let REUSE_ID = "ChooseDevicesTableViewCell"
    
    weak var delegate: ChooseDevicesTableViewCellDelegate?
    
    var titleLabel = UILabel()
    var errorLabel = UILabel()
    var content = UIView()
    var devicesCollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var devices: [NLBaseDevice]?
    
    var heightCollection: NSLayoutConstraint?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        devicesCollectionView.register(UINib(nibName: "DeviceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: DeviceCollectionViewCell.REUSE_ID)
        devicesCollectionView.register(UINib(nibName: "FDeviceCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: FDeviceCollectionViewCell.REUSE_ID)
        devicesCollectionView.register(UINib(nibName: "ThermostatCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ThermostatCollectionViewCell.REUSE_ID)
        devicesCollectionView.register(UINib(nibName: "TemperatureSensorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: TemperatureSensorCollectionViewCell.REUSE_ID)
        devicesCollectionView.register(AddDeviceCollectionViewCell.self, forCellWithReuseIdentifier: AddDeviceCollectionViewCell.REUSE_ID)
        initUi()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initUi() {
        titleLabel = UILabel.initWith(text: nil, font: .systemFont(ofSize: size15, weight: .medium), textColor: black)
        errorLabel = UILabel.initWith(text: nil, font: .systemFont(ofSize: size13, weight: .light), textColor: red)
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(content)
        content.addSubview(devicesCollectionView)
        devicesCollectionView.delegate = self
        devicesCollectionView.dataSource = self
        devicesCollectionView.isScrollEnabled = false
        contentView.addSubview(errorLabel)
        devicesCollectionView.backgroundColor = UIColor.clear
        devicesCollectionView.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
        }
        heightCollection = devicesCollectionView.autoSetDimension(.height, toSize: 0)
        
        titleLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 20), excludingEdge: .bottom)
        
        content.autoPinEdge(toSuperviewEdge: .left, withInset: 20)
        content.autoPinEdge(toSuperviewEdge: .right, withInset: 20)
        content.autoPinEdge(.top, to: .bottom, of: titleLabel, withOffset: 5)
        
        errorLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0, left: 20, bottom: 10, right: 20), excludingEdge: .top)
        errorLabel.autoPinEdge(.top, to: .bottom, of: content, withOffset: 5)
        
    }
    
    override func configure(entity: Any) {
        super.configure(entity: entity)
        guard let ent = entity as? SourceModel else { return }
        devices = ent.value as? [NLBaseDevice]
        titleLabel.text = ent.title
        errorLabel.text = ent.error?.discription
        calculateHeight()
        devicesCollectionView.reloadData()
        
    }
    
    private func calculateHeight() {
        let count: Double = Double((devices?.count ?? 0) + 1)
        let multiplier = (count / 3).rounded(.up)
        let size: Double = Double((UIScreen.main.bounds.width - size20 * 2) / 3.2)
        let height = size * multiplier + (multiplier + 1) * Double(size10)
        heightCollection?.constant = CGFloat(height)
    }
    
}

extension ChooseDevicesTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (devices?.count ?? 0 > 0) {
            return devices?.count ?? 0
        } else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        if let devices = devices, devices.count > 0 {
            cell = cell.cast(object: devices[indexPath.row], collection: collectionView, indexPath: indexPath)
        } else {
            cell = (collectionView.dequeueReusableCell(withReuseIdentifier: AddDeviceCollectionViewCell.REUSE_ID, for: indexPath) as? AddDeviceCollectionViewCell) ?? UICollectionViewCell()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = (UIScreen.main.bounds.width - size20 * 2) / 3.2
        return CGSize(width: size, height: size)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return size10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return size5
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let delegate = delegate else { return }
        if let devices = devices, devices.count > 0 {
            delegate.didSelectCellWithDevice(devices[indexPath.row], isAddTapped: false, cell: self)
        } else {
            delegate.didSelectCellWithDevice(nil, isAddTapped: true, cell: self)
        }
        
    }
    
}

protocol ChooseScenarioTableViewCellDelegate: class {
    func didSelectCellWithScenario(_ device: NLScenario?, isAddTapped: Bool, cell: UITableViewCell)
}

class ChooseScenarioTableViewCell: BaseTableViewCell {
    
    static let REUSE_ID = "ChooseScenarioTableViewCell"
    
    weak var delegate: ChooseScenarioTableViewCellDelegate?
    
    var titleLabel = UILabel()
    var errorLabel = UILabel()
    var content = UIView()
    var devicesCollectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    var scenario: NLScenario?
        
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        devicesCollectionView.register(UINib(nibName: "ScenarioCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: ScenarioCollectionViewCell.REUSE_ID)
        devicesCollectionView.register(AddScenarioCollectionViewCell.self, forCellWithReuseIdentifier: AddScenarioCollectionViewCell.REUSE_ID)
        initUi()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initUi() {
        titleLabel = UILabel.initWith(text: nil, font: .systemFont(ofSize: size15, weight: .medium), textColor: black)
        errorLabel = UILabel.initWith(text: nil, font: .systemFont(ofSize: size13, weight: .light), textColor: red)
        
        contentView.addSubview(titleLabel)
        contentView.addSubview(content)
        content.addSubview(devicesCollectionView)
        devicesCollectionView.delegate = self
        devicesCollectionView.dataSource = self
        devicesCollectionView.isScrollEnabled = false
        contentView.addSubview(errorLabel)
        devicesCollectionView.backgroundColor = UIColor.clear
        devicesCollectionView.snp.makeConstraints { (maker) in
            maker.edges.equalToSuperview()
            maker.height.equalTo(50)
        }
        titleLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 20), excludingEdge: .bottom)
        
        content.autoPinEdge(toSuperviewEdge: .left, withInset: 20)
        content.autoPinEdge(toSuperviewEdge: .right, withInset: 20)
        content.autoPinEdge(.top, to: .bottom, of: titleLabel, withOffset: 5)
        
        errorLabel.autoPinEdgesToSuperviewEdges(with: UIEdgeInsets(top: 0, left: 20, bottom: 10, right: 20), excludingEdge: .top)
        errorLabel.autoPinEdge(.top, to: .bottom, of: content, withOffset: 5)
        
    }
    
    override func configure(entity: Any) {
        super.configure(entity: entity)
        guard let ent = entity as? SourceModel else { return }
        scenario = ent.value as? NLScenario
        titleLabel.text = ent.title
        errorLabel.text = ent.error?.discription
        devicesCollectionView.reloadData()
        
    }
    
}

extension ChooseScenarioTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        var cell = UICollectionViewCell()
        if let scenario = scenario {
            cell = cell.cast(object: scenario, collection: collectionView, indexPath: indexPath)
        } else {
            cell = (collectionView.dequeueReusableCell(withReuseIdentifier: AddScenarioCollectionViewCell.REUSE_ID, for: indexPath) as? AddScenarioCollectionViewCell) ?? UICollectionViewCell()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return .zero
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width / 2.5, height: 44)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let delegate = delegate else { return }
        if let scenario = scenario {
            delegate.didSelectCellWithScenario(scenario, isAddTapped: false, cell: self)
        } else {
            delegate.didSelectCellWithScenario(nil, isAddTapped: true, cell: self)
        }
        
    }
    
}


//
//  ComplexCalendarTableViewCell.swift
//  newNL
//
//  Created by Сергей Глеб on 10/4/19.
//  Copyright © 2019 Глеб Сергей. All rights reserved.
//

import UIKit

struct ComplexCalendarObject {
    
    var onTime: NLTimeObject?
    var offTime: NLTimeObject?
    var calendar: NLCalendar?
    
    var type: TimerType
    
    init(onTime: NLTimeObject?, offTime: NLTimeObject?, calendar: NLCalendar?, type: TimerType) {
        self.type = type
        self.onTime = onTime
        self.offTime = offTime
        self.calendar = calendar
    }
    
    init(type: TimerType) {
        self.type = type
        switch type {
        case .on, .onAuto, .dusk, .sunrise, .runScenario:
            onTime = NLTimeObject.default
        case .off, .offAuto:
            offTime = NLTimeObject.default
        case .onAndOff, .onAndOffAuto:
            onTime = NLTimeObject.default
            offTime = NLTimeObject.default
        case .unknown:
            break
        }
        calendar = NLCalendar.defaultValue
    }
    
}

protocol ComplexCalendarTableViewCellDelegate: class {
    
    func didChangeTime(_ obj: ComplexCalendarObject)
    
}

class ComplexCalendarTableViewCell: BaseTableViewCell {
    
    
    weak var delegate: ComplexCalendarTableViewCellDelegate?
    static let REUSE_ID = "complexCalendarCell"
    
    override var reuseIdentifier: String? {
        return "complexCalendarCell"
    }
    
    private var calendarObject: ComplexCalendarObject?
    
    private var titleLabel = UILabel.initWith(text: nil, font: UIFont.systemFont(ofSize: 16, weight: .medium), textColor: .black)
    
    private var stackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .horizontal
        stack.distribution = .fill
        stack.spacing = 0
        return stack
    }()
    
    private var onTimeTf: UITextField?
    private var offTimeTf: UITextField?
    
    private var calendarCollection = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewFlowLayout())
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        calendarCollection.register(UINib(nibName: "DayView", bundle: nil), forCellWithReuseIdentifier: "dayCell")
        contentView.addSubview(titleLabel)
        contentView.addSubview(stackView)
        contentView.addSubview(calendarCollection)
        
        titleLabel.snp.makeConstraints { (maker) in
            maker.left.equalToSuperview().offset(20)
            maker.right.equalToSuperview().offset(-20)
            maker.top.equalToSuperview().offset(10)
        }
        
        stackView.snp.makeConstraints { (maker) in
            maker.top.equalTo(titleLabel.snp.bottom).offset(8)
            maker.left.equalToSuperview().offset(20)
            maker.right.equalToSuperview().offset(-20)
        }
        
        calendarCollection.snp.makeConstraints { (maker) in
            maker.top.equalTo(stackView.snp.bottom).offset(8)
            maker.height.equalTo(32)
            maker.left.equalToSuperview().offset(20)
            maker.right.equalToSuperview().offset(-20)
            maker.bottom.equalToSuperview().offset(-15)
        }
        calendarCollection.delegate = self
        calendarCollection.dataSource = self
        calendarCollection.backgroundColor = UIColor.clear
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func configure(entity: Any) {
        guard let ent = entity as? SourceModel else { return }
        titleLabel.text = ent.title
        guard let calendarObj = ent.value as? ComplexCalendarObject else { return }
        self.calendarObject = calendarObj
        var needOn = true
        var needOff = true
        switch calendarObj.type {
        case .on, .onAuto, .sunrise, .dusk, .runScenario:
            needOff = false
        case .off, .offAuto:
            needOn = false
        default:
            break
        }
        
        if let onTime = calendarObj.onTime, needOn {
            if onTimeTf == nil {
                onTimeTf = UITextField()
                onTimeTf!.snp.makeConstraints { (maker) in
                    maker.height.equalTo(30)
                }
                onTimeTf?.tintColor = .clear
            }
            onTimeTf?.attributedText = onTime.getStringTime().attributedThin24
            onTimeTf?.inputView = configurePicker(with: onTime.getStringTime())
        }
        if let offTime = calendarObj.offTime, needOff {
            if offTimeTf == nil {
                offTimeTf = UITextField()
                offTimeTf?.tintColor = .clear
                offTimeTf!.snp.makeConstraints { (maker) in
                    maker.height.equalTo(30)
                }
            }
            offTimeTf?.attributedText = offTime.getStringTime().attributedThin24
            offTimeTf?.inputView = configurePicker(with: offTime.getStringTime())
        }
        if onTimeTf != nil {
            if !stackView.arrangedSubviews.contains(onTimeTf!) {
                stackView.addArrangedSubview(onTimeTf!)
            }
        }
        if offTimeTf != nil {
            if !stackView.arrangedSubviews.contains(offTimeTf!) {
                stackView.addArrangedSubview(offTimeTf!)
                if onTimeTf != nil {
                    offTimeTf!.snp.makeConstraints({ (maker) in
                        maker.width.equalTo(onTimeTf!.snp.width)
                    })
                }
            }
        }
        calendarCollection.reloadData()
    }
    
    private func configurePicker(with time: String) -> UIDatePicker {
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = .current
        dateFormatter.dateFormat = "HH:mm"
        
        let picker = UIDatePicker()
        picker.datePickerMode = .time
        
        if let dateTime = dateFormatter.date(from: time) {
            picker.setDate(dateTime, animated: false)
        }
        
        picker.addTarget(self, action: #selector(datePickerDateChanged(_:)), for: .valueChanged)
        
        return picker
    }
    
    @objc private func datePickerDateChanged(_ picker: UIDatePicker) {
        if onTimeTf?.isFirstResponder ?? false {
            let calendar = NSCalendar.current
            calendarObject?.onTime?.hours = calendar.component(.hour, from: picker.date)
            calendarObject?.onTime?.minutes = calendar.component(.minute, from: picker.date)
        } else if offTimeTf?.isFirstResponder ?? false {
            let calendar = NSCalendar.current
            calendarObject?.offTime?.hours = calendar.component(.hour, from: picker.date)
            calendarObject?.offTime?.minutes = calendar.component(.minute, from: picker.date)
        }
        offTimeTf?.attributedText = calendarObject?.offTime?.getStringTime().attributedThin24
        onTimeTf?.attributedText = calendarObject?.onTime?.getStringTime().attributedThin24
        guard let delegate = delegate, let obj = calendarObject else { return }
        delegate.didChangeTime(obj)
    }
    
}

extension ComplexCalendarTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Day.DAYS_COUNT
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "dayCell", for: indexPath) as? DayView else { return UICollectionViewCell() }
        guard let days = calendarObject?.calendar?.days, let day = days[indexPath.row].value else { return cell }
        cell.configure(day: day, name: NLCalendar.DAYS_SHORT_NAMES[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let day = calendarObject?.calendar?.days?[indexPath.row] else { return }
        calendarObject?.calendar?.days?[indexPath.row].value = day.value?.toggle()
        collectionView.reloadData()
        guard let delegate = delegate, let obj = calendarObject else { return }
        delegate.didChangeTime(obj)
    }
    
}

extension ComplexCalendarTableViewCell: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 30, height: 30)
    }
    
}

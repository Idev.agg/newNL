//
//  BackButtonView.swift
//  newNL
//
//  Created by aggrroo on 19.10.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation
import UIKit

enum ImagePosition {
    case left, right
}

class Button: UIView {
    
    var view = UIView()
    var label = UILabel()
    
    internal var pressedClosure: (() -> ())?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    convenience init(text: String? = nil, image: UIImage? = nil, imagePosition: ImagePosition? = nil, strocked: Bool = false) {
        self.init()
        translatesAutoresizingMaskIntoConstraints = false
        layoutIfNeeded()
        addSubviewWithConstraintsToSuperview(subview: view, top: size10, left: size20, right: size20, bottom: size10)
        view.addSubviewWithConstraintsToSuperview(subview: label, top: size5, left: size5, right: size5, bottom: size5)
        var attText = NSMutableAttributedString()
        let font = UIFont.systemFont(ofSize: size16, weight: .regular)
        let color = UIColor.white
        if image == nil, let text = text {
            attText = text.attributedString(font: font, color: color)
        } else if text == nil, let image = image {
            attText = NSMutableAttributedString.getSingleImageString(image: image)
        } else if let text = text, let image = image, let imagePosition = imagePosition {
            switch imagePosition {
            case .left:
                attText = NSMutableAttributedString.getImageSting(image: image)
                attText.append(text.attributedString(font: font, color: color))
            case .right:
                attText = text.attributedString(font: font, color: color)
                attText.append(NSMutableAttributedString.getImageSting(image: image))
            }
        }
        if strocked {
            view.becomeStrocked(with: white)
        }
        label.attributedText = attText
        label.setWidth(attText.getWidth(withConstrainedHeight: label.bounds.height))
        label.sizeToFit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func configure(pressedClosure: (() -> ())?) {
        setTap(pressedClosure: pressedClosure)
    }
    
    private func setTap(pressedClosure: (() -> ())?) {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapped))
        addGestureRecognizer(tap)
        self.pressedClosure = pressedClosure
    }
    
    @objc private func tapped(sender: UITapGestureRecognizer) {
        pressedClosure?()
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        alpha = ALPHA_60
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        alpha = SIMPLE_APLHA
        pressedClosure?()
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        alpha = SIMPLE_APLHA
    }
}

extension Button {
    
    static var back: Button {
        return Button(text: " Назад", image: UIImage(named: "backWhite"), imagePosition: .left, strocked: true)
    }
    
    static var menu: Button {
        return Button(image: UIImage(named: "nooMenu"))
    }
    
    static var settings: Button {
        return Button(image: UIImage(named: "nooSettings"))
    }
    
    static var cancel: Button {
        return Button(text: "Отменить", strocked: true)
    }
    
    static var nextButton: Button {
        return Button(text: "Далее ", image: UIImage(named: "whiteContinue"), imagePosition: .right, strocked: true)
    }
    
}

//
//  RequestManager.swift
//  newNL
//
//  Created by Глеб Сергей on 20.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

enum TaskType {
    case uploadTask
    case dataTask
    case binaryDownload
    case sendCommand
}

enum TaskState {
    case inProcess, free, logProcess
}

class TaskObject: NSObject {
    
    let taskType: TaskType
    let taskFile: Prefixes
    let data: Data?
    let handler: ((Result_<NLBaseObject, CommonError>) -> Void)
    
    init(type: TaskType, file: Prefixes, data: Data? = nil, completionHandler: @escaping ((Result_<NLBaseObject, CommonError>) -> ())) {
        self.taskType = type
        self.taskFile = file
        handler = completionHandler
        self.data = data
        super.init()
        debugInizializationMessage(someClass: self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
}

class RequestManager: NSObject {
    
    static let shared = RequestManager()
    
    private var currentTask: TaskType?
    private var turnForRequest = [TaskObject]()
    private var turnForSendCommand: [NLCommonCommand]?
    private var currentState = TaskState.free
    
    private var sharedSession = URLSession.shared
    
    private override init() {
        super.init()
        sharedSession = URLSession(configuration: .default,
                                   delegate: self,
                                   delegateQueue: OperationQueue())
        debugInizializationMessage(someClass: self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
    func checkTurn() {
        guard turnForRequest.count > 0, let request = turnForRequest.first else { return }
        requestTurnControl(taskType: request.taskType,
                           file: request.taskFile,
                           data: request.data,
                           isFromTurn: true,
                           completionHandler: request.handler)
    }
    
    func sendCommand(strCommand: String,
                     completionHandler: @escaping ((Result_<NLBaseObject, CommonError>) -> ())) {
        requestTurnControl(taskType: .sendCommand, file: .command, strCommand: strCommand, isFromTurn: false, completionHandler: completionHandler)
    }
    
    func requestTurnControl(taskType: TaskType,
                            file: Prefixes,
                            data: Data? = nil,
                            needShowProgress: Bool = false,
                            strCommand: String? = nil,
                            isFromTurn: Bool?,
                            completionHandler: @escaping ((Result_<NLBaseObject, CommonError>) -> ())) {
        guard taskType != .sendCommand else {
            dataRequest(requestModel: CommandRequestModel(commandString: strCommand),
                        expectedFile: .command,
                        taskType: taskType,
                        isFromTurn: isFromTurn ?? false,
                        needShowProgress: needShowProgress,
                        completionHandler: completionHandler)
            return
        }
        debugPrintText(text: "\(file) come here" )
        switch currentState {
        case .free:
            currentState = .inProcess
            switch taskType {
            case .dataTask:
                dataRequest(requestModel: configurateRequest(file: file,
                                                          taskType: taskType),
                         expectedFile: file,
                         taskType: taskType,
                         isFromTurn: isFromTurn ?? false,
                         needShowProgress: needShowProgress,
                         completionHandler: completionHandler)
            case .uploadTask:
                guard let data = data else {
                    completionHandler(Result_(value: nil, error: DataError(error: .noData)))
                    currentState = .free
                    checkTurn()
                    return
                }
                upload(requestModel: configurateRequest(file: file,
                                                        taskType: taskType),
                       file: file,
                       data: data,
                       isFromTurn: isFromTurn ?? false,
                       completionHandler: completionHandler)
            case .binaryDownload:
                startBinaryDownload()
                return
            default:
                return
            }
        case .inProcess:
            guard !turnForRequest.contains(where: {
                $0.taskFile == file && $0.taskType == taskType
            }) else {
                completionHandler(Result_(value: nil, error: RequestError(error: .alreadyInTurn)))
                return
            }
            turnForRequest.append(TaskObject(type: taskType, file: file, data: data, completionHandler: completionHandler))
            debugPrintText(text: "Now turn haves \(turnForRequest.count), and includes \(turnForRequest)")
        case .logProcess:
            debugPrintText(text: "log")
            // TODO:
        }
    }
    
    private func dataRequest<T>(requestModel: T, expectedFile: Prefixes, taskType: TaskType, isFromTurn: Bool, needShowProgress: Bool, completionHandler: @escaping ((Result_<NLBaseObject, CommonError>) -> ())) where T: BaseRequestModel {
        
        guard let strUrl = requestModel.url, let url = URL(string: strUrl), let method = requestModel.method else {
            completionHandler(Result_(value: nil, error: ApiError(error: .incorrectUrl)))
            return
        }
//        var urlParams: String = String.empty
//        if (requestModel is CommandRequestModel) {
//            urlParams = (requestModel as! CommandRequestModel).stringParams
//            let fullParamsStr = "send.htm?sd=" + urlParams + "&rnd=\(Int.random(in: 0...1000))"
//            url.appendPathComponent(fullParamsStr)
//        }
        var request = URLRequest(url: url)
        request.httpMethod = method
        request.timeoutInterval = 5
        do {
            let data = try JSONSerialization.data(withJSONObject: requestModel.parameters ?? [String: Any](), options: .prettyPrinted)
            request.httpBody?.append(data)
            if let authorization = PreferencesService.shared.getCurrentAccount()?.getAuthorizationString() {
                request.setValue("Basic \(authorization)", forHTTPHeaderField: "Authorization")
            }
            if needShowProgress {
                BaseViewController.showLoadingViewOnCurrentScreen(show: true)
            }
            let task = sharedSession.dataTask(with: request) { (data, response, error) in
                Thread.performOnMain({
                    BaseViewController.showLoadingViewOnCurrentScreen(show: false)
                    guard error == nil && data != nil && expectedFile.expectedCount() == data?.count else {
                        // temp
                        if !isFromTurn, taskType != .sendCommand {
                            let taskObj = TaskObject(type: taskType, file: expectedFile, data: data, completionHandler: completionHandler)
                            self.turnForRequest.insert(taskObj, at: 0)
                        }
                        self.currentState = .free
                        self.checkTurn()
                        completionHandler(Result_(value: nil, error: ApiError(error: .occuredAnErrorTryAgain)))
                        return
                    }
                    if let data = data {
                        StoreFileManager.shared.save(data: data)
                        switch expectedFile {
                        case .stateHtm:
                            guard let dataPrefix = expectedFile.getStringPrefix().data(using: .windowsCP1251) else {
                                completionHandler(Result_(value: nil, error: ApiError(error: .noData)))
                                return
                            }
                            Parser.shared.parse(data: dataPrefix + data, prefix: expectedFile, complitionHandler: { (result) in
                                completionHandler(result)
                            })
                        case .logBin:
                            guard let dataPrefix = expectedFile.getStringPrefix().data(using: .windowsCP1251) else {
                                completionHandler(Result_(value: nil, error: ApiError(error: .noData)))
                                return
                            }
                            Parser.shared.parse(data: dataPrefix + data, prefix: expectedFile, complitionHandler: { (result) in
                                completionHandler(result)
                            })
                        default :
                            Parser.shared.parse(data: data, prefix: expectedFile, complitionHandler: { (result) in
                                completionHandler(result)
                            })
                        }
                    }
                    guard (requestModel is CommandRequestModel) == false else {
                        return
                    }
                    if isFromTurn {
                        self.turnForRequest.removeFirst()
                    }
                    self.currentState = .free
                    self.checkTurn()
                })
            }
            task.resume()
        } catch {
            completionHandler(Result_(value: nil, error: ApiError(error: .canNotConvertObjectToData)))
        }
    }
    
    private func startBinaryDownload() {
        
    }
    
    private func upload<T>(requestModel: T, file: Prefixes, data: Data, isFromTurn: Bool, completionHandler: @escaping ((Result_<NLBaseObject, CommonError>) -> ())) where T: BaseRequestModel {
        guard let strUrl = requestModel.url, let url = URL(string: strUrl), let method = requestModel.method else {
            completionHandler(Result_(value: nil, error: ApiError(error: .incorrectUrl)))
            return
        }
        BaseViewController.showLoadingViewOnCurrentScreen(show: true, description: "Сохранение")
        var request = URLRequest(url: url)
        request.httpMethod = method
        request.timeoutInterval = 10
        if let authorization = PreferencesService.shared.getCurrentAccount()?.getAuthorizationString() {
            request.setValue("Basic \(authorization)", forHTTPHeaderField: "Authorization")
        }
        
        var dataForm = Data()
        dataForm.append("\r\n--\("****************")\r\n".data(using: .utf8)!)
        dataForm.append("Content-Disposition: form-data; name=\"param\(Int.random(in: 0...50)))\";\r\n\r\n".data(using: .utf8)!)
        dataForm.append(data)
        dataForm.append("\r\n\r\n\r\n".data(using: .utf8)!)
        request.httpBody = dataForm
        
        let task = sharedSession.dataTask(with: request) { (responseData, response, error) in
            Thread.performOnMain({
                BaseViewController.showLoadingViewOnCurrentScreen(show: false)
                guard error == nil && responseData != nil else {
                    if !isFromTurn {
                        let taskObj = TaskObject(type: .dataTask, file: file, data: data, completionHandler: completionHandler)
                        self.turnForRequest.insert(taskObj, at: 0)
                    }
                    self.currentState = .free
                    self.checkTurn()
                    completionHandler(Result_(value: nil, error: ApiError(error: .occuredAnErrorTryAgain)))
                    return
                }
                
                Parser.shared.parse(data: responseData!, prefix: .command, complitionHandler: completionHandler)
                
                completionHandler(Result_(value: CommandResponse.success(), error: nil))
                if isFromTurn {
                    self.turnForRequest.removeFirst()
                }
                self.currentState = .free
                self.checkTurn()
            })
        }
        task.resume()
    }
    
    private func configurateRequest(file: Prefixes, taskType: TaskType) -> BaseRequestModel {
        switch taskType {
        case .dataTask: return BaseRequestModel(file: file)
        case .uploadTask:
            let req = BaseRequestModel(isUpload: true)
            return req
        case .binaryDownload: return BaseRequestModel(file: file)
        default: return BaseRequestModel(file: .unknown)
        }
    }

}

extension RequestManager: URLSessionDelegate, URLSessionTaskDelegate {
    
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
    }

    func urlSession(_ session: URLSession, dataTask: URLSessionDataTask, didReceive data: Data) {}
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didCompleteWithError error: Error?) {
        // todo + checkTUrn
    }
    
}

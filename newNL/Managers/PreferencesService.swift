//
//  PreferencesService.swift
//  newNL
//
//  Created by aggrroo on 21.10.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation
import UIKit

protocol PreferencesServiceProtocol {}

class PreferencesService: PreferencesServiceProtocol {
    
    static let shared = PreferencesService()
    
    let defaultTheme = Theme(navigationColor: .black,
                             mainScreenColor: .white,
                             textColor: .black,
                             inactiveColor: .lightGray)
    
    var accountList: NLAccountList?
    var currentAccount: NLAccount?
    var currentTheme: Theme?
    
    private init() {
        debugDeinitializationMessage(someClass: self)
        setupPreferences()
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
    private func setupPreferences() {
        accountList = getAccountList()
        currentAccount = getCurrentAccount()
    }
    
    private func setupTheme() -> Theme {
        guard let themas = StoreFileManager.shared.fetchThemas() else { return defaultTheme }
        return getCurrentTheme(themas: themas)
    }
    
    private func getCurrentTheme(themas: [Theme]) -> Theme {
        guard let thema = themas.first(where: {
            $0.isCurrent != nil && $0.isCurrent!
        }) else {
            return defaultTheme
        }
        return thema
    }
    
    func getAccountList() -> NLAccountList? {
        return accountList ?? StoreFileManager.shared.fetchAccountList()
    }
    
    func saveAccountList(completionHandler: ((CommonError) -> ())) {
        StoreFileManager.shared.saveAccountList(accounts: accountList, complitionHandler: completionHandler)
    }
    
    func getCurrentAccount() -> NLAccount? {
        guard currentAccount == nil else { return currentAccount }
        guard let accounts = getAccountList()?.accounts else { return nil }
        for account in accounts {
            guard let isCurrent = account.isCurrent, isCurrent else { continue }
            currentAccount = account
        }
        return currentAccount
    }
    
}

class Theme: NSObject {
    
    var navigationColor = UIColor()
    var mainScreenColor = UIColor()
    var textColor = UIColor()
    var inactiveColor = UIColor()
    var isCurrent: Bool?
    
    init(navigationColor: UIColor, mainScreenColor: UIColor, textColor: UIColor, inactiveColor: UIColor) {
        super.init()
        self.navigationColor = navigationColor
        self.mainScreenColor = mainScreenColor
        self.textColor = textColor
        self.inactiveColor = inactiveColor
    }
    
    deinit {}
}



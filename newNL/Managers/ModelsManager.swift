//
//  ModelsManager.swift
//  newNL
//
//  Created by Глеб Сергей on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

protocol ModelManagerProtocol {}

protocol DevicesReceiveDelegate: class {
    
    func didReceiveDevices(devices: [NLBaseDevice]?)
    func didReceiveScenarious(scenarious: [NLScenario]?)
    func didUpdateStates()
    
}

protocol HouseInfoReceiveDelegate: class {
    
    func didReceiveHouseInfo(house: NLHouse?, rooms: [NLRoom]?)

}

protocol TimerReceiveDelegate: class {
    
    func didReceiveTimers(timers: [NLTimer]?)
    
}

protocol AutoReceiveDelegate: class {
    
    func didReceiveAutos(autos: [NLAuto]?)
    
}

class ModelsManager: ModelManagerProtocol {
    
    static let shared = ModelsManager()
    private let fileManager: StoreFileManager
    private let apiManager: ApiManager
    private let requestManager: RequestManager
    private let preferences: PreferencesService
    private let parser: Parser
    
    private init() {
        fileManager = StoreFileManager.shared
        apiManager = ApiManager.shared
        requestManager = RequestManager.shared
        preferences = PreferencesService.shared
        parser = Parser.shared
        debugInizializationMessage(someClass: self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
    private var house: NLHouse?
    private var rooms: [NLRoom]?
    private var devices: NLBaseDevicesList?
    private var timers: NLTimerList?
    private var scenarious: NLScenarioList?
    private var autos: NLAutoList?
    
    weak var devicesDelegate: DevicesReceiveDelegate?
    weak var houseInfoDelegate: HouseInfoReceiveDelegate?
    weak var timersDelegate: TimerReceiveDelegate?
    weak var autosDelegate: AutoReceiveDelegate?
    
    func removeDevices() {
        self.devices = nil;
    }
    func removeAutos() {
        self.autos = nil
    }
    func removeTimers() {
        self.timers = nil
    }
    
    func reorderCurrentAccount(newCurrentId: String) {
        guard let accounts = PreferencesService.shared.getAccountList()?.accounts else { return }
        for account in accounts {
            guard account.uniqueId == newCurrentId else {
                account.isCurrent = false
                continue
            }
            account.isCurrent = true
        }
    }
    
    func logIn(finish: (() -> ())) {
        guard fileManager.fetchAccountList() == nil else {
            finish()
            return
        }
        let acc = NLAccount.init(adress: Adress(ip: "192.168.0.170"))
        fileManager.saveAccountList(accounts: NLAccountList(accounts: [acc])) { (err) in
            finish()
        }
    }
    
    func addAuto(_ auto: NLAuto) {
        if self.autos == nil {
            let list = [NLAuto]()
            self.autos = NLAutoList(autos: list)
        }
        self.autos?.autos?.append(auto)
    }
    
    func setScenarious(scenarious: NLScenarioList) {
        self.scenarious = scenarious
    }
    
    func setDevices(devices: NLBaseDevicesList) {
        self.devices = devices
    }
    
    func setHouse(house: NLHouse) {
        self.house = house
    }
    
    func setRooms(rooms: [NLRoom]) {
        self.rooms = rooms
    }
    
    func getDevices() -> [NLBaseDevice]? {
        return devices?.devices
    }
    
    func getRooms() -> [NLRoom]? {
        return rooms
    }
    
    func getAutos() -> [NLAuto]? {
        return autos?.autos
    }
    
    func getHouse() -> NLHouse? {
        return house
    }
    
    func getTimers() -> [NLTimer]? {
        return timers?.timers
    }
    
    func getScenarious() -> [NLScenario]? {
        return scenarious?.scenarious
    }
    
    func fetchDevicesInfo() {
        preferences.currentAccount?.isActual(complitionHandler: { [weak self] (actual) in
            guard self?.devices == nil, let actual = actual, !actual else {
                self?.devicesDelegate?.didReceiveDevices(devices: self?.devices?.devices)
                self?.houseInfoDelegate?.didReceiveHouseInfo(house: self?.house, rooms: self?.rooms)
                return
            }
            guard self?.fileManager.fetch(prefix: .deviceBin) == nil, self?.fileManager.fetch(prefix: .userBin) == nil else {
                guard let data = self?.fileManager.fetch(prefix: .deviceBin) else { return }
                self?.parser.parse(data: data, prefix: .deviceBin) { (result) in
                    self?.devices = result.value as? NLBaseDevicesList
                    guard let data = self?.fileManager.fetch(prefix: .userBin) else { return }
                    self?.parser.parse(data: data, prefix: .userBin, complitionHandler: { (result) in
                        self?.devices = result.value as? NLBaseDevicesList
                        self?.devicesDelegate?.didReceiveDevices(devices: self?.devices?.devices)
                        self?.houseInfoDelegate?.didReceiveHouseInfo(house: self?.house, rooms: self?.rooms)
                    })
                }
                return
            }
            guard !actual else { return }
            self?.requestManager.requestTurnControl(taskType: .dataTask, file: .deviceBin, isFromTurn: false) { (result) in
                guard let devices = result.value as? NLBaseDevicesList else { return }
                self?.devices = devices
            }
            self?.requestManager.requestTurnControl(taskType: .dataTask, file: .userBin, isFromTurn: false) { (result) in
                guard let devices = result.value as? NLBaseDevicesList else { return }
                self?.devices = devices
                self?.devicesDelegate?.didReceiveDevices(devices: devices.devices)
                self?.houseInfoDelegate?.didReceiveHouseInfo(house: self?.house, rooms: self?.rooms)
            }
        })
    }
    
    func fetchHouseInfo() {
        preferences.currentAccount?.isActual(complitionHandler: { [weak self] (actual) in
            guard self?.rooms == nil, let actual = actual, !actual else {
                self?.houseInfoDelegate?.didReceiveHouseInfo(house: self?.house, rooms: self?.rooms)
                return
            }
            guard self?.fileManager.fetch(prefix: .userBin) == nil else {
                guard let data = self?.fileManager.fetch(prefix: .userBin) else { return }
                self?.parser.parse(data: data, prefix: .userBin) { (result) in
                    self?.houseInfoDelegate?.didReceiveHouseInfo(house: self?.house, rooms: self?.rooms)
                }
                return
            }
            guard !actual else { return }
            self?.requestManager.requestTurnControl(taskType: .dataTask, file: .userBin, isFromTurn: false) { (result) in
                self?.houseInfoDelegate?.didReceiveHouseInfo(house: self?.house, rooms: self?.rooms)
            }
        })
    }
    
    func fetchTimers() {
        preferences.currentAccount?.isActual(complitionHandler: { [weak self] (actual) in
            guard self?.timers == nil, let actual = actual, !actual else {
                self?.timersDelegate?.didReceiveTimers(timers: self?.timers?.timers)
                return
            }
            guard self?.fileManager.fetch(prefix: .timerBin) == nil else {
                guard let data = self?.fileManager.fetch(prefix: .timerBin) else { return }
                self?.parser.parse(data: data, prefix: .timerBin, complitionHandler: { (result) in
                    self?.timers = result.value as? NLTimerList
                    self?.timersDelegate?.didReceiveTimers(timers: self?.timers?.timers)
                })
                return
            }
            guard !actual else { return }
            self?.requestManager.requestTurnControl(taskType: .dataTask, file: .timerBin, isFromTurn: false) { (result) in
                self?.timers = result.value as? NLTimerList
                self?.timersDelegate?.didReceiveTimers(timers: self?.timers?.timers)
            }
        })
    }
    
    func fetchScenarious() {
        preferences.currentAccount?.isActual(complitionHandler: { [weak self] (actual) in
            guard self?.scenarious == nil, let actual = actual, !actual else {
                self?.devicesDelegate?.didReceiveScenarious(scenarious: self?.scenarious?.scenarious)
                return
            }
            guard self?.fileManager.fetch(prefix: .presetBin) == nil else {
                guard let data = self?.fileManager.fetch(prefix: .presetBin) else { return }
                self?.parser.parse(data: data, prefix: .presetBin, complitionHandler: { (result) in
                    self?.scenarious = result.value as? NLScenarioList
                    self?.devicesDelegate?.didReceiveScenarious(scenarious: self?.scenarious?.scenarious)
                })
                return
            }
            guard !actual else { return }
            self?.requestManager.requestTurnControl(taskType: .dataTask, file: .presetBin, isFromTurn: false) { (result) in
                self?.scenarious = result.value as? NLScenarioList
                self?.devicesDelegate?.didReceiveScenarious(scenarious: self?.scenarious?.scenarious)
            }
        })
    }
    
    func fetchAutos() {
        preferences.currentAccount?.isActual(complitionHandler: { [weak self] (actual) in
            guard self?.autos == nil, let actual = actual, !actual else {
                self?.autosDelegate?.didReceiveAutos(autos: self?.autos?.autos)
                return
            }
            guard self?.fileManager.fetch(prefix: .autoBin) == nil else {
                guard let data = self?.fileManager.fetch(prefix: .autoBin) else { return }
                self?.parser.parse(data: data, prefix: .autoBin, complitionHandler: { (result) in
                    self?.autos = result.value as? NLAutoList
                    self?.autosDelegate?.didReceiveAutos(autos: self?.autos?.autos)
                })
                return
            }
            guard !actual else { return }
            self?.requestManager.requestTurnControl(taskType: .dataTask, file: .autoBin, isFromTurn: false) { (result) in
                self?.autos = result.value as? NLAutoList
                self?.autosDelegate?.didReceiveAutos(autos: self?.autos?.autos)            }
        })
    }
    
    func updateStates() {
        requestManager.requestTurnControl(taskType: .dataTask, file: .stateHtm, isFromTurn: false) { [weak self] (result) in
            guard result.value != nil, let states = (result.value as? NLStateObject)?.devicesInfoList else { return }
            print("")
            let ftxs = self?.devices?.devices?.filter { $0.deviceType == .ftx } as? [NLFTXDevice]
            if let wrFtxs = ftxs {
                for dev in wrFtxs {
                    if let bitsState = states[dev.chanel]?.state {
                        dev.currentState = DeviceState.getFromBits(bits: bitsState)
                    }
                    dev.setts = states[dev.chanel]?.sets
                }
            }
            self?.devicesDelegate?.didUpdateStates()
        }
    }
    
}

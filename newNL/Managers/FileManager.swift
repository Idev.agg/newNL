//
//  FileManager.swift
//  newNL
//
//  Created by aggrroo on 21.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation
import CoreData

protocol FileManagerProtocol {}

class StoreFileManager: FileManagerProtocol {
    
    static let shared = StoreFileManager()
    
    private let fileManager = FileManager.default
    private let userDefaults = UserDefaults.standard
    private let appDelegate = AppDelegate.appDelegate()
    private let context = AppDelegate.appDelegate().persistentContainer.viewContext
    private var account: NLAccount?
    
    private static let ENTITY = "PRF64Files"
    private static let ACC_KEY = "account"
    
    private init() {
        debugInizializationMessage(someClass: self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
    private var deviceBinData: Data?
    private var userBinData: Data?
    private var presetBinData: Data?
    private var timerBinData: Data?
    private var autoBinData: Data?
    private var settingsBinData: Data?
    private var stateHtmData: Data?
    private var logBinData: Data?
    
    func getUserBinData() -> Data? {
        return userBinData
    }
    
    func getAutoBinData() -> Data? {
        return autoBinData
    }
    
    func getTimerBinData() -> Data? {
        return timerBinData
    }
    
    func initiate() {
        if let acc = PreferencesService.shared.currentAccount ?? PreferencesService.shared.getCurrentAccount() {
            account = acc
        } else {
            account = NLAccount(adress: Adress(ip: "134.17.24.191"))
            saveAccountList(accounts: NLAccountList(accounts: [account!])) { (error) in
                debugPrintText(text: "account created")
            }
        }
    }
    
    func store(data: Data) {
        guard data.count > 10 else { return }
        switch data.getPrefix() {
        case .userBin:
            userBinData = data
        case .deviceBin:
            deviceBinData = data
        case .timerBin:
            timerBinData = data
        case .presetBin:
            presetBinData = data
        case .autoBin:
            autoBinData = data
        case .settingsBin:
            settingsBinData = data
        case .stateHtm:
            stateHtmData = data
        case .logBin:
            logBinData = data
        case .unknown:
            return
        case .command:
            return
        case .bindHtm:
            return
        case .rxsetHtm:
            return
        }
    }
    
    func save(data: Data) {
        store(data: data)
        return
        guard let object = load() else {
            createEntity()
            return
        }
       object.setValue(data, forKey: data.getPrefix().getStr())
    }
    
    private func saveData() {
        do {
            try context.save()
        } catch {
        }
    }
    
    private func createEntity() {
        return
        let entity = NSEntityDescription.entity(forEntityName: StoreFileManager.ENTITY, in: context)
        let newUser = PRF64File(entity: entity!, insertInto: context) as! PRF64File
        newUser.setValue(PreferencesService.shared.currentAccount?.uniqueId, forKey: StoreFileManager.ACC_KEY)
        saveData()
    }
    
    private func load() -> NSManagedObject? {
        return nil
        let account = PreferencesService.shared.currentAccount ?? PreferencesService.shared.getCurrentAccount()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: StoreFileManager.ENTITY)
        do {
            let results = try context.fetch(request) as? [NSManagedObject]
            guard let res = results else { return nil}
            return res.first(where: {
                $0.value(forKey: StoreFileManager.ACC_KEY) as? String == account?.uniqueId
            })
        } catch {
            return nil
        }
    }
    
    func fetch(prefix: Prefixes) -> Data? {
        return nil
        guard let object = load() else { return nil }
        return object.value(forKey: prefix.getStr()) as? Data
    }
    
//    func fetchData(prefix: Prefixes) -> (data: Data?, error: CommonError) {
//        let data: Data? = {
//            switch prefix {
//            case .deviceBin:     return deviceBinData
//            case .userBin:       return userBinData
//            case .presetBin:     return presetBinData
//            case .timerBin:      return timerBinData
//            case .autoBin:       return autoBinData
//            case .settingsBin:   return settingsBinData
//            case .unknown:       return nil
//            case .stateHtm:      return stateHtmData
//            case .logBin:        return logBinData
//            }
//        }()
//        guard data == nil else {
//            return (data, StorageError(error: .withoutError))
//        }
//        guard let account = PreferencesService.shared.getCurrentAccount(), let uniqueId = account.uniqueId else {
//            return (nil, StorageError(error: .noSuchFile))
//        }
//        let fileName = uniqueId + prefix.getStringPrefix()
//        if let dir = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
//            let fileURL = dir.appendingPathComponent(fileName)
//
////            if fileManager.fileExis {
////                fileManager.removeItem(at: fileURL)
////            }
//            // Reading
//            do {
//                let data = try Data(contentsOf: fileURL)
//                return (data, StorageError(error: .withoutError))
//            }
//            catch {
//                return (nil, StorageError(error: .unknownDataStorageError))
//            }
//        } else {
//            return (nil, StorageError(error: .noSuchFile))
//        }
//    }
//
//    func saveData(data: Data, complitionHandler: ((CommonError) -> ())?) {
//        guard let account = PreferencesService.shared.getCurrentAccount(), let uniqueId = account.uniqueId else {
//            complitionHandler?(StorageError(error: .noSuchFile))
//            return
//        }
//        let fileName = uniqueId + data.getPrefix().getStringPrefix()
//        let file = data
//        if let dir = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
//            let fileURL = dir.appendingPathComponent(fileName)
//            // Saving
//            do {
//                try file.write(to: fileURL, options: .atomicWrite)
//                LogManager.shared.printText(text: "\(file.count) bytes was writen to Path: \(fileURL)")
//                complitionHandler?(StorageError(error: .withoutError))
//            }
//            catch {
//                complitionHandler?(StorageError(error: .unknownDataStorageError))
//            }
//        } else {
//            complitionHandler?(StorageError(error: .noSuchFile))
//        }
//    }
//
//    func clearData(completeon: @escaping ((Bool) -> ())) {
//        let group = DispatchGroup()
//        group.enter()
//        for prefix in Prefixes.editable {
//            removeFile(prefix: prefix) { (error) in
//                guard error.isSuccessful() else {
//                    completeon(false)
//                    return
//                }
//                group.leave()
//            }
//        }
//        group.notify(queue: DispatchQueue.main) {
//            completeon(true)
//        }
//    }
//
//    func removeFile(prefix: Prefixes, complitionHandler: ((CommonError) -> ())?) {
//        guard let account = PreferencesService.shared.getCurrentAccount(), let uniqueId = account.uniqueId else {
//            complitionHandler?(StorageError(error: .noSuchFile))
//            return
//        }
//        let fileName = uniqueId + prefix.getStringPrefix()
//        if let dir = fileManager.urls(for: .documentDirectory, in: .userDomainMask).first {
//            let fileURL = dir.appendingPathComponent(fileName)
//            // Removing
//            do {
//                try fileManager.removeItem(at: fileURL)
//                LogManager.shared.printText(text: "\(prefix.getStringPrefix()) was removed from Path: \(fileURL)")
//                complitionHandler?(StorageError(error: .withoutError))
//            }
//            catch {
//                complitionHandler?(StorageError(error: .unknownDataStorageError))
//            }
//        } else {
//            complitionHandler?(StorageError(error: .noSuchFile))
//        }
//    }
    
    func saveAccountList(accounts: NLAccountList?, complitionHandler: ((CommonError) -> ())) {
        guard let accounts = accounts else {
            complitionHandler(StorageError(error: .noData))
            return
        }
        let data = NSKeyedArchiver.archivedData(withRootObject: accounts)
        userDefaults.set(data, forKey: NLAccountList.ACCOUNT_LIST_KEY_PATH)
        userDefaults.synchronize()
        complitionHandler(CommonError(error: .withoutError))
    }
    
    func fetchAccountList() -> NLAccountList? {
        guard let decodedObject = userDefaults.object(forKey: NLAccountList.ACCOUNT_LIST_KEY_PATH) as? Data else { return nil }
        return NSKeyedUnarchiver.unarchiveObject(with: decodedObject) as? NLAccountList
    }
    
    func fetchThemas() -> [Theme]? {
        //TODO:
        return [Theme]()
    }

}

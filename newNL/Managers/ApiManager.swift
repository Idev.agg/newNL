//
//  ApiManager.swift
//  newNL
//
//  Created by aggrroo on 23.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

protocol ApiProtocol: class {}

class ApiManager: ApiProtocol {
    
    static let shared = ApiManager()
    
    private let deviceBin = "device.bin"
    private let userBin = "user.bin"
    private let timerBin = "timer.bin"
    private let presetBin = "preset.bin"
    private let autoBin = "auto.bin"
    private let settingsBin = "settings.bin"
    private let stateHtm = "state.htm"
    private let logBin = "log.bin"
    private let rxsetHtm = "rxset.htm"
    private let bindHtm = "bind.htm"
    
    private init() {
        debugInizializationMessage(someClass: self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
    func getUrlForFile(prefix: Prefixes) -> String {
        switch prefix {
        case .userBin:
            return userBin
        case .deviceBin:
            return deviceBin
        case .timerBin:
            return timerBin
        case .presetBin:
            return presetBin
        case .autoBin:
            return autoBin
        case .settingsBin:
            return settingsBin
        case .stateHtm:
            return stateHtm
        case .logBin:
            return logBin
        case .unknown:
            return String.empty
        case .command:
            return String.empty
        case .bindHtm:
            return bindHtm
        case .rxsetHtm:
            return rxsetHtm
        }
    }
    
}

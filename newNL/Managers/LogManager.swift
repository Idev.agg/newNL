//
//  LogManager.swift
//  newNL
//
//  Created by aggrroo on 21.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

func debugPrintText(text: String) {
    #if DEBUG
    Swift.print("⚠️ " + text + " ⚠️")
    #endif
}

func debugInizializationMessage(someClass: Any) {
    #if DEBUG
    print("✳️ \(someClass) inizialized ✳️")
    #endif
}

func debugDeinitializationMessage(someClass: Any) {
    #if DEBUG
    print("❌ \(someClass) deinited ❌")
    #endif
}

func debugErrorMessage(error: CommonError) {
    #if DEBUG
    print("❗ There are an error now. \(error.code) - \(error.discription) ❗")
    #endif
}


//
//  Parser.swift
//  newNL
//
//  Created by aggrroo on 19.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import UIKit

protocol ParserProtocol: class {}

class Parser: ParserProtocol {
    
    static let PREFIX_PARSING_RANGE = Range(0...5)
    
    static let shared = Parser()
    
    private init() {
        debugInizializationMessage(someClass: self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
    func parse<T, Error>(data: Data, prefix: Prefixes, complitionHandler: ((Result_<T, Error>) -> ())) {
        
        switch prefix {
        case .userBin: complitionHandler(Result_(value: parseUserProperties(data: data), error: nil))
        case .deviceBin: complitionHandler(Result_(value: parseDevices(data: data), error: nil))
        case .timerBin: complitionHandler(Result_(value: parseTimers(data: data), error: nil))
        case .presetBin: complitionHandler(Result_(value: parseScenarious(data: data), error: nil))
        case .autoBin: complitionHandler(Result_(value: parseAutos(data: data), error: nil))
        case .settingsBin: complitionHandler(Result_(value: parseSettings(data: data), error: nil))
        case .stateHtm: complitionHandler(Result_(value: parseStates(data: data), error: nil))
        case .logBin: complitionHandler(Result_(value: parseLogs(data: data), error: nil))
        case .unknown: complitionHandler(Result_(value: nil, error: ParsingError(error: .unknownData)))
        case .command: complitionHandler(Result_(value: CommandResponse(data: data), error: nil))
        case .bindHtm: complitionHandler(Result_(value: BindModel(data: data), error: nil))
        case .rxsetHtm: complitionHandler(Result_(value: parseRxSet(data: data), error: nil))
        }
    }
    
    private func parseRxSet(data: Data) -> RxSetModel? {
        return RxSetModel(name: nil, type: .rxSetObj)
    }
    
    private func parseDevices(data: Data) -> NLBaseDevicesList? {
        var devices = [NLBaseDevice]()
        for chanel in 0..<NLBaseDevice.MAX_DEVICES_COUNT {
            let currentTXStartByte = NLBaseDevice.TX_START_BYTE + chanel * NLBaseDevice.DEVICE_DESCRIBE_LENGHT
            if data[currentTXStartByte] != Data.emptyByte {
                devices.append(parseDevicesBySubTypes(device: (NLTXDevice(settings: data[currentTXStartByte + NLTXDevice.SETTINGS_PARSING_INTERVAL.lowerBound...currentTXStartByte + NLTXDevice.SETTINGS_PARSING_INTERVAL.upperBound],
                                          chanel: chanel,
                                          deviceType: .tx,
                                          subType: data[currentTXStartByte + NLBaseDevice.SUBTYPE_BYTE].getSubTypeFor(type: .tx)))))
            }
            let currentRXStartByte = NLBaseDevice.RX_START_BYTE + chanel * NLBaseDevice.DEVICE_DESCRIBE_LENGHT
            if data[currentRXStartByte] != Data.emptyByte {
                devices.append(parseDevicesBySubTypes(device: (NLRXDevice(chanel: chanel,
                                          deviceType: .rx,
                                          subType: data[currentRXStartByte + NLBaseDevice.SUBTYPE_BYTE].getSubTypeFor(type: .rx)))))
            }
            let currentFTXStartByte = NLBaseDevice.FTX_START_BYTE + chanel * NLBaseDevice.DEVICE_DESCRIBE_LENGHT
            if data[currentFTXStartByte] != Data.emptyByte {
                devices.append(parseDevicesBySubTypes(device: (NLFTXDevice(id: data[currentFTXStartByte + NLFTXDevice.ID_PARSING_INTERVAL.lowerBound..<currentFTXStartByte + NLFTXDevice.ID_PARSING_INTERVAL.upperBound].map{ String(format:"%02x", $00) }.joined().uppercased(),
                                           chanel: chanel,
                                           deviceType: .ftx,
                                           subType: data[currentFTXStartByte + NLBaseDevice.SUBTYPE_BYTE].getSubTypeFor(type: .ftx)))))
            }
        }
        ModelsManager.shared.setDevices(devices: NLBaseDevicesList(devices: devices))
        guard devices.count > 0 else { return nil }
        return NLBaseDevicesList(devices: devices)
    }
    
    private func parseUserProperties(data: Data) -> NLBaseDevicesList? {
        let name: String = {
            guard let name = data[NLHouse.FIRST_BYTE..<NLHouse.FINISHED_BYTE].getString(start: NLHouse.NAME_PARSING_INTERVAL.lowerBound,
                                            lastReadable: NLHouse.NAME_PARSING_INTERVAL.upperBound - 1) else {
                return "Дом"
            }
            return name
        }()
        ModelsManager.shared.setRooms(rooms: parseRooms(data: data))
        ModelsManager.shared.setHouse(house: NLHouse(name: name))
        guard let devices = ModelsManager.shared.getDevices() else {
            return nil
        }
        guard let scenarious = ModelsManager.shared.getScenarious() else {
            return NLBaseDevicesList(devices: parseUserPropertiesForDevices(devices: devices, data: data))
        }
        ModelsManager.shared.setScenarious(scenarious: parseUserPropertiesForScenarious(scenarious: scenarious, data: data))
        return NLBaseDevicesList(devices: parseUserPropertiesForDevices(devices: devices, data: data))
    }
    
    private func parseRooms(data: Data) -> [NLRoom] {
        var rooms = [NLRoom]()
        rooms.append(NLRoom(name: NLRoom.UNDISTRIBUTED_DEVICES_ROOM_NAME,
                            number: NLRoom.UNDISTRIBUTED_DEVICES_ROOM_NUMBER))
        for i in 0..<NLRoom.MAX_ROOM_COUNT {
            let startByte = NLRoom.FIRST_BYTE + i * NLRoom.ROOM_DESCRIBE_BYTES_COUNT
            guard data[startByte + NLRoom.FINISHED_NAME_BYTE] != Data.emptyByte, let name = data[startByte..<NLRoom.FIRST_BYTE + (i + 1) * NLRoom.ROOM_DESCRIBE_BYTES_COUNT].getString(start: NLRoom.NAME_PARSING_INTERVAL.lowerBound, lastReadable: NLRoom.NAME_PARSING_INTERVAL.upperBound) else { continue }
            rooms.append(NLRoom(name: name, number: i, iconIndex: Int(data[startByte + NLRoom.ICON_BYTE])))
        }
        return rooms
    }
    
    private func parseUserPropertiesForDevices(devices: [NLBaseDevice], data: Data) -> [NLBaseDevice] {
        for device in devices {
            guard let startUserPropertiesByte = getStartUserPropertiesByte(device: device) else { continue }
            device.addProperties(name: data.subdata(in: startUserPropertiesByte..<startUserPropertiesByte + NLBaseDevice.NAME_PARSING_INTERVAL.upperBound).getString(start: NLBaseDevice.NAME_PARSING_INTERVAL.lowerBound, lastReadable: NLBaseDevice.NAME_PARSING_INTERVAL.upperBound - 1), iconIndex: Int(data[startUserPropertiesByte + NLBaseDevice.ICON_INDEX_BYTE]), attachedRoomNumber: Int(data[startUserPropertiesByte + NLBaseDevice.ATTACHED_ROOM_NUMBER_BYTE]))
        }
        return devices
    }
    
    private func parseUserPropertiesForScenarious(scenarious: [NLScenario], data: Data) -> NLScenarioList {
        for scenario in scenarious {
            let startPropertiesByte = NLScenario.FIRST_USER_PROPERTIES_BYTE  + scenario.number * NLScenario.USER_PROPERTIES_LENGHT
            scenario.name = data.subdata(in: startPropertiesByte..<startPropertiesByte + NLTimer.NAME_STOP_BYTE).getString(start: NLScenario.NAME_RANGE.lowerBound, lastReadable: NLScenario.NAME_RANGE.upperBound)
            scenario.iconIndex = Int(data[startPropertiesByte + NLScenario.ICON])
        }
        return NLScenarioList(scenarious: scenarious)
    }
    
    private func getStartUserPropertiesByte(device: NLBaseDevice) -> Int? {
        switch device.deviceType {
        case .tx:
            return NLBaseDevice.TX_START_USER_PROPERTIES + device.chanel * NLBaseDevice.DEVICE_DESCRIBE_LENGHT_USER_PROPERTIES
        case .rx:
            return NLBaseDevice.RX_START_USER_PROPERTIES + device.chanel * NLBaseDevice.DEVICE_DESCRIBE_LENGHT_USER_PROPERTIES
        case .ftx:
            return NLBaseDevice.FTX_START_USER_PROPERTIES + device.chanel * NLBaseDevice.DEVICE_DESCRIBE_LENGHT_USER_PROPERTIES
        case .frx:
            return nil
        case .unknown:
            return nil
        }
    }
    
    private func parseTimers(data: Data) -> NLTimerList {
        var timers = [NLTimer]()
        for i in 0..<NLTimer.MAX_TIMERS_COUNT {
            let firstByte = NLTimer.FIRST_SETTINGS_BYTE + i * NLTimer.SETTINGS_LENGHT
            guard data[firstByte] != Data.emptyByte else { continue }
            let commands: [NLCommonCommand] = {
                var commands = [NLCommonCommand]()
                for i in 0..<NLTimer.COMMANDS_COUNT {
                    let start = firstByte + NLTimer.FIRST_COMMAND_SETTINGS_BYTE + i * NLCommonCommand.lENGHT
                    let finish = start + NLCommonCommand.lENGHT
                    guard data[start] != Data.emptyByte else { return commands }
                    commands.append(parseCommand(data: data.subdata(in: start..<finish)))
                }
                return commands
            }()
            guard commands.count > 0 else { continue }
            let nameStart = NLTimer.FIRST_NAME_BYTE + NLTimer.NAME_DESCRIBE_LENGHT * i
            let nameFinish = nameStart + NLTimer.NAME_DESCRIBE_LENGHT
            guard let mode = commands.first?.mode else { continue }
            timers.append(NLTimer(name: data[nameStart..<nameFinish].getString(start: NLTimer.NAME_DESCRIBE_RANGE.lowerBound,
                                                                               lastReadable: NLTimer.NAME_DESCRIBE_RANGE.upperBound) ?? String.empty,
                                  timerType: TimerTypeConverter().toType(mode: UInt8(mode), type: data[firstByte + NLTimer.TYPE],
                                                                         command: commands.first?.command ?? .unknown),
                                  commands: commands,
                                  isActive: data[firstByte + NLTimer.STATE].toBool(),
                                  onTime: NLTimeObject(hours: Int(data[firstByte + NLTimer.ON_TIME_H]),
                                                       minutes: Int(data[firstByte + NLTimer.ON_TIME_M])),
                                  offTime: NLTimeObject(hours: Int(data[firstByte + NLTimer.OFF_TIME_H]),
                                                        minutes: Int(data[firstByte + NLTimer.OFF_TIME_M])),
                                  calendar: NLCalendar(bits: data[firstByte + NLTimer.DAYS].toBits(count: Day.BITS_COUNT)),
                                  number: i))
        }
        
        return NLTimerList(timers: timers)
    }
    
    private func parseScenarious(data: Data) -> NLScenarioList {
        var scenarious = [NLScenario]()
        for i in 0..<NLScenario.MAX_SCENARIOUS_COUNT {
            let firstByte = NLScenario.FIRST_BYTE + i * NLScenario.SCENARIO_LENGHT
            guard data[firstByte] != Data.emptyByte else { continue }
            let commands: [NLCommonCommand] = {
                var commands = [NLCommonCommand]()
                for i in 0..<NLScenario.MAX_COMMANDS_COUNT {
                    let start = firstByte + i * NLCommonCommand.lENGHT
                    let finish = start + NLCommonCommand.lENGHT
                    guard data[start] != Data.emptyByte else {
                        return commands
                    }
                    commands.append(parseCommand(data: data.subdata(in: start..<finish)))
                }
                return commands
            }()
            guard commands.count > 0 else { continue }
            scenarious.append(NLScenario(number: i, commands: commands))
        }
        guard let userData = StoreFileManager.shared.getUserBinData() else {
            return NLScenarioList(scenarious: scenarious)
        }
        return parseUserPropertiesForScenarious(scenarious: scenarious, data: userData)
    }
    
    private func parseDevicesBySubTypes(device: NLBaseDevice) -> NLBaseDevice {
        switch device.subType {
        case .temperatureSensor:
            return TemperatureSensor(chanel: device.chanel,
                                     deviceType: device.deviceType,
                                     subType: device.subType)
        case .temperatureAndHumiditySensor:
            return TemperatureAndHumiditySensor(chanel: device.chanel,
                                                deviceType: device.deviceType,
                                                subType: device.subType)
        case .motionSensor, .leakSensor, .lightSensor, .openingSensor:
            return Sensor(chanel: device.chanel,
                          deviceType: device.deviceType,
                          subType: device.subType)
        case .thermostat, .relayF, .dimmerF, .socket, .rollerBlinds:
            guard let wrappedDevice = device as? NLFTXDevice, wrappedDevice.subType == .thermostat else {
                return device
            }
            return Thermostat(id: wrappedDevice.id,
                              chanel: wrappedDevice.chanel,
                              deviceType: wrappedDevice.deviceType,
                              subType: wrappedDevice.subType)
        default:
            return device
        }
    }
    
    private func parseAutos(data: Data) -> NLAutoList {
        var autos = [NLAuto]()
        var data = data
        data.remove(at: NLAuto.DERIVED_BYTE)
        for i in 0..<NLAuto.MAX_AUTOS_COUNT {
            var startByte = NLAuto.FIRST_SETTINGS_BYTE + i * NLAuto.AUTO_LENGHT
            if i > 14 {
                startByte += 1
            }
            guard data[startByte] != Data.emptyByte else { continue }
            let startCommandByte = startByte + NLAuto.FIRST_COMMAND_BYTE
            let finishCommandByte = startCommandByte + NLCommonCommand.lENGHT
            guard let devices = ModelsManager.shared.getDevices() else { continue }
            let device = devices.list.getDevice(type: .rx, chanel: Int(data[startByte + NLAuto.EVENTER_CHANEL]), id: nil) as? NLRXDevice ?? NLRXDevice.empty()
            let nameStart = NLAuto.FIRST_NAME_BYTE + NLAuto.NAME_LENGHT * i
            let nameFinish = nameStart + NLAuto.NAME_LENGHT - 1
            autos.append(NLAuto(name: data[nameStart..<nameFinish].getString(start: NLAuto.NAME_RANGE.lowerBound, lastReadable: NLAuto.NAME_RANGE.upperBound - 1) ?? String.empty,
                                autoType: data[startByte + NLAuto.TYPE].autoType,
                                isActive: data[startByte + NLAuto.STATE].toBool(),
                                eventer: device,
                                command: parseCommand(data: data.subdata(in: startCommandByte..<finishCommandByte)),
                                parameter: Int(data[startByte + NLAuto.PARAMETER]),
                                number: i))
        }
        return NLAutoList(autos: autos)
    }
    
    private func parseSettings(data: Data) -> NLSettings {
        let settings = NLSettings()
        return settings
    }
    
    private func parseStates(data: Data) -> NLStateObject {
        var list = [Int: DeviceInfo]()
        for i in stride(from: 20, to: 212, by: 3) {
            let strByte = String(bytes: [data[i]], encoding: .utf8)
            guard let wrStrByte = strByte, let stateByte = UInt8(wrStrByte, radix: 16), let setsStr = data.getString(start: i + 1, lastReadable: i + 3) else { continue }
            list.updateValue(DeviceInfo(state: stateByte.toBits(count: 4), sets: setsStr), forKey: (i - 20)/3)
        }
        return NLStateObject(list: list)
    }
    
    private func parseLogs(data: Data) -> NLLogList {
        return NLLogList(logList: [NLLogItem()])
    }
    
    private func parseCommand(data: Data) -> NLCommonCommand {
        return NLCommonCommand(mode: Int(data[NLCommonCommand.MODE]),
                               ctr: Int(data[NLCommonCommand.CTR]),
                               res: Int(data[NLCommonCommand.RES]),
                               chanel: Int(data[NLCommonCommand.CHANEL]),
                               command: data[NLCommonCommand.COMMAND].command,
                               format: Int(data[NLCommonCommand.FORMAT]),
                               data0: Int(data[NLCommonCommand.DATA0]),
                               data1: Int(data[NLCommonCommand.DATA1]),
                               data2: Int(data[NLCommonCommand.DATA2]),
                               data3: Int(data[NLCommonCommand.DATA3]),
                               id: data.subdata(in: NLCommonCommand.ID_INTERVAL).hexEncodedString(options: .upperCased))
    }
}

enum Prefixes {
    // in Data
    case userBin
    case deviceBin
    case timerBin
    case presetBin
    case autoBin
    case settingsBin
    // not in Data
    case stateHtm
    case logBin
    //attach
    case bindHtm
    case rxsetHtm
    // exception
    case unknown
    // command
    case command
}

extension Prefixes {
    
    static var editable: [Prefixes] {
        return [.userBin, .deviceBin, .timerBin, .presetBin, .autoBin]
    }

    func getStringPrefix() -> String {
        switch self {
        case .userBin: return "PRF64U"
        case .deviceBin: return "PRF64D"
        case .timerBin: return "PRF64T"
        case .presetBin: return "PRF64P"
        case .autoBin: return "PRF64A"
        case .settingsBin: return "PRF64S"
        case .stateHtm: return "PRF64H"
        case .logBin: return "PRF64L"
        case .unknown: return String.empty
        case .command: return String.empty
        case .bindHtm: return String.empty
        case .rxsetHtm: return String.empty
        }
        
    }
    
    func getStr() -> String {
        switch self {
        case .userBin:
            return "userBin"
        case .deviceBin:
            return "deviceBin"
        case .timerBin:
            return "timerBin"
        case .presetBin:
            return "presetBin"
        case .autoBin:
            return "autoBin"
        case .settingsBin:
            return "settingsBin"
        case .stateHtm:
            return String.empty
        case .logBin:
            return "logBin"
        case .unknown:
            return String.empty
        case .command:
            return String.empty
        case .bindHtm:
            return String.empty
        case .rxsetHtm:
            return String.empty
        }
    }
    
    func expectedCount() -> Int {
        switch self {
        case .userBin: return 12294
        case .deviceBin: return 4102
        case .timerBin: return 8198
        case .presetBin: return 32774
        case .autoBin: return 12294
        case .settingsBin: return 4102
        case .unknown: return 0
        case .stateHtm: return 269
        case .logBin: return 0
        case .command: return 2
        case .bindHtm: return 44
        case .rxsetHtm: return 30
        }
    }
}

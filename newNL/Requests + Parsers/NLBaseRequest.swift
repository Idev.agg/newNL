//
//  BaseRequest.swift
//  newNL
//
//  Created by Глеб Сергей on 20.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

enum RequestMethod {
    case GET
    case POST
}

protocol BaseRequestProtocol: class {}

class BaseRequestModel: NSObject, BaseRequestProtocol {
    
    var url: String?
    var method: String?
    var parameters: [String: Any]?
    var data: Data?
    
    init(isUpload: Bool) {
        super.init()
        guard isUpload else {
            return
        }
        guard let baseUrlString = PreferencesService.shared.getCurrentAccount()?.adress.getUrl() else { return }
        self.method = RequestMethod.POST.string
        self.url = baseUrlString + "sett_eic.htm"
    }

    init(file: Prefixes, command: String = String.empty) {
        let currentUrlString = ApiManager.shared.getUrlForFile(prefix: file)
        guard let baseUrlString = PreferencesService.shared.getCurrentAccount()?.adress.getUrl() else { return }
        self.url = baseUrlString + currentUrlString + command
        self.method = RequestMethod.GET.string //method.string
        self.parameters = nil
        self.data = nil
        debugInizializationMessage(someClass: BaseRequestModel.self)
    }
    
    deinit {
        debugDeinitializationMessage(someClass: self)
    }
    
}

class CommandRequestModel: BaseRequestModel {
    
//    var stringParams: String
    
    convenience init(commandString: String?) {
        self.init(file: .command, command: commandString)
//        stringParams = (commandString ?? String.empty)
    }
    
    override init(file: Prefixes, command: String? = String.empty) {
//        self.stringParams = String.empty
        var cmd = command
        if command != nil, command != String.empty {
            cmd = "send.htm?sd=" + command! + "&rnd=\(Int.random(in: 0...1000))"
        }
        super.init(file: file, command: cmd ?? String.empty)
    }
    
}

extension RequestMethod {
    
    var string: String {
        switch self {
        case .GET:  return "GET"
        case .POST: return "POST"
        }
    }
}

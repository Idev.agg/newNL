//
//  NLAccount.swift
//  newNL
//
//  Created by aggrroo on 20.09.2018.
//  Copyright © 2018 Глеб Сергей. All rights reserved.
//

import Foundation

protocol BaseAdress: class, NSCoding {
    
    func getUrl() -> String?
    
}

protocol BaseAuthorization: class, NSCoding {}

class Adress: NSObject, BaseAdress {
    
    private static let IP_ADRESS_KEY_PATH = "com.nooLite.ipAdress"
    private static let DYN_DNS_KEY_PATH = "com.nooLite.dynDns"
    
    private static let HTTP_PREFIX = "http://"
    private static let HTTPS_PREFIX = "https://"
    private static let URL_SEPARATOR = "/"
    
    var ipAdress: String?
    var dynDNS: String?
    
    init(ip: String) {
        self.ipAdress = Adress.HTTP_PREFIX + ip + Adress.URL_SEPARATOR
    }
    
    init(dns: String) {
        self.dynDNS = dns
    }
    
    override init() {}
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(ipAdress, forKey: Adress.IP_ADRESS_KEY_PATH)
        aCoder.encode(dynDNS, forKey: Adress.DYN_DNS_KEY_PATH)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        let ip = aDecoder.decodeObject(forKey: Adress.IP_ADRESS_KEY_PATH) as? String
        let dns = aDecoder.decodeObject(forKey: Adress.DYN_DNS_KEY_PATH) as? String
        guard let ipAdress = ip, dns == nil else {
            self.init(dns: dns!)
            return
        }
        self.init(ip: ipAdress)
    }
    
}

class Login: NSObject, BaseAuthorization {
    
    private static let LOGIN_KEY_PATH = "com.nooLite.login.value"
    
    var value: String
    
    init(login: String) {
        value = login
    }
    
    override init() {
        value = String.empty
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(value, forKey: Login.LOGIN_KEY_PATH)
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        guard let value = aDecoder.decodeObject(forKey: Login.LOGIN_KEY_PATH) as? String else {
            self.init()
            return
        }
        self.init(login: value)
    }
    
}

class Password: NSObject, BaseAuthorization {
    
    private static let PASSWORD_KEY_PATH = "com.nooLite.password.value"
    
    var value: String
    
    init(password: String) {
        value = password
    }
    
    override init() {
        value = String.empty
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(value, forKey: Password.PASSWORD_KEY_PATH)
    }
    
    required convenience init(coder aDecoder: NSCoder) {
        guard let value = aDecoder.decodeObject(forKey: Password.PASSWORD_KEY_PATH) as? String else {
            self.init()
            return
        }
        self.init(password: value)
    }
    
}

class NLAccount: NSObject, BaseAuthorization {
    
    static let NLACCOUNT_KEY_PATH = "com.nooLite.account.main"
    private static let ADRESS_KEY_PATH = "com.nooLite.account.adress"
    private static let LOGIN_KEY_PATH = "com.nooLite.account.login"
    private static let PASSWORD_KEY_PATH = "com.nooLite.account.password"
    private static let HOUSE_NAME_KEY_PATH = "com.nooLite.account.houseName"
    private static let ROOMS_COUNT_KEY_PATH = "com.nooLite.account.roomsCount"
    private static let DEVICES_COUNT_KEY_PATH = "com.nooLite.account.devicesCount"
    private static let IS_CURRENT_ACCOUNT_KEY_PATH = "com.nooLite.account.isCurrent"
    private static let LAST_UPDATE_DAY_KEY_PATH = "com.nooLite.account.lastUpdateDay"

    var adress: Adress
    var login: Login?
    var password: Password?
    var uniqueId: String?
    var isCurrent: Bool?
    var lastUpdateDay: String?
    
    var houseName: String?
    var roomsCount: Int?
    var devicesCount: Int?
    
    init(adress: Adress) {
        self.adress = adress
        self.uniqueId = NLAccount.generateUinqueId()
    }
    
    override init() {
        self.adress = Adress.init()
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(adress, forKey: NLAccount.ADRESS_KEY_PATH)
        aCoder.encode(login, forKey: NLAccount.LOGIN_KEY_PATH)
        aCoder.encode(password, forKey: NLAccount.PASSWORD_KEY_PATH)
        aCoder.encode(houseName, forKey: NLAccount.HOUSE_NAME_KEY_PATH)
        aCoder.encode(roomsCount, forKey: NLAccount.ROOMS_COUNT_KEY_PATH)
        aCoder.encode(devicesCount, forKey: NLAccount.DEVICES_COUNT_KEY_PATH)
        aCoder.encode(isCurrent, forKey: NLAccount.IS_CURRENT_ACCOUNT_KEY_PATH)
        aCoder.encode(lastUpdateDay, forKey: NLAccount.LAST_UPDATE_DAY_KEY_PATH)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let adress = aDecoder.decodeObject(forKey: NLAccount.ADRESS_KEY_PATH) as? Adress else {
            self.init()
            return
        }
        
        self.init(adress: adress)
        login = aDecoder.decodeObject(forKey: NLAccount.LOGIN_KEY_PATH) as? Login
        password = aDecoder.decodeObject(forKey: NLAccount.PASSWORD_KEY_PATH) as? Password
        houseName = aDecoder.decodeObject(forKey: NLAccount.HOUSE_NAME_KEY_PATH) as? String
        roomsCount = aDecoder.decodeObject(forKey: NLAccount.ROOMS_COUNT_KEY_PATH) as? Int
        devicesCount = aDecoder.decodeObject(forKey: NLAccount.DEVICES_COUNT_KEY_PATH) as? Int
        isCurrent = aDecoder.decodeObject(forKey: NLAccount.IS_CURRENT_ACCOUNT_KEY_PATH) as? Bool
        lastUpdateDay = aDecoder.decodeObject(forKey: NLAccount.LAST_UPDATE_DAY_KEY_PATH) as? String
    }
    
    func isActual(complitionHandler: @escaping ((Bool?) -> ())) {
        var isActual = false
        complitionHandler(isActual)
//        let request = BaseRequestModel(currentUrlString: ApiManager.shared.getUrlForFile(prefix: .stateHtm), method: .GET, data: nil, parameters: nil)
            // need to return object and if not actual remove all files
    }
    
}

class NLAccountList: NSObject, BaseAuthorization {
    
    static let ACCOUNT_LIST_KEY_PATH = "com.nooLite.account.accountList"
    
    var accounts: [NLAccount]?
    
    init(accounts: [NLAccount]) {
        self.accounts = accounts
    }
    
    override init() {}
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(accounts, forKey: NLAccountList.ACCOUNT_LIST_KEY_PATH)
    }
    
    required convenience init?(coder aDecoder: NSCoder) {
        guard let accounts = aDecoder.decodeObject(forKey: NLAccountList.ACCOUNT_LIST_KEY_PATH) as? [NLAccount] else {
            self.init()
            return
        }
        self.init(accounts: accounts)
    }
    
}

extension Adress {
    
    func getUrl() -> String? {
        return ipAdress ?? dynDNS
    }
    
}

extension NLAccount {
    
    static func generateUinqueId() -> String {
        return "ABCDEFT"
        return UUID().uuidString }
    
    func getAuthorizationString() -> String? {
        guard let login = login?.value, let password = password?.value else { return nil }
        let loginString = String(format: "%@:%@", login, password)
        let loginData = loginString.data(using: .utf8)!
        return loginData.base64EncodedString()
    }
    
}

